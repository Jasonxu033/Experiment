----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:46:22 10/19/2017 
-- Design Name: 
-- Module Name:    multiply - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BoothMultiplier is
    generic ( N : integer :=8 );
    Port ( multiplicand : in  BIT_VECTOR (N-1 downto 0);
              multiplier : in  BIT_VECTOR (N-1 downto 0);
              product : inout  BIT_VECTOR (2*N-1 downto 0);
              clk : in BIT );
end BoothMultiplier;

architecture Behavioral of BoothMultiplier is
    signal mdreg : BIT_VECTOR (N-1 downto 0);
    signal adderout : BIT_VECTOR (N-1 downto 0);
    signal augend: BIT_VECTOR (N-1 downto 0);
    signal carries, tcbuffout :  BIT_VECTOR (N-1 downto 0);
    signal mrreg : BIT_VECTOR (N downto 0);
    signal adder_ovfl : BIT;
    signal comp,clr_mr,load_mr,shift_mr,clr_md : BIT;
    signal load_md,clr_pp,load_pp,shift_pp : BIT;
    signal state_counter : natural range 0 to 2*N :=0;
Begin
   
    process
        begin
            wait until (clk' event and clk='1');
            if state_counter < 2*N then
                state_counter <= state_counter+1;
            else
                state_counter <= 0;
            end if;
    end process;

    process(state_counter)
		begin

			comp <= '0';
			clr_mr <= '0';
			load_mr <= '0';
			shift_mr <= '0';
			clr_md <= '0';
			load_md <= '0';
			clr_pp <= '0';
			load_pp <= '0';
			shift_pp <= '0';
			IF state_counter = 0 THEN
				load_mr <= '1';
				load_md <= '1';
				clr_pp <= '1';
			ELSIF state_counter MOD 2 = 0 THEN
				shift_mr <= '1';
				shift_pp <= '1';
			ELSE
				IF mrreg(0) = mrreg(1) THEN
					NULL;
				ELSE
					load_pp <= '1';  
				END IF;
				comp <= mrreg(1);
			END IF;
		end process;

    process
		begin
			wait until (clk' event and clk='1');
			IF clr_md = '1' THEN
				mdreg <= (OTHERS => '0');
			ELSIF load_md = '1' THEN
				mdreg <= multiplicand;
			ELSE
				mdreg <= mdreg;
			END IF;
			IF clr_mr = '1' THEN
				mrreg <= (OTHERS => '0');
			ELSIF load_mr = '1' THEN
				mrreg( N DOWNTO 1) <= multiplier;
				mrreg(0) <= '0';
			ELSIF shift_mr = '1' THEN
				mrreg <= mrreg SRL 1;
			ELSE
				mrreg <= mrreg;
			END IF;
			IF clr_pp = '1' THEN
				product <= (OTHERS => '0');
			ELSIF load_pp = '1' THEN
				product(2*N-1 DOWNTO N) <= adderout;
				product(N-1 DOWNTO 0) <= product(N-1 DOWNTO 0);
			ELSIF shift_pp = '1' THEN
				product <= product SRA 1;
			ELSE
				product <= product;
			END IF;
		end process;
    augend <= product(2*N-1 downto N);
    addgen : for i in adderout' range 
    generate
        last_adder : if i = 0 generate
            adderout(i) <= tcbuffout(i) XOR augend(i) XOR comp;
            carries(i) <= (tcbuffout(i) AND augend(i)) OR (tcbuffout(i) AND comp) OR (comp AND augend(i));
        end generate;
        other_adder : if i /= 0 generate
            adderout(i) <= tcbuffout(i) XOR augend(i) XOR carries(i-1);
            carries(i) <= (tcbuffout(i) AND augend(i)) OR (tcbuffout(i) AND carries(i-1)) OR (carries(i-1) AND augend(i));
        end generate;
    end generate;
    adder_ovfl <= carries(N-2) xor carries(N-1);
    tcbuffout <= NOT mdreg WHEN (comp = '1') ELSE mdreg;
    
end Behavioral;

