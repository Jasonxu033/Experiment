----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:45:41 10/12/2017 
-- Design Name: 
-- Module Name:    eight_bit_adder2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eight_bit_adder2 is
    Port ( x : in  STD_LOGIC_VECTOR (7 downto 0);
           y : in  STD_LOGIC_VECTOR (7 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC;
           f : out  STD_LOGIC_VECTOR (7 downto 0));
end eight_bit_adder2;

architecture Behavioral of eight_bit_adder2 is
	signal c: STD_LOGIC_VECTOR ( 8 downto 0);
	signal g: STD_LOGIC_VECTOR ( 7 downto 0);
	signal p: STD_LOGIC_VECTOR ( 7 downto 0);

begin
process (x,y,cin)
    begin
        c(0) <= cin;  
        for i in 0 to 7 loop
            g(i) <= x(i) and y(i);
            p(i) <= x(i) nor y(i);
				c(i+1) <=g(i) or(p(i)and c(i));
        end loop;
        cout <= c(8);
    end process;
end Behavioral;

