----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:21:21 10/12/2017 
-- Design Name:    
-- Module Name:    eight_bit_adder1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eight_bit_adder1 is
    Port ( x : in  STD_LOGIC_VECTOR (7 downto 0);
           y : in  STD_LOGIC_VECTOR (7 downto 0);
           cin : in  STD_LOGIC;
           f : out  STD_LOGIC_VECTOR (7 downto 0);
           cout : out  STD_LOGIC);
end eight_bit_adder1;

architecture Behavioral of eight_bit_adder1 is
	signal c:STD_LOGIC_VECTOR(7 downto 1);

begin
	f(0) <= x(0) xor y(0) xor cin;
   c(1) <= (x(0) and y(0)) or ( (x(0) or y(0)) and cin );
	
	f(1) <= x(1) xor y(1) xor c(1);
   c(2) <= (x(1) and y(1)) or ( (x(1) or y(1)) and c(1) );
	
	f(2) <= x(2) xor y(2) xor c(2);
   c(3) <= (x(2) and y(2)) or ( (x(2) or y(2)) and c(2) );

	f(3) <= x(3) xor y(3) xor c(3);
	c(4) <= (x(3) and y(3)) or ( (x(3) or y(3)) and c(3) );
	
	f(4) <= x(4) xor y(4) xor c(4);
	c(5) <= (x(4) and y(4)) or ( (x(4) or y(4)) and c(4) );
	
	f(5) <= x(5) xor y(5) xor c(5);
	c(6) <= (x(5) and y(5)) or ( (x(5) or y(5)) and c(5) );
	
	f(6) <= x(6) xor y(6) xor c(6);
	c(7) <= (x(6) and y(6)) or ( (x(6) or y(6)) and c(6) );
	
	f(7) <= x(7) xor y(7) xor c(7);
	cout <= (x(7) and y(7)) or ( (x(7) or y(7)) and c(7) );

end Behavioral;

