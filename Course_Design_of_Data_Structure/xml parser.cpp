#include<iostream>
#include<string>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<stack>
#include<vector>
#include<windows.h>
using namespace std;
/**
	Elenmet:     普通标签
	Comment:     注释
	Text：       文本
	Declareation:声明
	Attribute:   属性
	Unknow:      未知标签
*/

enum Type{Element, Comment, Text, Declaration, Attribute, Unknow
};

/**
    定义节点类，用来建树
*/
class XmlNode                                   ///各种节点
{
public:
    XmlNode* parent = NULL;
    XmlNode* pre    = NULL;
    XmlNode* next   = NULL;

    Type type;
    string name;                                         //attribute标签的name-value对的value
    string value;

    XmlNode* firstChild = NULL;
    XmlNode* lastChild = NULL;
};
 /**
    实现各种操作的函数
 */
 bool checkXml(XmlNode* root)             ///检查xml的合法性
 {
     bool result;
     return result;
 }

 /**====================建树部分==========================*/
 XmlNode* createTree(XmlNode* root)
 {
     root = new XmlNode;
     XmlNode* createStringToTree(XmlNode* root, string fileName="");
     XmlNode* createKeyToTree(XmlNode* root);
     cout<<"\n";
     cout<<"0.通过键盘输入\n";
     cout<<"1.通过文件创建\n";
     //cout<<"2.返回主菜单    。";
     cout<<"请输入建树方式：";
     int n;
     cin>>n;
     switch(n)
     {
         case 0: root=createKeyToTree(root);    break;
         case 1: root=createStringToTree(root); break;
         //case 2:                            break;
     }
     cout<<root->firstChild->name;
     cout<<"\n建树完成！\n";
     getchar();
     return root;
 }
 XmlNode* createStringToTree(XmlNode* root, string fileName="")         ///通过读取文件创建Xml树
 {
     int flag=0;
     if(fileName=="")
     {
         cout<<"请输入文件名: ";
         cin>>fileName;
     }
     stack<string>Stack;
     ///一次从xml文件里读取所有内容，存入string中
     std::ifstream t(fileName.c_str());
	 std::stringstream buffer;
	 buffer << t.rdbuf();
	 std::string str(buffer.str());
	 t.close();

	 ///变量初始化
     XmlNode xmlNode;
     XmlNode *point = NULL;
     XmlNode *q     = NULL;
     XmlNode *temp  = NULL;
     XmlNode *tt     = NULL;

     cout<<str;
     string name = str.substr(str.find("<")+1 ,str.find(">")-str.find("<")-1);
     ///TODO
     if(name.find("=")!=-1)  ///当根结点存在属性时
     {
         string name_real = name.substr(0,str.find(" ")-1);
         root->name       = name_real;
         root->type       = Element;
         Stack.push(name_real);
         cout<<"节点："<<name_real<<endl;
         str.erase(0, name.length()+2);

         name = name.erase(0,name.find(" ")+1);
         while(!name.empty())
         {
             string temp;
             if(name.find(" ")==-1)
             {
                 temp=name;
                 cout<<name<<endl;
                 name.erase(0,name.length());
             }
             else
             {
                 temp=name.substr(0,name.find(" "));
                 name = name.erase(0,name.find(" ")+1);
                 cout<<temp<<endl;
             }
             string Name, Value;
             int pos = temp.find("=");
             Name  = temp.substr(0,pos);
             Value = temp.substr(pos+2,temp.length()-pos-3);
             //cout<<Name<<" "<<Value<<endl;

             /**
                插入attribute节点过程，
                该过程不需要移动point
             */
             q = new XmlNode;
             q->name  = Name;
             q->value = Value;
             q->type = Attribute;
             if(root->lastChild)
             {
                 tt = root->lastChild;
                 root->lastChild = q;
                 tt->next=q;
                 q->pre=tt;
                 q->parent=root;
             }
             else
             {
                 root->firstChild=q;
                 root->lastChild =q;
                 q->parent=root;
             }
             cout<<"Name: "<<root->lastChild->name<<"  "<<"Value: "<<root->lastChild->value<<endl;
         }

     }
     else  ///根节点不存在属性时
     {
         root->name = name; ///第一个匹配出来的节点必然是根节点
         root->type = Element;
         Stack.push(name);
         cout<<"节点： "<<root->name<<endl;
     }
     point = root;
     str.erase(0, name.length()+2);
     int i=0;
     while(str[i]=='\n' || str[i]=='\t' || str[i]==' ') str.erase(0,1);


     while(!str.empty())
     {
         //getchar();
         Sleep(30);
         while(str[i]=='\n' || str[i]=='\t' || str[i]==' ') str.erase(0,1);
         /**
            if{当第一个字符是'#',则接下来的文本是comment}
            else
            {当第二个字符是'!',标签为Declareation
             当第一个字符是'<',且name中没有' '时，标签是element；
             当第一个字符是'<',且name中存在' '时，标签是attribute；
             存在'/',则需要出栈匹配了
            }
            如果什么都不是，那么在'<'之前的文本时text
         */
         if(str[0]=='#') ///这里是comment
         {cout<<"0";
         }
         else if(str[0]=='<') ///这里是element，atribute,declareation，待匹配标签， 自包含标签
         {
             name = str.substr(str.find("<")+1 ,str.find(">")-str.find("<")-1);  ///匹配<>内的内容
             if(name[0]=='/') /// </label>
             {
                 string namet = name.substr(1,name.length()-1);
                 if(namet != Stack.top())  ///标签未匹配
                 {
                     cout<<"文件括号不匹配，异常退出。";
                     flag = 1;
                     //getchar();
                     break;
                 }
                 Stack.pop();
                 point = point->parent;
                 str.erase(0, name.length()+2);
                 cout<<"结束标签: "<<name<<endl;
                 if(Stack.empty()) break;
             }
             ///TODO
             else if(name[0] != '/' && name.find("/")!=-1) /// <labels name="value"/>
             {

             }
             else if(name.find("=")!=-1) /// <label name="value">
             {
                 string name_real = name.substr(0,str.find(" ")-1);
                 q = new XmlNode;
                 q->name = name_real;
                 q->type = Element;
                 if(point->lastChild)
                 {
                     temp             = point->lastChild;
                     point->lastChild = q;
                     temp->next       = q;
                     q->pre           = temp;
                     q->parent        = point;
                 }
                 else
                 {
                     point->firstChild=q;
                     point->lastChild =q;
                     q->parent=point;
                 }
                 Stack.push(name_real);
                 point = point->lastChild;
                 cout<<"Name: "<<point->name<<endl;
                 str.erase(0, name.length()+2);

                 name = name.erase(0,name.find(" ")+1);
                 string temp;

                 /** 以" "为分隔符，分割该标签，提取Name，Value，
                     每提取出一对"Name-Value"对，则给当前节点添加子节点，
                     节点类型为attribute
                 */
                 while(!name.empty())
                 {
                     if(name.find(" ")==-1)
                     {
                         temp=name;
                         cout<<name<<endl;
                         name.erase(0,name.length());
                     }
                     else
                     {
                         temp=name.substr(0,name.find(" "));
                         name = name.erase(0,name.find(" ")+1);
                         cout<<temp<<endl;
                     }
                     string Name, Value;
                     int pos = temp.find("=");
                     Name  = temp.substr(0,pos);
                     Value = temp.substr(pos+2,temp.length()-pos-3);
                     //cout<<Name<<" "<<Value<<endl;

                     /**
                        插入attribute节点过程，
                        该过程不需要移动point
                     */
                     q = new XmlNode;
                     q->name  = Name;
                     q->value = Value;
                     q->type = Attribute;
                     if(point->lastChild)
                     {
                         tt = point->lastChild;
                         point->lastChild = q;
                         tt->next=q;
                         q->pre=tt;
                         q->parent=point;
                     }
                     else
                     {
                         point->firstChild=q;
                         point->lastChild =q;
                         q->parent=point;
                     }
                     cout<<"Name: "<<point->lastChild->name<<"  "<<"Value: "<<point->lastChild->value<<endl;
                 }
             }
             else /// <label>
             {
                 q = new XmlNode;
                 q->name = name;
                 q->type = Element;
                 if(point->lastChild)
                 {
                     temp = point->lastChild;
                     point->lastChild = q;
                     temp->next=q;
                     q->pre=temp;
                     q->parent=point;
                 }
                 else
                 {
                     point->firstChild=q;
                     point->lastChild =q;
                     q->parent=point;
                 }
                 Stack.push(name);
                 point = point->lastChild;
                 cout<<"Name: "<<point->name<<endl;
                 str.erase(0, name.length()+2);
             }

         }
         else /// text
         {
             /// get the text name from the str
             name = str.substr(0, str.find("<"));

             /// delete the name part from str
             str.erase(0, name.length());

             /// delete the '\n','\t',' ' in name
             while(name.find("\n")!=-1) name.erase(name.find("\n"),1);
             while(name.find(" " )!=-1) name.erase(name.find(" " ),1);
             while(name.find("\t")!=-1) name.erase(name.find("\t"),1);

             /// insert the node into the tree
             q = new XmlNode;
             point->firstChild = q;
             point->lastChild  = q;
             q    ->type       = Text;
             q    ->name       = name;
             q    ->parent     = point;

             /// cout the node->name
             cout<<"text: "<<point->firstChild->name<<endl;
         }
     }
     if(flag==0) cout<<"建树成功！";
     //else root=NULL;
     return root;
 }
 XmlNode* createKeyToTree(XmlNode* root)                                ///通过键盘输入创建Xml树
 {
     XmlNode* point = root;
     XmlNode* temp  = NULL;
     XmlNode* q     = NULL;
     int n;
     cout<<"请输入根节点标签\n";
     string name;
     cin>>name;
     point->name=name;
     while(1)
     {
         cout<<"请输入相应字符\n"\
               "0 ：插入普通标签\n"\
               "1 ：插入注释标签\n"\
               "2 ：插入文本标签\n"\
               "3 ：插入声明标签\n"\
               "6 ：进入左子节点\n"\
               "7 ：进入兄弟节点\n"\
               "8 ：返回父节点\n"\
               "9 ：退出\n";
         cin>>n;
         switch(n)
         {
            case 0:///插入element
            {
                cout<<"请输入标签名:";
                cin>>name;
                q = new XmlNode;
                q->name = name;
                q->type = Element;
                if(point->lastChild)
                {
                    temp = point->lastChild;
                    point->lastChild = q;
                    temp->next=q;
                    q->pre=temp;
                    q->parent=point;
                }
                else
                {
                    point->firstChild=q;
                    point->lastChild =q;
                    q->parent=point;
                }
                cout<<"当前节点"<<point->name;
            }
            break;
            case 1:///插入comment
            {
                cout<<"请输入标签名:";
                cin>>name;
                q = new XmlNode;
                q->name = name;
                q->type = Comment;
                if(point->lastChild)
                {
                    temp = point->lastChild;
                    point->lastChild = q;
                    temp->next=q;
                    q->pre=temp;
                    q->parent=point;
                }
                else
                {
                    point->firstChild=q;
                    point->lastChild =q;
                    q->parent=point;
                }
                cout<<"当前节点"<<point->name;
            }
            break;
            case 2:///插入text
            {
                cout<<"请输入标签名:";
                cin>>name;
                q = new XmlNode;
                q->name = name;
                q->type = Text;
                if(point->lastChild)
                {
                    temp = point->lastChild;
                    point->lastChild = q;
                    temp->next=q;
                    q->pre=temp;
                    q->parent=point;
                }
                else
                {
                    point->firstChild=q;
                    point->lastChild =q;
                    q->parent=point;
                }
                cout<<"当前节点"<<point->name;
            }
            break;
            case 3:///插入Declareation
            {
                cout<<"请输入标签名:";
                cin>>name;
                q = new XmlNode;
                q->name = name;
                q->type = Declaration;
                if(point->lastChild)
                {
                    temp = point->lastChild;
                    point->lastChild = q;
                    temp->next=q;
                    q->pre=temp;
                    q->parent=point;
                }
                else
                {
                    point->firstChild=q;
                    point->lastChild =q;
                    q->parent=point;
                }
                cout<<"当前节点"<<point->name;
            }
            break;
            case 6: ///firstchild
            {
                if(!point->firstChild)
                    cout<<"该节点没有儿子"<<endl;
                else
                {
                    point = point->firstChild;
                    cout<<"当前节点:"<<point->name<<endl;
                }
            };
            break;
            case 7: ///next
            {
                if(!point->next)
                    cout<<"该节点不存在右兄弟节点"<<endl;
                else
                {
                    point = point->next;
                    cout<<"当前节点:"<<point->name<<endl;
                }
            }
            break;
            case 8: ///parent
            {
                if(!point->parent)
                    cout<<"该节点是根节点"<<endl;
                else
                {
                    point = point->parent;
                    cout<<"当前节点:"<<point->name<<endl;
                }
            };
            break;
            case 9:
            {
                /*
                temp = point->firstChild;
                while(temp != NULL)
                {
                    if(temp->next->type != Text)
                    {
                        cout<<"叶子节点必须是文本";
                        break;
                    }
                    temp = temp->next;
                }
                return root;
                */
            }
            return root;
            break;
         }
     }
     return root;
 }
 /**======================================================*/

 /**====================查找部分==========================*/
void searchNode(XmlNode* root )
{
    XmlNode* searchXmlNode(XmlNode* root,string findname);
    XmlNode* searchNodeByAttribute(XmlNode* root);
    string inputnodeName;
    cout<<"\n";
    cout<<"0.通过标签名字查找\n";
    cout<<"1.通过名字与属性查找唯一确定的标签\n";
    cout<<"请输入查找方式：";
    char n;
    cin>>n;
    switch(n)
    {
         case '0':
             cout<<"\n请输入标签名: ";
             cin>>inputnodeName;
             searchXmlNode(root, inputnodeName);
             break;
         case '1':
             searchNodeByAttribute(root);
             break;
     }
     getchar();
     return;
}
void dumpNode(XmlNode* pNode,string findname,vector <XmlNode*> &vecNode)
{
    /***

     */
    XmlNode* pchildNode=NULL;
    //cout<<pNode->name<<"   ";
    if(pNode->type==Element)///如果是普通类型，对孩子递归
    {
        if(pNode->name==findname)
            {
                //cout<<"找到了"<<"   ";
                vecNode.push_back(pNode);///同名，入vector

            }
        for(pchildNode=pNode->firstChild;pchildNode;pchildNode=pchildNode->next)
        {
            dumpNode(pchildNode,findname,vecNode);

        }
    }
    return ;
}
vector<string> getText(XmlNode* pNode)
{
    /***
        找出文本并输出
        返回存有文本内容的vector
     */
    XmlNode* pchildNode=NULL;
    vector<string> vecText;
    if(pNode->type==Text)
        {
            cout<<pNode->name<<endl;
            vecText.push_back(pNode->name);
        }
    for(pchildNode=pNode->firstChild;pchildNode;pchildNode=pchildNode->next)
    {
        getText(pchildNode);

    }

    return vecText;
}
void printPath(XmlNode* pNode)
{
     /***
        输出某一标签的路径
     */
    stack<XmlNode*> stackNode;
    XmlNode* tempNode=pNode;
    XmlNode* forPrintNode=NULL;
    do{
        stackNode.push(tempNode);
        tempNode=tempNode->parent;
    }while(tempNode);
    string space="";
    while(!stackNode.empty())
    {
        if(stackNode.top()->type==Element)
        {
            cout<<space<<stackNode.top()->name<<"(";
            ///输出每个标签的属性
            for(forPrintNode=stackNode.top()->firstChild;forPrintNode;forPrintNode=forPrintNode->next)
            {
                if(forPrintNode->type==Attribute)
                {
                    cout<<forPrintNode->name<<"=\""<<forPrintNode->value<<"\" ";
                }
            }
            cout<<")\n";
            space=space+" ";
        }
        stackNode.pop();
    }
    cout<<"标签"<<pNode->name<<"包含的文本:\n";
    getText(pNode);
    return;
}
 XmlNode* searchXmlNode(XmlNode* root,string findname)
 {
     /***
        遍历该树，查找名字为findname的标签
        将所有名字为findname的标签指针存到vecNode中
        输出每个标签的路径供选择
        查找成功，返回结点的地址（指针）
        否则返回NULL
     */
     XmlNode* pNode = NULL;
     vector<XmlNode*> vecNode;
     for(pNode=root->firstChild;pNode;pNode=pNode->next)
     {
         dumpNode(pNode,findname,vecNode);
     }

     int size = vecNode.size();
     cout<<"共找到"<<size<<"个标签\n";
     if(!vecNode.empty())
     {
         for(int i=0;i<size;i++)
        {
            cout<<"\n第"<<i<<"个："<<endl;
            printPath(vecNode[i]);  ///输出路径
        }
        cout<<"\n输入想要的标签序号：";
        int nodeNumber;
        cin>>nodeNumber;
        if(0<=nodeNumber&&nodeNumber<size)
            return vecNode[nodeNumber];
        else
            cout<<"该输入不合法！";
            return NULL;
     }

     if(vecNode.empty())
     {
         return NULL;
     }
 }
 XmlNode* searchNodeByAttribute(XmlNode* root)
{
    /***
        仅用来通过输入标签名字和属性查找唯一的标签
        因此输入的（多个）属性和属性值需要能唯一确定一个标签
        （只要能唯一确定某标签既可，既输入的属性个数可以少于该标签的所有属性个数）
        如果输入的属性能匹配到多个标签
        则只返回第一个匹配的标签的指针

     */
    XmlNode* pNode = NULL;
    XmlNode* sameNameNode = NULL;
    XmlNode* tempChildNode = NULL;
    vector<XmlNode*> vecNode;
    vector<string> vecAttribute;
    int attributeNumber;

    cout<<"\n请输入标签名: ";
    string nodeName;
    cin>>nodeName;
    cout<<"\n请输入属性个数：";
    cin>>attributeNumber;
    for(int i=0;i<attributeNumber;i++)
    {
        string attributeName;
        string attributeValue;
        cout<<"\n属性名：";
        cin>>attributeName;
        cout<<"\n属性值：";
        cin>>attributeValue;
        vecAttribute.push_back(attributeName);
        vecAttribute.push_back(attributeValue);///vector内部：<name|value|name|value...<
    }
    for(pNode=root->firstChild;pNode;pNode=pNode->next)
    {
        dumpNode(pNode,nodeName,vecNode);
    }
    ///将所有同名标签存入vecNode后，逐个比较每个标签
    for(int j=0;j<vecNode.size();j++)
    {
        sameNameNode=vecNode[j];
        int checkRight=0;
        for(int k=0;k<=(attributeNumber-1)*2;k=k+2)///将每个存在vector中的属性提取出来，
        {
            for(tempChildNode=sameNameNode->firstChild;tempChildNode;tempChildNode=tempChildNode->next)///在该节点的所有孩子节点中搜索有无同名同值的属性节点
            {
                if(tempChildNode->type==Attribute)///如果该标签为属性标签，则开始比较
                {
                    if(tempChildNode->name==vecAttribute[k]&&tempChildNode->value==vecAttribute[k+1])///如果同名同值
                    {
                        checkRight++;///则匹配的属性个数加一
                    }
                }
            }
        }
        if(checkRight==attributeNumber)///一旦出现具有所有属性都匹配的标签
        {
            cout<<"\n查找结果：\n";
            printPath(vecNode[j]);
            return vecNode[j];///返回该标签的指针
        }

    }
    cout<<"\n没有匹配的标签！\n";
    return NULL;///没找到则返回NULL


}
/**========================================================*/

/**====================删除部分============================*/
void deleteNode(XmlNode* root)
 {
     /**
        删除一个节点
     */
     XmlNode* pNodet=NULL;
     XmlNode* ptempNode=NULL;
     string inputnodeName;
     cout<<"\n";
     cout<<"确定被删除的节点：\n";
     cout<<"0.通过标签名字查找\n";
     cout<<"1.通过名字与属性查找唯一确定的标签\n";
     cout<<"请输入查找方式：";
     char n;
     cin>>n;
     switch(n)
     {
         case '0':
             cout<<"\n请输入你想删除的标签名：: ";
             cin>>inputnodeName;
             pNodet=searchXmlNode(root, inputnodeName);
             break;
         case '1':
             pNodet=searchNodeByAttribute(root);
             break;
    }
     if(pNodet)
    {

     if(!pNodet->pre&&pNodet->next)
     {
         ptempNode=pNodet;
         pNodet=pNodet->next;
         pNodet->parent->firstChild=pNodet;
         pNodet->pre=NULL;
         delete(ptempNode);
     }
     else if(!pNodet->next&&pNodet->pre)
     {
         ptempNode=pNodet;
         pNodet=pNodet->pre;
         pNodet->parent->lastChild=pNodet;
         pNodet->next=NULL;
         delete(ptempNode);
     }
     else if(!pNodet->pre&&!pNodet->next)
     {
         ptempNode=pNodet;
         pNodet->parent->firstChild=NULL;
         delete(ptempNode);
     }
     else if(pNodet->pre&&pNodet->next)
     {
         ptempNode=pNodet;
         pNodet->pre->next=pNodet->next;
         pNodet->next->pre=pNodet->pre;
         delete(ptempNode);
     }
     cout<<"删除完毕\n";
    }
    getchar();
     return;
 }
/**========================================================*/

/**====================插入部分============================*/
 void insertNode(XmlNode* root)
 {
     XmlNode *parent=NULL,*t;
     while(!parent)
     {
         string inputnodeName;
         cout<<"\n";
         cout<<"查找被插入节点的父节点：\n";
         cout<<"0.通过标签名字查找\n";
         cout<<"1.通过名字与属性查找唯一确定的标签\n";
         cout<<"请输入查找方式：";
         char n;
         cin>>n;
         switch(n)
         {
             case '0':
                 cout<<"\n请输入父节点的名字: ";
                 cin>>inputnodeName;
                 parent=searchXmlNode(root, inputnodeName);
                 break;
             case '1':
                 parent=searchNodeByAttribute(root);
                 break;
          }
     }
     cout<<"\n";
     cout<<"0.普通标签\n";
     cout<<"1.属性标签\n";
     cout<<"2.文本    \n";
     //cout<<"3.声明";
     cout<<"请输入插入节点类型： ";
     XmlNode *node = new XmlNode;
     int type;
     cin>>type;
     switch(type)
     {
         case 0: node->type=Element;   break;
         case 1: node->type=Attribute; break;
         case 2: node->type=Text;      break;
     }
     string name,value;
     if(type==1)
     {
         cout<<"请输入属性名： ";
         cin>>name;
         cout<<"请输入属性值： ";
         cin>>value;
         node->value = value;
         node->name  = name;
     }
     else if(type==0)
     {
         cout<<"请输入标签名: ";
         cin>>name;
         node->name  = name;
     }
     else if(type==2)
     {
         cout<<"请输入文本内容： ";
         cin>>name;
         node->name  = name;
     }
     if(parent->lastChild)
     {
         t = parent->lastChild;
         parent->lastChild = node;
         t->next=node;
         node->parent=parent;
     }
     else
     {
         parent->firstChild=node;
         parent->lastChild =node;
         node->parent=parent;
     }
     cout<<"插入成功。";
     getchar();

 }
/**========================================================*/

/**====================输出文件部分============================*/
int count_s=0;
string str_s;
void print(XmlNode* root)
{
    void PrintToFile(XmlNode* root, string fileName);
    cout<<"请输入输出文件名：";
    string fileName;
    cin>>fileName;
    PrintToFile(root, fileName);
}
void MakeString(XmlNode* p)
{
    XmlNode *q=p;
    if(p->type==Attribute)
    {
        for(int i=0;i<count_s;i++)
            str_s+="\t";
        str_s+="<";
        str_s+=p->name;
        str_s+=">";
        str_s+=p->value;
        str_s += "</";
        str_s += p->name;
        str_s += ">\n";

    }
    else if(p->type==Text)
    {
        if(p->parent->firstChild==p->parent->lastChild)
        {
            str_s = str_s.erase(str_s.length()-1, 1);
            str_s+=p->name;
        }
        else
        {
            for(int i=0;i<count_s;i++)
                str_s+="\t";
            //str_s+="\n";
            str_s+=p->name;
            str_s+="\n";
        }
    }
    else
    {
        for(int i=0;i<count_s;i++)
            str_s+="\t";

        str_s+="<";
        str_s+=p->name;
        str_s+=">\n";
    }
    if(p->firstChild)
    {
        count_s++;
        MakeString(q->firstChild);
        count_s--;
        if(p->firstChild->type!=Text&&p->firstChild->next)
        {
            for (int i = 0; i < count_s; i++)
                str_s += "\t";
        }
        else if(p->lastChild->type!=Text)
            for (int i = 0; i < count_s; i++)
                str_s += "\t";
        str_s += "</";
        str_s += p->name;
        str_s += ">\n";


    }
    else if(p->firstChild==NULL&&p->type!=Text&&p->type!=Attribute)
    {
        str_s = str_s.erase(str_s.length()-1, 1);
        str_s += "</";
        str_s += p->name;
        str_s += ">\n";
    }

    if(p->next)
    {
        MakeString(q->next);

       /* for(int i=0;i<count_s;i++)
            str_s+="\t";

        str_s += "</";
        str_s += p->name;
        str_s += ">\n";*/
    }
}
void PrintToFile(XmlNode* root, string fileName)
{

    XmlNode* p=root;
    MakeString(p);
    ofstream t(fileName.c_str());
    t<<str_s;
    cout<<str_s;
    //getchar();
    t.close();
    str_s.clear();
    cout<<str_s;
    getchar();
}
/**========================================================*/

/**====================修改部分============================*/
void editLabel(XmlNode* root)
{
    string labelName;
    XmlNode *node = NULL;
    while(node==NULL)
    {
        cout<<"\n请输入要修改的标签名： ";
        cin>>labelName;
        node = searchXmlNode(root, labelName);
    }
    string name,value;
    if(node->type == Attribute)
    {
        cout<<"您选择的是属性标签。\n";
        cout<<"请输入属性名： \n";
        cin>>name;
        cout<<"请输入属性值： \n";
        cin>>value;
        node->name = name;
        node->value= value;
    }
    else
    {
        cout<<"请输入需要修改的标签名: ";
        cin>>name;
        node->name = name;
    }
}
/**========================================================*/

 /**
 主函数，用以调试`
 */
int main()
{
	XmlNode *root; //= new XmlNode;
	XmlNode *p;
	//root = createKeyToTree(root);
	//root = createStringToTree(root, "2.xml");
	//p = root;
	//string fname;
    //cout<<endl<<"想查找哪个标签？输入标签名：";
    //cin>>fname;
	//searchXmlNode(p,fname);
    char n;
    while(1)
    {
        getchar();
        system("cls");
        cout<<"===========XML解析===========\n";
        cout<<"        0.XML文件解析\n";
        cout<<"        1.查找节点   \n";
        cout<<"        2.插入节点   \n";
        cout<<"        3.删除节点   \n";
        cout<<"        4.修改标签   \n";
        cout<<"        5.输出文件   \n";
        cout<<"        #.退出程序   \n";
        cout<<"==============================\n";
        cout<<"请输入功能号： ";
        cin>>n;
        switch(n)
        {
            case '0':root=createTree(root);break;
            case '1':searchNode(root)     ;break;
            case '2':insertNode(root)     ;break;
            case '3':deleteNode(root)     ;break;
            case '4':editLabel(root)      ;break;
            case '5':print(root)          ;break;
            case '#':exit(1)              ;break;
        }
    }
	return 0;
}
