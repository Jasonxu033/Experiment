create table SL
	(
		学号 char(10),
		课程号 char(10),
		成绩 char(10),
		primary key(学号, 课程),
		foreign key 学号 references student(学号),
		foreign key 课程号 references lesson(课程号)
	);