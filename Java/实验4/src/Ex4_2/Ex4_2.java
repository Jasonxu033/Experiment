/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4_2;

/**
 *
 * @author Jason_xu
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Ex4_2 extends WindowAdapter implements AdjustmentListener
{
    JLabel la=new JLabel("刻度："), la2=new JLabel("调色板"), lar=new JLabel("红色"),
            lag=new JLabel("绿色"), lab=new JLabel("蓝色"); //标签
    
    int r=0,g=0,b=0;
    
    JScrollBar sbr=new JScrollBar(JScrollBar.HORIZONTAL,10,10,0,260),
            sbg=new JScrollBar(JScrollBar.HORIZONTAL,10,10,0,260),
            sbb=new JScrollBar(JScrollBar.HORIZONTAL,10,10,0,260);//滚动条
    
    public Ex4_2()
    {
        JFrame f=new JFrame("调色板");
        Container cp=f.getContentPane();
        Box baseBox=Box.createVerticalBox();
        Box box1=Box.createHorizontalBox();
        Box box2=Box.createVerticalBox();
        cp.add(baseBox);
        baseBox.add(box1);
        baseBox.add(box2);
        box1.add(la);
        box2.add(la2);
        box2.add(lar);
        box2.add(sbr);
        box2.add(lag);
        box2.add(sbg);
        box2.add(lab);
        box2.add(sbb);
        
        la2.setBackground(new Color(0,0,0));
        la2.setBorder(BorderFactory.createEtchedBorder());
        la2.setOpaque(true);
        la2.setMaximumSize(new Dimension(450,220));
        
        sbr.setUnitIncrement(5);
        sbr.setBlockIncrement(10);
        sbr.addAdjustmentListener(this);
        sbg.setUnitIncrement(5);
        sbg.setBlockIncrement(10);
        sbg.addAdjustmentListener(this);
        sbb.setUnitIncrement(5);
        sbb.setBlockIncrement(10);
        sbb.addAdjustmentListener(this);
        
        f.pack();
        f.setVisible(true);
        f.addWindowListener(this);
    }
    public static void main(String[] args)
    {
        new Ex4_2();
    }
    public void adjustmentValueChanged(AdjustmentEvent ae)
    {
        String s1,s2,s3,s;
        if((JScrollBar)ae.getSource()==sbr)
        {
            r=ae.getValue();
            s1="red:";
        }
        if((JScrollBar)ae.getSource()==sbg)
        {
            g=ae.getValue();
            s2="green:";
        }
        if((JScrollBar)ae.getSource()==sbb)
        {
            b=ae.getValue();
            s3="green:";
        }
        s="标签  "+"red:"+r+"  green:"+g+"  blue:"+b;
        la.setText(s);
        la2.setBackground(new Color(r,g,b));
    }
    public void windowClosing(WindowEvent e)
    {
        System.exit(0);
    }
}
