/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4_1;

/**
 *
 * @author Jason_xu
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Ex4_1 extends JApplet implements ActionListener
{
    Container cp=getContentPane();
    JButton bt1=new JButton("North"),bt2=new JButton("South"),
            bt3=new JButton("East"),bt4=new JButton("West");
    JLabel lb=new JLabel("",JLabel.CENTER);
    public void init()
    {
        cp.add(bt1,BorderLayout.NORTH);
        cp.add(bt2,BorderLayout.SOUTH);
        cp.add(bt3,BorderLayout.EAST);
        cp.add(bt4,BorderLayout.WEST);
        cp.add(lb,BorderLayout.CENTER);
        bt1.addActionListener(this);
        bt2.addActionListener(this);
        bt3.addActionListener(this);
        bt4.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==bt1)
        {
            lb.setText("North");
        }
        if(e.getSource()==bt2)
        {
            lb.setText("South");
        }
        if(e.getSource()==bt3)
        {
            lb.setText("East");
        }
        if(e.getSource()==bt4)
        {
            lb.setText("West");
        }
    }
}
