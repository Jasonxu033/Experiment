/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4_4;

/**
 *
 * @author Jason_xu
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class Ex4_4
{
    int r=0,g=0,b=0;
    JFrame f=new JFrame("跑马灯");
    JLabel jb=new JLabel("江南皮革厂！江南皮革厂！倒闭了！倒闭了！！");
    Container cp=f.getContentPane();
    public Ex4_4()//构造方法
    {
        cp.add(jb);
        
        f.pack();
        f.setVisible(true);
        f.setSize(300,150);
        f.addWindowListener(new WinLis());
        new A().run();
    }
    class WinLis extends WindowAdapter//win监听器
    {
        public void windowClosing(WindowEvent e)
        {
            System.exit(0);
        }
    }
    
    public class A extends Thread//多线程
    {
        public void run()
        {
            while(true)
            {
                int i;
                for(i=1;i<=300;i++)
                {
                    try
                    {
                        Thread.sleep(20);
                    }
                    catch(Exception e)
                    {
                        System.out.println("线程错误"+e);
                    }
                    if(i%20==0)
                    {
                        r=new Random().nextInt(256);
                        g=new Random().nextInt(256);
                        b=new Random().nextInt(256);
                    }
                    jb.setLocation(i,5);
                    jb.setForeground(new Color(r,g,b));
                }
                for(i=300;i>=1;i--)
                {
                    try
                    {
                        Thread.sleep(20);
                    }
                    catch(Exception e)
                    {
                        System.out.println("线程错误"+e);
                    }
                    if(i%20==0)
                    {
                        r=new Random().nextInt(256);
                        g=new Random().nextInt(256);
                        b=new Random().nextInt(256);
                    }
                    jb.setLocation(i,5);
                    jb.setForeground(new Color(r,g,b));
                }
            }
        }
    }
    public static void main(String[] args)//主方法
    {
        Ex4_4 aa=new Ex4_4();    
    }
}