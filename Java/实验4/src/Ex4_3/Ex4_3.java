/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4_3;

/**
 *
 * @author Jason_xu
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class Ex4_3 extends WindowAdapter implements ActionListener
{
    String path1,path2;
    
    JTextArea text=new JTextArea();
    JMenuBar bar=new JMenuBar();
    
    JMenu menu1=new JMenu("文件");
    JMenu menu2=new JMenu("编辑");
    JMenu menu3=new JMenu("格式");
    JMenu menu4=new JMenu("查看");
    JMenu menu5=new JMenu("帮助");
    
    JMenuItem menu1_1=new JMenuItem("新建");
    JMenuItem menu1_2=new JMenuItem("打开");
    JMenuItem menu1_3=new JMenuItem("保存");
    JMenuItem menu1_4=new JMenuItem("另存为");
    JMenuItem menu1_5=new JMenuItem("退出");
    
    public Ex4_3()
    {
        JFrame f=new JFrame("文本编辑器");
        Container cp=f.getContentPane();
        
        cp.add(new JScrollPane(text));
        
        bar.setOpaque(true);
        f.setJMenuBar(bar);
        
        bar.add(menu1);
        menu1.add(menu1_1);
        menu1_1.addActionListener(this);
        menu1.add(menu1_2);
        menu1_2.addActionListener(this);
        menu1.add(menu1_3);
        menu1_3.addActionListener(this);
        menu1.add(menu1_4);
        menu1_4.addActionListener(this);
        menu1.addSeparator();
        menu1.add(menu1_5);
        menu1_5.addActionListener(this);
        
        bar.add(menu3);
        
        f.pack();
        f.setVisible(true);
        f.setSize(1000,500);
        f.addWindowListener(this);
    }
    public static void main(String[] args)
    {
        new Ex4_3();
    }
    public void windowClosing(WindowEvent e)
    {
        System.exit(0);
    }
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==menu1_1)//新建
        {
            text.setText("");
        }
        if(e.getSource()==menu1_2)//打开
        {
            JFileChooser jfc=new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES );  
            jfc.showDialog(new JLabel(), "选择");
            File file=jfc.getSelectedFile();
            String s="";
            path1=file.getAbsolutePath();
            
            StringBuilder result = new StringBuilder();
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
                while((s = br.readLine())!=null)
                {//使用readLine方法，一次读一行
                    result.append(System.lineSeparator()+s);
                }
                br.close();    
            }
            catch(Exception ae)
            {
                ae.printStackTrace();
            }
            s= result.toString();
            text.setText(s);
        }
        if(e.getSource()==menu1_3)//保存
        {
            String s="";
            File fp=new File(path1);
            s=text.getText();
            try{
                PrintWriter pfp= new PrintWriter(fp);
                pfp.print(s);
                pfp.close();
            }
            catch(Exception ae){
                ae.printStackTrace();
            }
        }
        if(e.getSource()==menu1_4)//另存为
        {
            JFileChooser jfc=new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES );  
            jfc.showDialog(new JLabel(), "选择");
            File file=jfc.getSelectedFile();
            String s="";
            path2=file.getAbsolutePath();
            File fp=new File(path2);
            s=text.getText();
            try{
                PrintWriter pfp= new PrintWriter(fp);
                pfp.print(s);
                pfp.close();
            }
            catch(Exception ae){
                ae.printStackTrace();
            }           
        }
        if(e.getSource()==menu1_5)//退出
        {
            System.exit(0);
        }
    }
}
