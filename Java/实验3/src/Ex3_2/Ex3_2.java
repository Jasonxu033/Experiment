/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3_2;

/**
 *
 * @author Jason_xu
 */
import java.awt.Graphics;
import java.applet.Applet;
class Rect
{
    protected double width,height;
    Rect()
    {
        this.height=10;
        this.width=10;
    }
    Rect(double width, double height)
    {
        this.height=height;
        this.width=width;
    }
    double area()
    {
        return height*width;
    }
    double perimeter()
    {
        return 2*(height+width);
    }
}
class PlainRect extends Rect
{
    double startX,startY;
    PlainRect(double width, double height, double startX, double startY)
    {
        super(width, height);
        this.startX=startX;
        this.startY=startY;
    }
    PlainRect()
    {
        super(0, 0);
        this.startX=0;
        this.startY=0;
    }
    boolean isInside(double x, double y)
    {
       if(x>=startX && x<=(startX+width) && y<startY && y>=(startY-height))
           return true;
       else return false;
    }
}
public class Ex3_2
{
    public static void main(String[] args)
    {
    
    }
}
