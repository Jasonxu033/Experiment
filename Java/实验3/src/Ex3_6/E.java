/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3_6;

/**
 *
 * @author Jason_xu
 */
interface InterfaceA
{
    int method(int n);
}
class ClassA implements InterfaceA
{
    public int method(int n)
    {
        return (1+n)*n/2;
    }
}
class ClassB implements InterfaceA
{
    public int method(int n)
    {
        int ans=1;
        for(int i=1;i<=n;i++)
        {
            ans*=i;
        }
        return ans;
    }
}
public class E
{
    public static void main(String[] args)
    {
        InterfaceA a;
        a=new ClassA();
        System.out.println("前n项和是："+a.method(9));
        InterfaceA b;
        b=new ClassB();
        System.out.println("n的阶乘是："+b.method(9));
    }
}
