/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3_5;

/**
 *
 * @author Jason_xu
 */
class A extends Thread
{
    String name;
    int pauseTime;
    A(String name, int pauseTime)
    {
        this.name=name;
        this.pauseTime=pauseTime;
    }
    public void run()
    {
        for(int i=0;i<20;i++)
        {
            try
            {
                System.out.println(name);
                Thread.sleep(pauseTime);
            }
            catch(Exception e)
            {
                System.out.println("线程错误："+e);
            }
        }
    }   
}
public class Ex3_5
{
    static public void main(String[] args)
    {
        A a=new A("A线程", 300);
        a.start();   
    }
}
