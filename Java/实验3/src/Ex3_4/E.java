/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3_4;

/**
 *
 * @author Jason_xu
 */
interface InterfaceA
{
    void  printCapitalLetter();
}
interface InterfaceB
{
    void printLowercaseLetter();
}
class Print implements InterfaceA, InterfaceB
{
    public void  printCapitalLetter()
    {
        System.out.println("A,B,C,D,E,F,\nG,H,I,J,K,L,\nM,N,O,P,Q,R,\nS,T,U,V,W,X,\nY,Z\n");
    }
    public void  printLowercaseLetter()
    {
        System.out.println("a,b,c,d,e,f,\ng,h,i,j,k,l,\nm,n,o,p,q,r,\ns,t,u,v,w,x,\ny,z\n");
    }
}
public class E
{
    public static void main(String[] args)
    {
        Print a=new Print();
        a.printCapitalLetter();
        Print b=new Print();
        b.printLowercaseLetter();
    }
}
