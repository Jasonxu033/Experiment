/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3_7;

/**
 *
 * @author Jason_xu
 */
interface Computer
{
    int computer(int n, int m);
}
class Addition implements Computer
{
    public int computer(int n, int m)
    {
        return n+m;
    }
}
class Subtraction implements Computer
{
    public int computer(int n, int m)
    {
        return n-m;
    }
}
class Division implements Computer
{
    public int computer(int n, int m)
    {
        return n/m;
    }
}
class Multiplication implements Computer
{
    public int computer(int n, int m)
    {
        return n*m;
    }
}
class UseComputer
{
    public void useCom(Computer com, int one, int two)
    {
        System.out.println(com.computer(one, two));
    }
}
public class Ex3_7
{
    public static void main(String[] args)
    {
        Addition a=new Addition();
        Subtraction b=new Subtraction();
        Division c=new Division();
        Multiplication d=new Multiplication();
        UseComputer ans=new UseComputer();
        System.out.print("10+5=");
        ans.useCom(a, 10, 5);
        System.out.print("10-5=");
        ans.useCom(b, 10, 5);
        System.out.print("10/5=");
        ans.useCom(c, 10, 5);
        System.out.print("10*5=");
        ans.useCom(d, 10, 5);
    }
}
