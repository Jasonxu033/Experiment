package exp1;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
/**
 * Servlet implementation class Exp1
 */
@WebServlet("/Exp1")
public class Exp1 extends HttpServlet 
{
	public void service(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException
	{
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("The time now is:"+new java.util.Date());
		out.println("</body>");
		out.println("</html>");
	}
}
