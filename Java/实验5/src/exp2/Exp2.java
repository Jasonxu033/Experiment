package exp2;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

/**
 * Servlet implementation class Exp2
 */
@WebServlet(name="Exp2", urlPatterns="/login.do")
public class Exp2 extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException
	{
		
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		HttpSession	session = request.getSession();
		try
		{
			boolean isLogin=(boolean)session.getAttribute("islogin");
			if(isLogin) 
			{
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<html><head>");
				out.println("已经登陆的用户。");
				out.println("</head></html>");
				return;
			}
		}
		catch(Exception e){}
		if(username.equals("zhangsan") && password.equals("123456"))
		{
			request.getRequestDispatcher("/welcome.html").forward(request, response);
			session.setAttribute("islogin", true);
		}
		else
		{
			response.sendRedirect("login.html");
			return;
		}
	}
}
