/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1_2;

/**
 *
 * @author Jason_xu
 */
public class Ex1_2
{
    static int fun(int n)
    {
        if(n==1) return 1;
        return n*fun(n-1);
    }
    public static void main(String[] args)
    {
        int i,j,sum=0;
        for(i=1;i<=10;i++)
        {
            sum+=fun(i);
        }
        System.out.println(sum);
    }
}
