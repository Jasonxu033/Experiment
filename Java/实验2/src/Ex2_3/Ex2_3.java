/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2_3;

/**
 *
 * @author Jason_xu
 */
class Account
{
    String accountName;
    double balance;
    void saveMoney(double x)
    {
        balance+=x;
    }
    void withdrawMoney(double x)
    {
        balance-=x;
    }
    void inquire()
    {
        System.out.println("the inquire of you account is:"+balance+"\n");
    }
}
public class Ex2_3
{
    public static void main(String[] args)
    {
        Account account=new Account();
        account.balance=100;
        account.inquire();
        account.accountName="Ma Yun";
        System.out.println("存入金额：1000。");
        account.saveMoney(1000);
        account.inquire();
        System.out.println("取出金额：200。");
        account.withdrawMoney(200);
        account.inquire();
    }
}
