/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2_8;

/**
 *
 * @author Jason_xu
 */
class MotorVehicle
{
    String licensePlate;
    int speed;
    double loadCapacity;
    MotorVehicle()
    {
        licensePlate="XX1234";
        speed=100;
        loadCapacity=100;
    }
    MotorVehicle(String licensePlate, int speed, double loadCapacity)
    {
        this.licensePlate=licensePlate;
        this.speed=speed;
        this.loadCapacity=loadCapacity;
    }
    void speedUp()
    {
        this.speed++;
    }
    void speedDown()
    {
        this.speed--;
    }
    void changeLicensePlate(String x)
    {
        this.licensePlate=x;
    }
    double getLoadCapacity()
    {
        return loadCapacity;
    }
}
public class Ex2_8
{
    public static void main(String[] args)
    {
        MotorVehicle car1=new MotorVehicle();
        car1.changeLicensePlate("9752");
        car1.speedUp();
        MotorVehicle car2=new MotorVehicle("5086", 150, 200);
        car2.speedDown();
        System.out.println("车牌号："+car1.licensePlate+"  "+"车速："+car1.speed+"  "+"载重量："+car1.loadCapacity);
        System.out.println("车牌号："+car2.licensePlate+"  "+"车速："+car2.speed+"  "+"载重量："+car2.loadCapacity);

    }
}
