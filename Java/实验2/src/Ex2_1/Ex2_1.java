/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2_1;

/**
 *
 * @author Jason_xu
 */
class Lader
{
    public double shangDi,xiaDi,gao,area;
    Lader(double shangDi, double xiaDi, double gao)
    {
        this.shangDi=shangDi;
        this.xiaDi=xiaDi;
        this.gao=gao;
    }
    double GetArea()
    {
        area=(shangDi+xiaDi)*gao/2;
        return area;
    }
}
class Circle
{
    public double area,r,perimeter;
    Circle(double r)
    {
        this.r=r;
    }
    double GetArea()
    {
        area=java.lang.Math.PI;
        return area;
    }
    double GetPerimeter()
    {
        perimeter=2*java.lang.Math.PI;
        return perimeter;
    }
}
public class Ex2_1
{
    public static void main(String[] args)
    {
        Lader lader=new Lader(12.1,24.2,10.0);
        Circle circle=new Circle(4.0);
        System.out.println("the area of Lader:"+lader.GetArea());
        System.out.println("the area of Circle:"+circle.GetArea());
        System.out.println("the perimeter of Lader:"+circle.GetArea());
    }
}
