package pratice;

public class Car {
	private String number;
	private String name;
	private String date;
	
	public String getName() { return this.name; }
	public String getNumber() { return this.number; }
	public String getDate() { return this.date; }
	
	public void setName(String name) { this.name=name; }
	public void setNumber(String number) { this.number=number; }
	public void setDate(String date) { this.date=date; }
}
