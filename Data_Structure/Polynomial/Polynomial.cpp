#include<iostream>
#include<cstdlib>
using namespace std;
struct Polynomial
{
	int index;
	int coef;
	int length;
	struct Polynomial *next;
};
//创建多项式 
struct Polynomial* CreatPolyn()
{
	int n;
	struct Polynomial *p,*q,*t,*head;
	cout<<"请输入多项式的长度：";
	cin>>n;
	cout << "请输入多项式的系数：\n";
	int i,num;
	head=NULL;
	for(i=0;i<n;i++)
	{
		cin>>num;
		p=(struct Polynomial*)malloc(sizeof(Polynomial));
		p->coef=num;
		p->next=NULL;
		if(head==NULL)
			head=p;
		else q->next=p;
		q=p;
	}
	cout << "请输入多项式的指数：\n";
	t=head;
	while(t!=NULL)
	{
		cin>>t->index;
		t=t->next;
	}
	head->length=n;
	return head;
}
//销毁多项式 
void DestoryPolyn(struct Polynomial *head)
{
	struct Polynomial *p;
	while(head!=NULL)
	{
		p=head;
		head=head->next;
		free(p);
	}
}
//输出多项式 
void PrintPolyn(struct Polynomial *head)
{
	struct Polynomial *t=head;
	while(t!=NULL)
	{
		if(t->next==NULL)
		{
			cout<<t->coef<<"x^"<<t->index;
			break;
		}
		if(t->next->coef < 0)
			cout<<t->coef<<"x^"<<t->index;
		else cout<<t->coef<<"x^"<<t->index<<"+";
		t=t->next;
	}
	cout<<"\n";
}
//多项式加法 
struct Polynomial* AddPolyn(struct Polynomial *a, struct Polynomial *b)
{
	struct Polynomial *headAns=NULL,*p,*q;
	int n,m;
	//headAns->length=0;
	while(a!=NULL && b!=NULL)
	{
		if( a->index == b->index )
		{
			n=a->coef+b->coef;
			m=a->index;
			a=a->next;
			b=b->next;
		}
		else if( (a->index > b->index))
		{
			n=b->coef;
			m=b->index;
			b=b->next;
		}
		else if( (a->index < b->index))
		{
			n=a->coef;
			m=a->index;
			a=a->next;
		}

		p=(struct Polynomial*)malloc(sizeof(Polynomial));
		p->coef=n;
		p->index=m;
		p->next=NULL;
		if(headAns==NULL)
			headAns=p;
		else q->next=p;
			q=p;
		//headAns->length+=1;
	}
	if(a!=NULL && b==NULL)
	{
		while(a!=NULL)
		{
			n=a->coef;
			m=a->index;
			a=a->next;
			p=(struct Polynomial*)malloc(sizeof(Polynomial));
			p->coef=n;
			p->index=m;
			p->next=NULL;
			if(headAns==NULL)
				headAns=p;
			else q->next=p;
				q=p;
			//headAns->length+=1;
		}
	}
	if(b!=NULL && a==NULL)
	{
		while(b!=NULL)
		{
			n=b->coef;
			m=b->index;
			b=b->next;
			p=(struct Polynomial*)malloc(sizeof(Polynomial));
			p->coef=n;
			p->index=m;
			p->next=NULL;
			if(headAns==NULL)
				headAns=p;
			else q->next=p;
				q=p;
			//headAns->length+=1;
		}
	}
	return headAns;
}
//多项式减法 
struct Polynomial* SubPolyn(struct Polynomial *a, struct Polynomial *b)
{
	struct Polynomial *headAns=NULL,*p,*q;
	//headAns->length=0;
	int n,m;
	while(a!=NULL && b!=NULL)
	{
		if( a->index == b->index )
		{
			n=a->coef - b->coef;
			m=a->index;
			a=a->next;
			b=b->next;
			if(n==0) continue;
		}
		else if( (a->index > b->index))
		{
			n=(-1)*b->coef;
			m=b->index;
			b=b->next;
		}
		else if( (a->index < b->index))
		{
			n=a->coef;
			m=a->index;
			a=a->next;
		}

		p=(struct Polynomial*)malloc(sizeof(Polynomial));
		p->coef=n;
		p->index=m;
		p->next=NULL;
		if(headAns==NULL)
			headAns=p;
		else q->next=p;
			q=p;
		//headAns->length+=1;
	}
	if(a!=NULL && b==NULL)
	{
		while(a!=NULL)
		{
			n=a->coef;
			m=a->index;
			a=a->next;
			p=(struct Polynomial*)malloc(sizeof(Polynomial));
			p->coef=n;
			p->index=m;
			p->next=NULL;
			if(headAns==NULL)
				headAns=p;
			else q->next=p;
				q=p;
			//headAns->length+=1;
		}
	}
	if(b!=NULL && a==NULL)
	{
		while(b!=NULL)
		{
			n=(-1)*b->coef;
			m=b->index;
			b=b->next;
			p=(struct Polynomial*)malloc(sizeof(Polynomial));
			p->coef=n;
			p->index=m;
			p->next=NULL;
			if(headAns==NULL)
				headAns=p;
			else q->next=p;
				q=p;
			//headAns->length+=1;
		}
	}
	return headAns;
}
//多项式长度 
int PolyLength(struct Polynomial *head)
{
	return head->length; 
} 
//多项式排序
struct Polynomial* SortPolyn(struct Polynomial *head)
{
	int i,j,t;
	struct Polynomial *b=head;
	for(i=0;i<head->length;i++)
	{
		for(j=0;j<head->length-1-i;j++)
		{
			if(b->index > b->next->index)
			{
				t=b->index;
				b->index=b->next->index;
				b->next->index=t;
				
				t=b->coef;
				b->coef=b->next->coef;
				b->next->coef=t;
			}
			b=b->next;
		}
		b=head;
	}
	return head;
} 
//合并同类项 
struct Polynomial* MergePolyn(struct Polynomial *head)
{
	head=SortPolyn(head);
	int len=head->length,i;
	struct Polynomial *t=head,*p;
	while(t != NULL)
	{
		if(t->next !=NULL && (t->index == t->next->index))
		{
			t->coef+=t->next->coef;
			p=t->next;
			t->next=t->next->next;
			free(p);	
		}
		t=t->next;
	}
	return head;
}
//多项式乘法 
struct Polynomial* MultiPolyn(struct Polynomial *a, struct Polynomial *b)
{
	struct Polynomial *headAns=NULL,*p,*q,*t1,*t2;
	t1=a;t2=b;
	int lena=a->length,lenb=b->length;
	int i,j,n,m;
	for(i=1;i<=lena;i++)
	{
		for(j=1;j<=lenb;j++)
		{
			n= t1->coef * t2->coef;
			m= t1->index + t2->index;
			t2=t2->next;
			
			p=(struct Polynomial*)malloc(sizeof(Polynomial));
			p->coef=n;
			p->index=m;
			p->next=NULL;
			if(headAns==NULL)
				headAns=p;
			else q->next=p;
				q=p;
		}
		t1=t1->next;
		t2=b;
	}
	headAns->length=lena*lenb;
	headAns=MergePolyn(headAns);
	return headAns;
}

int main()
{
	struct Polynomial *headA,*headB,*headC,*add,*sub,*mul;
	headA=CreatPolyn();
	headB=CreatPolyn();
	headC=CreatPolyn();
	add=AddPolyn(headA,headB);
	sub=SubPolyn(headA,headB);
	mul=MultiPolyn(headA,headB);

	cout<<"输出第一个多项式：";
	PrintPolyn(headA);
	cout<<"输出第二个多项式：";
	PrintPolyn(headB);
	cout<<"输出第三个多项式：";
	PrintPolyn(headC);
	cout<<"输出第三个多项式排序后结果：";
	headC=SortPolyn(headC);
	PrintPolyn(headC);
	cout<<"输出第三个多项式合并同类项后结果：";
	headC=MergePolyn(headC);
	PrintPolyn(headC);
	
	cout<<"\n输出前两个多项式的和：";
	PrintPolyn(add);
	
	cout<<"\n输出前两个多项式的差：";
	PrintPolyn(sub);
	
	//cout<<"\n输出第一个多项式的长度："<<headA->length<<endl;
	
	cout<<"\n输出前两个多项式的积：";
	PrintPolyn(mul);
	
	DestoryPolyn(headA);
	DestoryPolyn(headB);
	DestoryPolyn(add);
	DestoryPolyn(sub);
	DestoryPolyn(mul);
	return 0;
}
