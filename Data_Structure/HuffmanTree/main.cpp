#include<iostream>
#include<string>
#include<HuffmanTree.h>
#include<CreatHuffmanTree.h>
#include<HuffmanCoding.h>
using namespace std;

int main()
{
	int n;
	HuffmanTree HT;
	cout << "请输入结点个数：";
	cin >> n;
	cout << "请输入节点权值：";
	int i;
	int *a;
	a = (int *)malloc((n+1)*sizeof(int)); //动态申请数组空间a
	for (i = 1; i <= n; i++)
	{
		cin >>a[i];
	}
	HT = CreatHuffmanTree(a,n);
	
	//输出哈夫曼树
	for (i = 1; i <= 2 * n - 1; i++)
		cout <<"下标号："<<i << " 权重:" <<HT[i].weight <<endl;
	
	HuffmanCoding(HT,n);
	return 0;
}