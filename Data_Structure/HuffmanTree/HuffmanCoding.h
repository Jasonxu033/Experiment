#include<iostream>
using namespace std;
void HuffmanCoding(HuffmanTree HT, int n)
{
	int i, j, k;
	char *stack;
	stack = (char*)malloc(n*sizeof(char));  //申请大小为n的栈
	int top = 1;
	//从叶子节点开始搜索
	for (i = 1; i <= n; i++)
	{
		j = i, k = 0;
		while (HT[j].parent != 0)
		{
			if (HT[HT[j].parent].lchild == j)  //判断j号节点的双亲的做孩子是不是j
			{
				stack[top] = '0';
				top++;
			}
			else
			{
				stack[top] = '1';
				top++;
			}
			j = HT[j].parent;
		}
		cout << "节点" << i << "： ";
		while (top != 0)    //利用堆栈实现数组的倒序输出
		{
			top--;
			cout << stack[top];
		}
		cout << endl;
	}
}