#include<iostream>
//创建赫夫曼树
HuffmanTree CreatHuffmanTree(int* w, int n)
{
	int m = 2 * n - 1;
	HuffmanTree HT;
	HT = (HuffmanTree)malloc((m + 1)*sizeof(HTnode));//申请空间，类似于建立一个数组
	int i, j;
	for (i = 1; i <= m; i++)
	{
		HT[i].parent = 0;
		if (i <= n)
		{
			HT[i].weight = w[i];
		}
	}
	for (i = n + 1; i <= m; i++)
	{
		int a = 999, b = 999, flag1 = 0, flag2 = 0;

		//找出最小的两个节点的下标
		for (j = 1; j < i; j++)
		{
			if (HT[j].parent == 0 && HT[j].weight < a)
			{
				flag1 = j;
				a = HT[j].weight;
			}
		}
		for (j = 1; j < i; j++)
		{
			if (HT[j].parent == 0 && HT[j].weight < b && j != flag1)
			{
				flag2 = j;
				b = HT[j].weight;
			}
		}

		//构建小二叉树
		HT[flag1].parent = i;
		HT[flag2].parent = i;
		HT[i].lchild = flag1;
		HT[i].rchile = flag2;
		HT[i].weight = HT[flag1].weight + HT[flag2].weight;
	}
	return HT;
}