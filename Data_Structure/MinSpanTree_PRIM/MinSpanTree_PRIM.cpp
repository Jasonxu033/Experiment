#include<iostream>
#include<cstdlib>
#define INF 25535
using namespace std;

void Prim(int **map, int n)
{
	int i,j,k,min;
	int *adjvex  = (int*)malloc(n*sizeof(int));  //保存相关定点下标 
	int *lowcost = (int*)malloc(n*sizeof(int*)); //保存相关定点间边的权值 
	lowcost[0] = 0; //lowcost值为0，表示此下表定点已经加入生成树 
	adjvex[0]  = 0;
	for(i=1;i<n;i++)
	{
		lowcost[i] = map[0][i];
		adjvex[i]  = 0;
	}
	for(i=1;i<n;i++)
	{
		min = INF;
		//筛选出lowcost中的非0最小值 
		for(j=1;j<n;j++)
		{
			if(lowcost[j]!=0 && lowcost[j]<min)
			{
				min = lowcost[j];
				k = j;
			}
		}
		cout<<adjvex[k]<<"->"<<k<<endl; //把筛选出的值加入生成树并输出 
		lowcost[k] = 0;
		//初始化下一轮的lowcost 
		for(j=1;j<=n;j++)
		{
			if(lowcost[j]!=0 && lowcost[j]>map[k][j])
			{
				lowcost[j] = map[k][j];
				adjvex[j]  = k;
			}
		}
	}
}
int main()
{
	int n;
	int i,j;
	cin>>n;
	int **map;  //动态申请二维数组 
	map = (int**)malloc(n*sizeof(int*));  
	for(i=0;i<n;i++) map[i] = (int*)malloc(n*sizeof(int));
	for(i=0;i<n;i++)
	//构建图 
	for(j=0;j<n;j++)
	{
		cin>>map[i][j];
		if(i!=j && map[i][j]==0) map[i][j] = INF; //把输入的0转化成INF 
	}
	Prim(map, n);
	return 0;
} 
