//CH6-15.cpp:虚函数与派生类的关系
#include <iostream>
#include <string>
using namespace std;
class A { 
public: 
    void f(int i){cout<<"…A"<<endl;} 
};
class B: public A { 
public:
    virtual void f(int i){cout<<"…B"<<endl;}
};
class C: public B {
public: 
    void f(int i){cout<<"…C"<<endl;}
};
class D: public C{
public:
    void f (int){cout<<"…D"<<endl;}
};
void main(){
    A *pA, a;
    B *pB, b;
	C c;    
	D d;
    pA=&a;    pA->f(1);				//调用A::f
    pA=&b;    pA->f(1);				//调用A::f
    pA=&c;    pA->f(1);				//调用A::f
    pA=&d;    pA->f(1);				//调用A::f
    //pB=&a;	  pB->f(1);	//错误，派生类指针不能指向基类对象
	pB=&b;	  pB->f(1);				//调用B::f
	pB=&c;    pB->f(1);				//调用C::f
	pB=&d;	  pB->f(1);             //调用D::f
}
