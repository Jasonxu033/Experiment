//CH6-18.cpp:派生类D的对象通过基类B的普通函数f调用派生类D中的虚函数g
#include <iostream>
using namespace std;
class B{
public: 
    void f(){ g(); } 
    virtual void g(){ cout << "B::g"; }
};
class D : public B{
public: 
    void g(){ cout << "D::g"; }
};
void main(){
    D d;
    d.f();
}
