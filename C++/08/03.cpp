#include <iostream>
using namespace std;
class B{
public: 
    virtual void f(int i){ cout << "B::f"<<endl; };
};
class D : public B{
public: 
    void f(char c){ cout << "D::f..."<<endl; }
};
void main(){
    D d;
    B *pB = &d, &rB=d, b;
    pB->f('1');
    rB.f('1');
}
