#include<iostream>
using namespace std;
class A{
	int a,b;
public:
	A(int x,int y){a=x;b=y;}
	virtual int geta(){return a;}
	int getb(){return b;}
};
class B:public A{
public:
	B(int x,int y):A(x,y){};
	int geta(){return 2*A::geta();}
};
int main()
{
	B b(1,4);
	cout<<b.geta()<<endl;
	return 0;
}