#include<iostream>
using namespace std;
class B{
public:
	virtual void f(){cout<<"B::f"<<endl;}
};
class D:public B{
public:
	void f(){cout<<"D::f"<<endl;}
};
int main()
{
	D d;
	B *pB = &d,&rB=d,b;
	b=d;
	b.f();
	pB->f();
	rB.f();
	return 0;
}