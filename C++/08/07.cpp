#include<iostream > 
using namespace std;
class Student 
{ 
public: 
    //.... 
    virtual void calcTuition() //计算学费 
    { 
        cout<<"计算学生学费"<<endl; 
    } 
}; 
class GraduateStudent :public Student 
{ 
public: 
	void calcTuition() 
    { 
        cout<<"计算研究生学费"<<endl; 
    } 
}; 
void fn(Student& x) 
{
		x.calcTuition(); 
} 
int main() 
 { 
	Student s; 
	GraduateStudent gs; 
	fn(s);           //计算一下学生s 的学费 
	fn(gs);          //计算一下研究生gs 的学费 
	return 0;
} 
