#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
	for(int i=0;i<4;i++)
	{
		cout.fill(' ');
		cout<<setw(5-i)<<' ';
		cout.fill('@');
		cout<<setw(2*i+1)<<'@'<<endl;
	}
	return 0;
}
