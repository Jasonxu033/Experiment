#include<iostream>
#include<cstring>
using namespace std;
class Person{
private:
	char *name;
	int age;
public:
	Person(char*Name,int Age);
	~Person();
	void setAge(int x){age=x;
	}
	void print();
	Person(const Person &p);
};
Person::Person(char *Name,int Age){
	name=new char[strlen(Name)+1];
	strcpy(name,Name);
		age=Age;
		cout<<"constructor..."<<age<<endl;
}
Person::~Person(){
	cout<<"constructor..."<<age<<endl;
	delete name;
}
void Person::print(){
	cout<<name<<"\t"<<age<<endl;
}
Person::Person(const Person &p){
	name=new char[strlen(p.name)+1];
	strcpy(name,p.name);
	age=p.age;
	cout<<"Copy constructor..."<<endl;
}
int main()
{
	Person p1("zhangyong",21);
	Person p2=p1;
	p1.setAge(1);
	p2.setAge(2);
	p1.print();
	p2.print();
	return 0;
}
