#include<iostream>
using namespace std;
class A{
private:
	int i;
public:
	A(int x){
		i=x;
		cout<<"constructor:"<<i<<endl;
	}
	~A(){cout<<"destructor:"<<i<<endl;}
};
int  main()
{
	A a1(1),a2(2),a3(3),a4(4);
	return 0;
}
