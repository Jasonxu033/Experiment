#include<iostream>
using namespace std;
template<class T>
T max(T *a,int n)
{
	T max=0;
	for (int i = 0; i < n; i++)
		if (a[i] >= max) max = a[i];
	return max;
}
int main()
{
	int a[] = { 5, 2, 7, 3, 9 };
	double b[] = { 8, 3, 1, 4, 5, 6, 8 };
	cout << "a[5] = { 5, 2, 7, 3, 9 }" << endl;
	cout << "b[7] = { 8, 3, 1, 4, 5, 6, 8 }" << endl;
	cout << "a的最大值为：" << max(a, 5) << endl;
	cout << "b的最大值为：" << max(b, 7) << endl;
	return 0;
}