#include<iostream>
#include"List.h"
using namespace std;

void main()
{
	List<int> intList;
	cout<<"在位置1插入节点数据为2： "<<intList.insertNode(2,1)<<endl;
	cout<<"在位置2插入节点数据为5： "<<intList.insertNode(5,2)<<endl;
	cout<<"在位置3插入节点数据为100： "<<intList.insertNode(100,3)<<endl;
	cout<<"--------intList------"<<endl;
	intList.print();
	cout<<"50在链表中的位置是： "<<intList.find(50)<<endl;
	cout<<"5在链表中的位置是 ： "<<intList.find(5)<<endl;
	cout<<"删除位置0上的节点： "<<intList.deleteNode(0)<<endl;
	cout<<"删除位置2上的节点：  "<<intList.deleteNode(2)<<endl;
	cout<<"删除位置4上的节点：  "<<intList.deleteNode(4)<<endl;
	cout<<"--------intList------"<<endl;
	intList.print();
	    
		List<double> doubleList;
		doubleList.insertNode(87.23,1);
		doubleList.insertNode (76.98,2);
		doubleList.insertNode (81.89,3);
		doubleList.insertNode (32.3,4);
		doubleList.insertNode (76.98,2);
		cout<<"\n--------doubleList------"<<endl;
		doubleList.print();
		doubleList.empty();
		cout<<"\n--------doubleList------"<<endl;
		doubleList.print();
}