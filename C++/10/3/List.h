#include<iostream>
using namespace std;
template<class T>struct ListNode
{
	T data;
	ListNode<T> *next;
};
template<class T>class List
{
public:
	List();
	~List();
	bool insertNode(T,int);//在链表指定位置插入元素
	bool deleteNode(int n);//删除链表指定元素
	int  find(T t);		   //查找指定元素在链表中的位置
	void empty();		   //清除链表所有元素
	bool print();		   //打印链表
	int  getLenth()		   //获取链表长度
	{ 
		return length;
	}	
	ListNode<T> *getHead(){return head;}
private:
	ListNode<T> *head;
	int length;
};
template<class T>List<T>::List()//在函数定义前面加上了类模板完全限定List<T>::
{
	ListNode<T> *node=new ListNode<T> ;
	node->next=NULL;
	head=node;
	length=0;
}
template<class T>inline List<T>::~List()//指定为内联函数
{
	ListNode<T> *p;
	while (p=head)
	{
		head=head->next;
		delete p;
	}
}
template<class T>bool List<T>::insertNode(T t,int n)
{
	ListNode<T> *p;
	p=head;
	int i=1;
	while(p!=NULL && i<n)
	{
		p=p->next;
		i++;
	}
	if(p==NULL && i>n)	{
		return false;
	}
	ListNode<T> *q;
	q=new ListNode<T>;
	q->data=t;
	q->next=p->next;
	p->next=q;
	length++;
	return true;
}
template<class T>bool List<T>::deleteNode(int n)
{
	int i=1;
	ListNode<T> *p,*q;
	p=head;
	while(p->next!=NULL && i<n)
	{
		p=p->next;
		i++;
	}
	if (p->next==NULL || i>n) {
		cout<<"删除未成功：找不到指定位置。"<<endl;
		return false; 
	}
	q=p->next;
    cout<<"删除数据项..."<<q->data<<endl;
    p->next=q->next;
    length--;
    delete q;
	return true;
}
template<class T>int List<T>::find(T t)
{
	ListNode<T> *p=head->next;
	int i=1;
	while(p && p->data!=t)
	{
		p=p->next;
		i++;
	}
	if(p)
		 return i;
	else
		 return -1;
}
template<class T>bool List<T>::print()
{
	ListNode<T> *p=head->next;
	if(length==0)
		return false;
	else
	{
		cout<<"链表中有"<<length<<"项数据:"<<endl;
		while(p)
		{
			cout<<p->data<<"\t";
			p=p->next;
		}
	}
	cout<<endl;
	cout<<"---------List--------"<<endl;
	return true;
}
template<class T>void List<T>::empty()
{
	ListNode<T> *p,*q;
	p=head->next;
	head->next=NULL;
	while(p!=NULL)
	{
		q=p;
		p=p->next;
		delete q;
	}
	length=0;
}

