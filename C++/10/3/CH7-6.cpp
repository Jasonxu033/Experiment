//CH7-6.cpp：编写一个排序函数sort()，该函数能够List模板建立的链表进行从大到小的排序。 
#include"List.h"
#include<iostream>
using namespace std;
template<class T> 
void sort(List<T> *L)
{
	ListNode<T> *p,*q;
	p=L->getHead();
	p=p->next;
	while(p!=NULL)
	{		
		ListNode<T>* t=p;
		q=p;
		while(q->next!=NULL)
		{	q=q->next;
			if(t->data < q->data )	t=q;
		}
	    int x=t->data;
		t->data =p->data;
		p->data=x;
		p=p->next;
	}
}

void main()
{
	List<int> intList;
	intList.insertNode (3,1);
	intList.insertNode (4,2);
	intList.insertNode (1,3);
	intList.insertNode (32,4);
	intList.print();
	sort(&intList);
	intList.print();

	List<char> charList;
	charList.insertNode ('a',1);
	charList.insertNode ('w',2);
	charList.insertNode ('100',3);
	charList.insertNode ('o',4);
	charList.insertNode ('b',5);
	charList.print();
	sort(&charList);
	charList.print();
}

