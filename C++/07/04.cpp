//CH6-6.cpp:用友元重载Time类的自增运算符++
#include<iostream>
using namespace std;
class Time{
private:
	int hour,minute,second;
public:
	Time(int h,int m,int s);
	friend Time & operator++(Time &t);
	void display();
};
Time::Time(int h,int m,int s):hour(h),minute(m),second(s) {
	if(hour>=24) hour=0;//若初始小时超过，重置为
	if(minute>=60) minute=0;//若初始分钟超过，重置为
	if(second>=60) second=0;//若初始秒钟超过，重置为
}
Time &operator ++(Time &t) {
		++t.second;
		if(t.second>=60){
			t.second=0;
			++t.minute;
			if(t.minute>=60){
				t.minute=0;
				++t.hour;
				if(t.hour>=24)  t.hour=0;
			}
		}
		return t;
}

void Time::display(){
	cout<<hour<<":"<<minute<<":"<<second<<endl;
}
int main(){
		Time t1(23,59,59);
		t1.display();
		++t1;         //隐式调用方式
		t1.display(); 
		operator++(t1);
		t1.display();
		return 0;
}
