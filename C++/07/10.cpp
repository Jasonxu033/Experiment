#include<iostream>
using namespace std;
class Calculator{
	int count;
public:
	Calculator(int n){count=n;}
	friend Calculator & operator ++(Calculator &c){++c.count;return c;}
	friend Calculator & operator ++(Calculator &c,int){c.count++;return c;}
	friend Calculator & operator --(Calculator &c){--c.count;return c;}
	friend Calculator & operator --(Calculator &c,int){c.count--;return c;}
	Calculator operator +(Calculator c){return Calculator(count + c.count);}
	friend ostream & operator << (ostream &os,Calculator &c)
	{
		os<<c.count;
		return os;
	}
	friend istream & operator >> (istream &is,Calculator &c)
	{
		is>>c.count;
		return is;
	}
};
int main()
{
	Calculator a(1),b(2);
	cout<<a<<endl;
	cin>>a;
	++a;cout<<a<<endl;
	a++;cout<<a<<endl;
	a--;cout<<a<<endl;
	--a;cout<<a<<endl;
	a=a+b;cout<<a<<endl;
	return 0;
}

	