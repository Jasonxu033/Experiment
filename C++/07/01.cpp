//CH6-3.cpp：有复数类Complex，利用运算符重载实现复数的加、减、乘、除等复数运算
#include<iostream>
using namespace std;
class Complex {
private:
    double  r, i;
public:
	Complex (double R=0, double I=0):r(R), i(I){ };
	Complex operator+(Complex b);//复数加法
	Complex operator-(Complex b);//复数减法
	Complex operator*(Complex b);//复数乘法
	Complex operator/(Complex b);//复数除法
	void  display();
};
Complex Complex::operator +(Complex b)
{return Complex(r+b.r,i+b.i);}
Complex Complex::operator -(Complex b)
{return Complex(r-b.r,i-b.i);}
Complex Complex::operator *(Complex b){
	Complex t;
	t.r=r*b.r-i*b.i;
	t.i=r*b.i+i*b.r;
	return t;
}
Complex Complex::operator /(Complex b) {
	Complex t;
	double x;
	x=1/(b.r*b.r+b.i*b.i);
	t.r=x*(r*b.r+i*b.i);
	t.i=x*(i*b.r-r*b.i);
	return t;
}
void Complex::display(){
	cout<<r;
	if (i>0) cout<<"+";
	if (i!=0) cout<<i<<"i"<<endl;
}
void main(void) {
	Complex c1(1,2),c2(3,4),c3,c4,c5,c6;
	c3=c1+c2;
	c4=c1-c2;
	c5=c1*c2;
	c6=c1/c2;
	c6=c1+2;
	//c6=2+c1;
	c1.display();
	c2.display();
	c3.display();
	c4.display();
	c5.display();
	c6.display();
}
