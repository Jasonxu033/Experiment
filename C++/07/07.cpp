//CH6-10.cpp:设计一个工资管理类，它能根据职工的姓名录入和查询职工的工资，每个职工的基本数据有职工姓名和工资
#include <iostream>
#include <string>
using namespace std;
struct Person{                	//职工基本信息的结构
	double salary;
	char *name;
};
class SalaryManage{
	Person *employ;          	//存放职工信息的数组
	int max;                  	//数组下标上界
	int n;                    	//数组中的实际职工人数
public:
	SalaryManage(int Max=0){
		max=Max;
		n=0;
		employ=new Person[max];
	}

//重载[]，返回引用,查找有无名字与Name参数代表的同名职工，
//如果没有，就建立该职工，存入工资，然后返回该职工的工资
	double &operator[](char *Name) {   
		Person *p;
		for(p=employ;p<employ+n;p++)      	
			if(strcmp(p->name,Name)==0) 
				return p->salary;
		p=employ + n++; 
		p->name=new char[strlen(Name)+1];
		strcpy(p->name,Name);
		p->salary=0;
		return p->salary;
	}
	void display(){      	//输出全部职工的姓名和工资
		for(int i=0;i<n;i++)
			cout<<employ[i].name<<"   "<<employ[i].salary<<endl;
	}
};
void main(){
		SalaryManage s(3);
		s["杜一为"]=2188.88; 
// []被重载为返回引用的函数，所以[]可以出现在“=”左边
		s["李海山"]=1230.07;
		s["张军民"]=3200.97;

		cout<<"杜一为\t"<<s["杜一为"]<<endl; //返回杜一为的工资
		cout<<"李海山\t"<<s["李海山"]<<endl;
		cout<<"张军民\t"<<s["张军民"]<<endl;
	
		cout<<"------以下为display的输出--------\n\n";
		s.display();
}
