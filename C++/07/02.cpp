//CH6-4.cpp:设计一个时间类Time，它能够完成秒钟的自增运算
#include<iostream>
using namespace std;
class Time{
private:
	int hour,minute,second;
public:
	Time(int h,int m,int s);
	Time operator++();
	void display();
};
Time::Time(int h,int m,int s):hour(h),minute(m),second(s) {
	if(hour>=24) hour=0;//若初始小时超过24，重置为0
	if(minute>=60) minute=0;//若初始分钟超过60，重置为0
	if(second>=60) second=0;//若初始秒钟超过60，重置为0
}
Time Time::operator ++(){
	++second;
	if(second>=60) {
		second=0;
		++minute;
		if(minute>=60){
			minute=0;
			++hour;
			if(hour>=24)  hour=0;
		}
	}
	return *this;
}
void Time::display(){
	cout<<hour<<":"<<minute<<":"<<second<<endl;
}
void main(){
		Time t1(23,59,59);
		t1.display();
		++t1;           //隐式调用方式
		t1.display(); 
		t1.operator++();//显式调用方式
		t1.display();
}
