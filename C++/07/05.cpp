//CH6-7.cpp：设计一个计数器counter，用类成员重载自增运算符实现计数器的自增，用友元重载实现计数器的自减
#include<iostream>
using namespace std;
class Counter{
private:
	int n;
public:
	Counter(int i=0){n=i;}
	Counter operator++();
	Counter operator++(int);
	friend Counter operator--(Counter &c);
	friend Counter operator--(Counter &c,int);
	void display();
};
Counter Counter::operator++(){
		++n;
		return *this;
}
Counter Counter::operator++(int){
		n++;
		return *this;
}
Counter operator--(Counter &c){
		--c.n;
		return c;
}
Counter operator--(Counter &c,int){
		c.n--;
		return c;
}

void Counter::display(){
		cout<<"counter number = "<<n<<endl;
}
int main(){
		Counter a;
		++a;         //调用Counter::operator++()
		a.display();
		a++;         //调用Counter::operator++(int)
		a.display();
		--a;         //调用operator--(Counter &c)
		a.display();
		a--;         //调用operator--(Counter &c,int)
		a.display();
		return 0;
}
