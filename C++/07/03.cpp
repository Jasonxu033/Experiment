//Ch6-5.cpp:对于例6-3中的复数类Complex，利用友元运算符重载实现复数的加法，用成员函数重载减法运算
#include <iostream>
using namespace std;
class Complex {
private:
	double  r, i;
public:
	Complex (double R=0, double I=0) : r(R), i(I){ };
	friend Complex operator+(Complex a,Complex b); //实现复数+复数
	Complex operator-(Complex a); //实现复数-复数
	Complex operator-(double a);  //实现复数-双精度数
	void  display(){
		cout<<r;
		if (i>0) cout<<"+";
		if (i!=0) cout<<i<<"i"<<endl;
	}
};
Complex operator+(Complex a,Complex b)
{
	Complex t;
	t.r=a.r+b.r;
	t.i=a.i+b.i;
	return t;
}
Complex Complex::operator-(Complex a){  return Complex(r-a.r,i-a.i);}
Complex Complex::operator-(double a) {	return Complex(r-a,i);}
int main(){
	Complex c1(1,2),c2(3,4),c3,c4,c5,c6;
	c3=c1+c2; c3.display();
	c3=2+c2;  c3.display();
	c3=c2+2;  c3.display();
	c4=c1-c2; c4.display();
    //c5=4-c1;  c5.display();   //L1 错误
	c5=c1-4;  c5.display ();
	c3=operator+(c1,c2);c3.display();
	c3=operator+(2,c2);
	c3=operator+(c2,2);
	c4=c1.operator-(c2);
	c5=c1.operator-(4);

	c3=Complex(2,0)+c2;
	c3=c2+Complex(2,0);
	c5=c1-Complex(4,0);
	return 0;
}