//CH6-9.cpp:重载类string的赋值运算符，解决赋值操作引起的指针悬挂问题 
#include <iostream>
#include <string>
using namespace std;
class String{
		char *ptr;
        int n;
public:
		String(char * s,int a);
		String& operator=(const String& s);
		~String(){delete ptr;}
		void print(){cout<<ptr<<endl;}
};

String::String(char * s,int a){
		ptr=new char[strlen(s)+1];
		strcpy(ptr,s);
		n=a;
}
String& String::operator=(const String& s) {
		if(this==&s)  return *this;
		delete ptr;
		ptr=new char[strlen(s.ptr)+1];
		strcpy(ptr,s.ptr);
		return *this;
}
void main(){
		String p1("Hello",8);				
		{	String p2("chong qing",10);     
			p2=p1;							
			cout<<"p2:";					
			p2.print();						
		}									    
		cout<<"p1:";						
		p1.print();								
}				
