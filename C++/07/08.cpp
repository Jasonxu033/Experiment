//CH6-13.cpp:有一销售人员类Sales，其数据成员有姓名name，身份证号id，年龄age。重载输入/输出运算符实现对Sales类数据成员的输入和输出
#include<iostream>
#include<string>
using namespace std;
class Sales{
private:
		string name,id;
		int age;
public:
		Sales(string Name,string ID,int Age);
		friend Sales &operator<<(ostream &os,Sales &s);
		//重载输出运算符
		friend Sales &operator>>(istream &is,Sales &s);
		//重载输入运算符
};

Sales::Sales(string Name,string ID,int Age) {
		name=Name;
		id=ID;
		age=Age;
}
Sales& operator<<(ostream &os,Sales &s) {
		os<<s.name<<"\t";             //输出姓名
		os<<s.id<<"\t";               //输出身份证号
		os<<s.age<<endl;              //输出年龄
		return s;
}
Sales &operator>>(istream &is,Sales &s) {
		cout<<"输入雇员的姓名，身份证号，年龄"<<endl; 
		//显示输入提示信息
		is>>s.name>>s.id>>s.age;                      
		//数据成员数据输入
		return s;
}
void main(){
		Sales s1("黄梅","214198012111711",40);  //L1
		cout<<s1;                             	//L2
		cout<<endl;                             //L3
		cin>>s1;                              	//L4
		cout<<s1;                             	//L5
}
