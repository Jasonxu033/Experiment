//CH8-9.cpp设计图8-3所示异常继承体系中从BasicException到FileSysException部分的异常类。
#include<iostream>
	using namespace std;
class BasicException{
public:
	virtual char* Where(){return "BasicException...";}
};
class FileSysException:public BasicException{
public:
	char *Where(){return "FileSysException...";}
};
class FileNotFound:public FileSysException{
public:
	char *Where(){return "FileNotFound...";}
};
class DiskNotFound:public FileSysException{
public:
	char *Where(){return "DiskNotFound...";}
};
void main(){
	try{
		//程序代码
			throw FileSysException();
	}
	catch(BasicException &p){cout<<p.Where()<<endl;}
	try{
		//程序代码
			throw DiskNotFound();
	}
	catch(BasicException &p){cout<<p.Where()<<endl;}
}
