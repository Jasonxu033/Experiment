//CH8-4.cpp:异常处理从函数中独立出来，由调用函数完成
#include<iostream>
using namespace std;
void temperature(int t) {
    if(t==100)  throw "沸点！";
    else if(t==0)  throw "冰点！";
    else  cout<<"the temperature is ..."<<t<<endl;
}
void main(){
    try{
        temperature(10);
        temperature(50);
        temperature(100);
    }
    catch(char *s){cout<<s<<endl;}
}
