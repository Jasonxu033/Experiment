//CH8-3.cpp:temperature是一个检测温度异常的函数，当温度达到冰点或沸点时产生异常
#include<iostream>
using namespace std;
void temperature(int t)
{
    try{
        if(t==100) throw "沸点！";
        else if(t==0) throw "冰点！";
        else cout<<"the temperature is OK..."<<endl;
    }
    catch(int x){cout<<"temperatore="<<x<<endl;}
    catch(char *s){cout<<s<<endl;}
}
void main(){
    temperature(0);								//L1
    temperature(10);							//L2
    temperature(100);							//L3
}
