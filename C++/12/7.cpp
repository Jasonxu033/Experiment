//CH8-7.cpp:设计一个堆栈，当入栈元素超出了堆栈容量时，就抛出一个栈满的异常；
//如果栈已空还要从栈中弹出元素，就抛出一个栈空的异常。
#include <iostream>
using namespace std;
const int MAX=3;
class Full{};	//L1  堆栈满时抛出的异常类
class Empty{};  //L2  堆栈空时抛出的异常类
class Stack{
private:
    int s[MAX];
    int top;
public:
    void push(int a);
    int  pop();
    Stack(){top=-1;}
};
void Stack::push(int a){
    if(top>=MAX-1) throw Full(); //L3
    s[++top]=a;
}
int Stack::pop(){
    if(top<0) throw Empty();     //L4
    return s[top--];
}

void main(){
    Stack s;
    try{
        s.push(10);
        s.push(20);
        s.push(30);
        s.push(40);		//L5  将产生栈满异常
        cout<<"stack(0)= "<<s.pop()<<endl;
        cout<<"stack(1)= "<<s.pop()<<endl;
        cout<<"stack(2)= "<<s.pop()<<endl;
        cout<<"stack(3)= "<<s.pop()<<endl;			//L6
    }
    catch(Full){									//L7
    cout<<"Exception: Stack Full"<<endl;    }
    catch(Empty){									//L8
    cout<<"Exception: Stack Empty"<<endl; }
}
