//CH8-6.cpp:类B有一个类A的对象成员数组obj，类B的构造函数进行了自由存储空间的过量申请，
//最后造成内存资源耗尽产生异常，则异常将调用对象成员数组obj的析构函数，回收obj占用的存储空间
#include<iostream>
using namespace std;
class A{
    int a;
public:
    A(int i=0):a(i){}
    ~A(){cout<<"in A destructor..."<<endl;}
};
class B{
    A obj[3];
    double *pb[10];
public:
    B(int k){
	  cout<<"in B constructor..."<<endl;
	  for (int i=0;i<20;i++){
		pb[i]=new double[2];
		if(i==3)
			throw i;
		else 
			cout<<"Allocated 2 doubles in pb["<<i<<"]"<<endl;
}
    }
};
void main(){
	try{B b(2);}
	catch(int e){ cout<<"catch an exception when allocated pb["	<<e<<"]"<<endl;   }
}
