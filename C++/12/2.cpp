//CH8-2.cpp:catch捕获异常时，不会进行数据类型的默认转换
#include<iostream>
using namespace std;
void main(){
    cout<<"1--befroe try block..."<<endl;
    try{
        cout<<"2--Inside try block..."<<endl;
        throw 10;                        
        cout<<"3--After throw ...."<<endl;
    }
    catch(double i) { 			//仅此与例8.1不同
        cout<<"4--In catch block1 ...catch an int type exception..errcode is.."<<i<<endl;
    }
    cout<<"5--After Catch...";
}
