//CH8-1.cpp:异常处理的简单例程
#include<iostream>
using namespace std;
void main(){
    cout<<"1--befroe try block..."<<endl;
    try{
        cout<<"2--Inside try block..."<<endl;
        throw 10;
        cout<<"3--After throw ...."<<endl;
    }
    catch(int i) {
		cout<<"4--In catch block1 ...catch an int type exception..errcode is.."<<i<<endl;
    }
    catch(char * s) {
        cout<<"5--In catch block2 ...catch an int type exception..errcode is.."<<s<<endl;
    }
    cout<<"6--After Catch...";
}
