#include<iostream>
#include<cstring>
using namespace std;
class Book{
private:
	char bkName[];
	double price;
	static int number;
	static double totalprice;
public:
	Book(char *,double);
	~Book();
	double getPrice(){
		return price;
	}
	char *getName(){
		return bkName;
	}
	static int getNumber(){
		return number;
	}
	static double getTotalPrice()
	{
		return totalprice;
	}
	void display();
};
Book::Book(char *Name,double Price)
{
	strcpy(bkName,Name);
	price=Price;
	number ++;
	totalprice+=price;
}
Book::~Book(){
	number--;
	totalprice-=price;
}
void Book::display(){
	cout<<"book name:"<<bkName<<" "<<"price:"<<price<<endl;
	cout<<"number:"<<number<<" "<<"totalprice:"<<totalprice<<endl;
	cout<<"call static function"<<getNumber()<<endl;
}
int Book::number=0;
double Book::totalprice=0;
int main()
{
	Book b1("C++程序设计",32.5);
	Book b2("数据库系统原理",23);
	cout<<b1.getName()<<"\t"<<b1.getPrice()<<endl;
	cout<<b2.getName()<<"\t"<<b2.getPrice()<<endl;
	cout<<"总共:"<<b1.getNumber<<"本书"<<"\t总价"<<b1.getPrice()<<endl;
	{
		Book b3("数据库系统原理",23);
		cout<<"总共:"<<b3.getNumber<<"本书"<<"\t总价"<<b1.getPrice()<<endl;
	}
	cout<<"总共:"<<b1.getNumber<<"本书"<<"\t总价"<<b1.getPrice()<<endl;
	b2.display();
	return 0;
}

