#include<iostream>
#include<cstring>
using namespace std;
class StudentID{
	public:
		StudentID(int id=0):value(id){
			cout<<"Assigning student id "<<value<<endl;
		}
		~StudentID(){
			cout<<"Destructing id "<<value<<endl;
		}
		int getId(){return value;
		};
	protected:
		int value;
};
class Student{
	public:
		Student(char *pName="noName ",int ssID=0):id(ssID){
		cout<<"Constructing student "<<pName<<endl;
		strncpy(name,pName,sizeof(name));
		name[sizeof(name)-1]='\0';
		StudentID id(ssID);
		}
		void display(){cout<<"student "<<name<<"\t"<<id.getId()<<endl;
		}
	private:
	    char name[20];
	    StudentID id;	
};
int main()
{
	Student s("Randy",9818);
	s.display();
	return 0;
}
