#ifndef Stack_h
#define Stack_h
class Stack{
private:
	int *data;
	int count;
	int size;
public:
	Stack(int stacksize=10);
	~Stack();
	void Push(int x);
	int Pop();
	int howMany();
};
#endif