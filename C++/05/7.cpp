#include<iostream>
class X{
private:
	int a; //不能对成员数据直接赋值
	int b; //引用要在定义时就指明对象
	int c; //const要在定义时赋值
	
	
public:
	X(){a=b=0;} //构造函数没有返回值类型
	X(int i,int j,int k){a=i;b=j;c=k;}
	void setB(int k){b=k;}//static函数针对static数据
	//setC(int k)const{c+=k;}
	X(int i){a=i;}//构造函数应该是公有的
	void setA(int i){ a=i; }//被外部程序调用的成员要是公有的
};
int main()
{
	X x1;
	X x2(3);
	X x3(1,2,3);
	x1.setA(3);
	return 0;
}