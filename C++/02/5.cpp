#include<iostream>
using namespace std;
int min(int *p,int n=5){
	int min=p[0];
	for(int i=1;i<n;i++){
		min=min<p[i]?min:p[i];
	}
	return min;
}
double min(double *p,int n=5){
	double min=p[0];
	for(int i=1;i<5;i++){
		min=min<p[i]?min:p[i];
	}
	return min;
}
float min(float *p,int n=5){
	float min=p[0];
	for(int i=1;i<n;i++){
		min=min<p[i]?min:p[i];
	}
	return min;
}
long min(long *p,int n=5){
	long min=p[0];
	for(int i=1;i<n;i++){
		min=min<p[i]?min:p[i];
	}
	return min;
}


int main()
{
	int a[5]={2,6,5,8,9};
	double b[]={2.1,6.1,5.1,8.1,9.1};
	float c[]={2.1,6.1,5.1,8.1,9.1};
	long d[]={2,6,5,8,9};
	std::cout<<min(a)<<endl<<min(b)<<endl<<min(c)<<endl<<min(d)<<endl;
	return 0;
}