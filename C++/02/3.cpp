#include<iostream>
using namespace std;
int &f1(int x,int y){
	//return x*y;//返回引用的语句必须返回一个变量
	int t=x*y;
	return t;
}
int *f2(int a,int b=1){
	int t=a*b;
	return &t;
}
int main()
{
	//const r;//const的值必须定义时就初始化
    //const r=10;
	//int &a;//引用必须在定义的时候初始化
	int r=10;
	int &a=r;//不能引用常量
	int *p;
	//r=10;
	//a=r;
	const char *pc1="duang";
	//char *const pc2="duang";//指针常量不可指向常量
	char const *pc3="duang";
	const char const *pc4="duang";
	//pc1[2]='t';//指向常量的指针不能改变值
	//pc2[2]='t';
	//pc3[2]='t';
	//pc4[2]='t';//指向常量的常量指针
	cout<<f1(2,3);
	cout<<f2(2,3);
	return 0;
}
