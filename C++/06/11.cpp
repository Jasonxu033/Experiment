#include<iostream>
#include<string>
using namespace std;
class Person{
	string name;
	string age;
	string sex;
public:
	void input(){ cin >> name >> age >> sex; }
	void display(){ cout << "姓名:" << name << "\t" << "年龄:" << age << "\t" << "性别:" << sex << endl; }
};
class Teacher :public Person{
	string num;
	string professional;
	string department;
public:
	void input(){ cin >> num >> professional>> department; }
	void display(){ cout << "编号:" << num << "\t" << "职称:" << professional << "\t" << "系别:" << department << endl; }
};
class Student :public Person{
	string num;
	string professional;
	string department;
public:
	void input(){ cin >> num >> professional >> department; }
	void display(){ cout << "编号:" << num << "\t" << "类别:" << professional << "\t" << "系别:" << department << endl; }
};
int main()
{
	Teacher a;
	Student b;
	a.Person::input();
	a.input();
	b.Person::input();
	b.input(); 
	a.Person::display();
	a.display();
	b.Person::display();
	b.display();
	return 0;
}