#include<iostream>
#include<string>
using namespace std;
class Publication{
	string title;
	string name;
	double price;
	string date;
public:
	void inputDate(){ cin >> title >> name >> price >> date; }
	void display(){ cout << "标题:" << title << "\t" << "书名:" << name << "\t" << "单价:" << price << "\t" << "出版日期：" << date << endl; }
	Publication(){}
	~Publication(){}
};
class Book :public Publication{
	int page;
public:
	void inputDate(){ cin >> page; }
	void display(){ cout << "页数:" << page << endl; }
	Book(){}
	~Book(){}
};
class Tape :public Publication{
	string playtime;
public:
	void inputDate(){ cin >> playtime; }
	void display(){ cout << "播放时长：" << playtime << endl; }
	Tape(){}
	~Tape(){}
};
int main()
{
	Tape a1;
	Book b1;
	a1.Publication::inputDate();
	a1.inputDate();
	b1.Publication::inputDate();
	b1.inputDate();
	a1.Publication::display();
	a1.display();
	b1.Publication::display();
	b1.display();
	return 0;
}