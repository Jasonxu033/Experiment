#include <iostream>
using std::cout;
using std::endl;
class A {
    int a;
public: 
    A(int x) {
        a=x;
        cout<<"Virtual Bass A..."<<a<<endl; 
    }
}; 
class B:virtual public A {
public: 
    B(int i):A(i){ 
		cout<<"Virtual Bass B..."<<endl; }
};
class C:virtual public A{
    int x;
public:
    C(int i):A(i){
        x=i;
		cout<<"Constructing C..."<<endl;  
    }
};
class ABC:public C, public B {
public: 
    ABC(int i,int j,int k):C(i),B(j),A(k)//L1，这里必须对A进行初始化
        { cout<<"Constructing ABC..."<<endl; }
}; 
void main()
{
    ABC obj(1,2,3);
}
