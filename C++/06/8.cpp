#include<iostream>
using namespace std;
class A{
	int x;
public:
	A(int a){x=a;}//构造函数应该是公有成员
	A(){};
	setA(int y){x=y;}
};
class B:private A{
public:
	B(){cout<<"B"<<endl;}
};
int main()
{
	A a1(2),a2;
	A a3=a1;
	B b;
	a2.setA(3);//私有继承无法访问直接访问基类成员
	return 0;
}