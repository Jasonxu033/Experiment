#include<string>
#include"Person.h"
#include"Course.h"
using std::string;
#ifndef Student_h
#define Student_h
class Student : public Person
 {
  private:
		string sno;				//学号
		string major;			//专业
        int ncourse;			//选课门数
		Course courEnroll[5];	//学习的课程信息及成绩
  public:
		Student();
		Student(string Name,string ID,string Sex,double Heigh,string Sno,string Major,int n,Course c[]);
		void setCourseInfo(string Sno,string Major,int n,Course c[]);   //设置成绩信息
		double totalCredits();	//计算获得的总学分
		double totalGrade();	//计算获得的总分
		void print();			//打印输出学生全部信息
		string getMajor(){return major;}//获取学生的专业	
 };
#endif