#include <iostream>
#include <string>
using namespace std;
#ifndef person_h
#define person_h
class Person{
protected:
	 string name;       //姓名
	 string id;         //身份证号
	 double height;     //身高
	 string sex;        //性别
public:
	 Person();
	 Person(string Name, string Id, string Sex,double Height);
	 string getName(){return name;}
	 string getId(){return id;}
	 double getHeight(){return height; }
	 string getSex(){return sex;}
	 void setPersonInfo(string Name, string Id, string Sex,double Height);

};
#endif

