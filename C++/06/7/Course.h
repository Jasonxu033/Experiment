#include<string>
using std::string;
#ifndef Course_h
#define Course_h
class Course{
private:
	string courseName;     //课程名
	string cno;            //课程号
	double grade;          //成绩
	double credit;         //学分
public:
    Course(string Name="",string Cno="",double Grade=0,double Credit=0.0);
	void setCourse(string name,string number,double Grade,double Credits);
	void print();
	double getGrade(){return  grade;}
	double getCredits(){return credit;}
	string getCno(){return cno;}
	string getCourseName(){return courseName;}
};
#endif