#include<iostream>
#include<iomanip>
#include"course.h"
using namespace std;

Course::Course(string Name,string Cno,double Grade,double Credit)
{
	courseName=Name;
	cno=Cno;
	grade=Grade;
	credit=Credit;
}
void Course::setCourse(string Name, string Cno, double Grade, double Credits)
{
	courseName=Name;
	cno=Cno;
	grade=Grade;
	credit=Credits;
}
void Course::print()
{
	cout<<setiosflags(ios::left);
	cout<<setw(14)<<courseName<<setw(10)<<cno<<setw(15)<<credit<<setw(15)<<grade<<endl;
}

