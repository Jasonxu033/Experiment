#include<iostream>
#include<iomanip>
#include"student.h"
#include"Course.h"
using namespace std;
Student::Student(){
	sno="";
	major="";
    ncourse=0;
}
Student::Student(string Name,string ID,string Sex,double Height,string Sno,string Major,int n,Course c[])
:Person(Name,ID,Sex,Height)
{
	sno=Sno;
	major=Major;
	ncourse=n;
	for(int i=0;i<n;i++)
		courEnroll[i]=c[i];
}
void Student::setCourseInfo(string Sno,string Major,int n,Course c[])
{
	sno=Sno;
	major=Major;
	ncourse=n;
	for(int i=0;i<n;i++)
		courEnroll[i]=c[i];
}

double Student::totalCredits(){
	double sum=0.0;
	for(int i=0;i<ncourse;i++)
		sum +=courEnroll[i].getCredits();
	return sum;
}
double Student::totalGrade(){
	double sum=0.0;
	for(int i=0;i<ncourse;i++)
		sum +=courEnroll[i].getGrade();
	return sum;
}
void Student::print()
{
	cout<<setiosflags(ios::left);
	cout<<setw(14)<<getName()<<setw(20)<<getId()
		<<setw(4) <<getSex() <<setw(5) <<getHeight();
	cout<<setw(10)<<sno<<setw(16)<<major;
	cout<<"共"<<ncourse<<"科成绩"<<endl;
	for(int i=0;i<ncourse;i++)
		courEnroll[i].print();
	cout<<"总分为： "<<totalGrade()<<"\t总学分为："<<totalCredits()<<endl;

}
