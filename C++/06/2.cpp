#include <iostream>
using namespace std;
class B{
	int x;
public:
	void set(int i){ x=i;}
	void setx(int i){ x=i;}
	void printx(){ cout<<"x="<<x<<endl; }
};
class D:public B{
	int y;
public:
	void sety(int p){ y=p; }
	void printy(){ cout<<"y="<<y<<endl; }
	void setxy(int i,int j){
		setx(i);//L1 在派生类成员函数中直接访问基类成员
		y=j;
	}
	void set(int i,int j){
		B::set(i);//L2 通过基类名字限定访问被隐藏的基类成员set成员
		y=j;
	}
};
void main(){
	D obj;
	obj.setx(2);//L3 通过派生类对象直接访问基类成员
	obj.printx();//L4 访问基类成员, 输出x=2
	obj.sety(3);//L5 访问派生类成员
	obj.set(1,2);//L6 访问派生类set成员
	obj.B::set(3);//L7 访问基类set成员
}