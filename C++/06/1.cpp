#include<iostream>
using namespace std;
class Base
{
	int x;
public:
	void setx(int i){ x=i;}
	void set(int n){ x=n;}
	void print(){ cout<<"Base class:x="<<x<<endl;}
};
class Derived:public Base
{
	int m,n;
public:
	void set(int p,int k){ m=p;n=k;}
	void set(int i,int j,int k){
		Base::set(i);
		m=j;
		n=k;
	}
	void print()
	{
		Base::print();
		cout<<"Derived Class:m="<<m<<endl;
		cout<<"Derived Class:n="<<n<<endl;
	}
};
int main()
{
	Derived d;
	d.set(1,3);
	d.print();
	d.set(5,6,7);
	d.print();
	d.Base::print();
	d.setx(8);
	return 0;
}
