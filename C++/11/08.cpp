//CH7-14.cpp：用multimap构造汉英对照字典
#include<iostream>
#include<string>
#include<map>
using namespace std;
void main(){		
     multimap<string,string> dict; 	//dict是用于存放字典的multimap
	 multimap<string,string>::iterator p;//定义访问字典的迭代器
	string eng[]={"polt","gorge","cliff","berg","precipice","tract"};
	string chi[]={"小块地，地点","峡谷","悬崖","冰山","悬崖","一片，区域"};
	for(int i=0;i<6;i++)
		dict.insert(make_pair(eng[i],chi[i]));  //批量插入字典元素
		//插入单个元素
	dict.insert(make_pair(string("tract"),string("地带")));
	dict.insert(make_pair(string("precipice"),string("危险的处境")));
	for(p=dict.begin();p!=dict.end();p++)//输出字典内容
		cout<<p->first<<"\t" <<p->second<<endl;//first是键，second是值
	string word;
	cout<<"请输入要查找的英文单词：";
	cin>>word;
	for(p=dict.begin();p!=dict.end();p++)//遍历字典，查找单词
		if(p->first==word)
	        cout<<p->second<<endl; //输出单词的中文解释
	cout<<"请输入要查找的中文单词：";           
	cin>>word;
	for(p=dict.begin();p!=dict.end();p++)//遍历字典，查找汉词
		if(p->second==word)
	        cout<<p->first<<endl;//输出汉词对应的英语单词
}
