//CH7-13.cpp：用map查询雇员的工资
#include<iostream>
#include<string>
#include<map>
using namespace std;
void main(){
	string name[]={"张大年","刘明海","李煜"};//雇员姓名
	double salary[]={1200,2000,1450};//雇员工资
	map<string,double> sal;//用映射存储姓名和工资
	map<string,double>::iterator p;//定义映射的迭代器
	for(int i=0;i<3;i++)
		sal.insert(make_pair(name[i],salary[i]));//将姓名/工资加入映射
	sal["tom"]=3400; //通过下标运算加入map元素
	sal["bob"]=2000;
	for(p=sal.begin();p!=sal.end();p++)	//输出映射中的全部元素
		cout<<p->first<<"\t"<<p->second<<endl; //输出元素的键和值
	string person;
	cout<<"输入查找人员的姓名：";
	cin>>person;
	for(p=sal.begin();p!=sal.end();p++)//据姓名查工资，找到就输出
		if(p->first==person)
	         cout<<p->second<<endl;
}
