//CH7-15.cpp
#include<iostream>
#include<list>
#include<algorithm>
using namespace std;
void main(){
	int a1[]={10,65,20,30,40,35,50,50};
	int a2[]={40,35,60};
	int a[10];
	int *ptr;
	ptr=find(a1,a1+9,40);//查找40在arr数组中的地址
	cout<<40<<"在数组中的下标是："
		<<ptr-a1<<endl;//find返回地址，计算出数组元素位置
	list<int> L1; //定义链表L1
	list<int>::iterator pos;
	for(int i=0;i<7;i++)
		L1.push_back(a1[i]); //将a1数组加入到L1链表中
	pos=find(L1.begin(),L1.end(),40);//在L1中查找40，结果放于pos中
	if(pos!=L1.end()) 
		cout<<"L1链表中存在数据元素："<<*pos//输出找到的数据
	        <<"，它是链表中的第"<<distance(L1.begin(),pos)+1
		<<"个节点！"<<endl; //distance计算迭代器与链首元素间隔的元素个数
	int n1=count(a1,a1+10,50);//统计arr数组中50的个数
	cout<<"arr 数组中有："<<n1<<"个"<<50<<endl;
	ptr=search(a1,a1+6,a2,a2+2);//查找a2数组在a1中的位置
	if(ptr==a1+6)	
		cout<<"no match found\n";
	else
		cout<<"a2 match a1 at:"<<(ptr-a1)<<endl;//输出第一个匹配元素的位置
	
	sort(a1,a1+7);
	sort(a2,a2+3);
	for(int i=0;i<7;i++)
		cout<<a1[i]<<"  ";
	cout<<endl;	
	for(int i=0;i<3;i++)
		cout<<a2[i]<<"  ";
	cout<<endl;	
	merge(a1,a1+7,a2,a2+3,a);//将a1、a2合并，结果放在a数组中
	for(int i=0;i<10;i++)
		cout<<a[i]<<"  ";
	cout<<endl;	
}
