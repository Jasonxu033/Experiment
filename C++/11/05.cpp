//CH7-11.cpp
#include<iostream>
#include<list>
using namespace std;
int main(){
	int i;
	list<int> L1, L2, L3(10); 	//L1、L2是空链表，L3是有10个元素的链表
	list<int>::iterator iter;  //定义迭代器iter
	int a1[]={100,90,80,70,60};       
	int a2[]={30,40,50,60,60,60,80};
	for(i=0;i<5;i++)
		L1.push_back(a1[i]);   	//插入L1链表元素，在表尾插入
	for(i=0;i<7;i++)
		L2.push_front(a2[i]);   //插入L2链表元素，在表头插入
//通过迭代器iter顺序输出L1的所有元素。迭代器是指针，访问iter指向的元素
	for(iter=L1.begin();iter!=L1.end();iter++) 
		cout<<*iter<<"\t" ;
	cout<<endl;
	int sum=0;
	//通过迭代器反向输出L2的所有元素
	list<int>::reverse_iterator ri; 
	for(ri=L2.rbegin();ri!=L2.rend();ri++){ 
		cout<<*ri<<"\t";      
		sum+=*ri;  //计算L2所有链表节点的总和
	}                 
	cout<<"\nL2: sum="<<sum<<endl;
	int data=0;
	//通过迭代器修改L3链表的内容
	for(iter=L3.begin();iter!=L3.end();iter++)	
		*iter=data+=10;         	//修改迭代器所指节点的数据
	for(iter=L3.begin();iter!=L3.end();iter++)
		cout<<*iter<<"\t";
	cout<<endl;
	return 0;
}
