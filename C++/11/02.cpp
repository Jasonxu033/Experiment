//CH7-8.cpp
#include<iostream>
#include<list>                    //链表头文件
using namespace std;
void main(){
	int i;
	list<int> L1,L2; //定义两个没有元素的整数链表L1和L2
	int a1[]={100,90,80,70,60};
	int a2[]={30,40,50,60,60,60,80};
	for(i=0;i<5;i++)
		L1.push_back(a1[i]);//将a1数组加入到L1链表中
	for(i=0;i<7;i++)
		L2.push_back(a2[i]);//将a2数组加入到L2链表中
	L1.reverse();//将L1链表倒序
	L1.merge(L2);//将L2合并到L1链表中
	cout<<"L1的元素个数为："<<L1.size()<<endl; //输出L1的元素个数
	L1.unique();//删除L1中相邻位置的相同元素，只留1个
	while(!L1.empty()){
		cout<<L1.front()<<"\t";//输出L1链表的链首元素
		L1.pop_front();//删除L1的链首元素
	}
	cout<<endl;
}
