//CH-12.cpp
#include<iostream>
#include<string>
#include<set>
using namespace std;
void main(){
	int a1[]={-2,0,30,12,6,7,12,10,9,10};
	set<int,greater<int> >set1(a1,a1+7);//定义从大到小排序的整数集合
	set<int,greater<int> >::iterator p1;//迭代器的定义要与集合排序相符
	set1.insert(12);  //向集合插入元素
	set1.insert(12);	
	set1.insert(4);    
	for(p1=set1.begin();p1!=set1.end();p1++)
		cout<<*p1<<"  "; //输出集合中的内容，它是从大到小排序
	cout<<endl;                         
	string a2[]={"杜明","王为","张清山","李大海","黄明浩","刘一","张三","林浦海","王小二","张清山"};
	//定义字符串的multiset集合，默认排序从小到大
	multiset<string> set2(a2,a2+10);   
	multiset<string>::iterator p2;
	set2.insert("杜明");  
	set2.insert("李则");
	for(p2=set2.begin();p2!=set2.end();p2++)
		cout<<*p2<<"  ";  //输出集合内容
	cout<<endl;
	string sname;
	cout<<"输入要查找的姓名：";
	cin>>sname;//输入要在集合中查找的姓名
	p2=set2.begin();
	bool s=false;	//s用于判定找到姓名与否
	while(p2!=set2.end()){
		if(sname==*p2) { //如果找到就输出姓名
			cout<<*p2<<endl;
			s=true;
		}
		p2++;
	}
   if(!s)
		cout<<sname<<"不在集合中！"<<endl;	//如果没有找到就给出提示
}
