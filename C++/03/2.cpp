#include<iostream>
using namespace std;
class Clock
{
public:
	void setHour(int h){hour=h;}
	void setMinute(int m){minute=m;}
	void setSecond(int s){second=s;}
	void dispTime(){
		cout<<"Now is : "<<hour<<":"<<minute<<":"<<second<<endl;
	}
private:
	int hour,minute,second;
};
int main()
{
	Clock *pa,*pb,aClock,bClock;
	aClock.setMinute(12);
	aClock.setHour(16);
	aClock.setSecond(27);
	bClock=aClock;
	pa=new Clock;
	pa->setHour(10);
	pa->setMinute(23);
	pa->setSecond(34);
	pb=pa;
	pa->dispTime();
	pb->dispTime();
	aClock.dispTime();
	bClock.dispTime();
	return 0;
}
