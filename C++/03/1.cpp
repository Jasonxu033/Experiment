#include<iostream>
using std::cout;
using std::endl;

class Complex
{
private:
	double r;
	double i;
public:
	void init(double rr,double ii)
	{
		r=rr;
		i=ii;
	}
	double real(){return r;}
	double image(){return i;}
};

int main()
{
	Complex a;
	a.init(2,3);
	cout<<a.real()<<"+"<<a.image()<<"i"<<endl;
}