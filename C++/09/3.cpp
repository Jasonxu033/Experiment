#include<iostream>
#include<string>
using namespace std;
class Person{
	string name;
	string num;
	string age;
	string sex;
public:
	virtual void print() = 0;
	virtual void getpay() = 0;
	Person(string na, string nu, string a, string se)
	{
		name = na; num = nu; age = a; sex = se;
	}
	string getname(){ return name; }
	string getnum(){ return num; }
	string getage(){ return age; }
	string getsex(){ return sex; }
};
class Boss:public Person{
	double salary;
public:
	Boss(string a, string b, string c, string d) :Person(a, b, c, d){}
	virtual void print()
	{ 
		cout << "名字：" << getname() << "\t"
			<< "编号：" << getnum() << "\t"
			<< "年龄：" << getage() << "\t"
			<< "性别：" << getsex() << "\t"
			<< "工资：" << salary << endl;
	}
	virtual void getpay()
	{
		cout << "请输入年薪:";
		cin >> salary;
	}
};
class Employee :public Person{
	double salary;
public:
	Employee(string a, string b, string c, string d) :Person(a, b, c, d){}
	virtual void print()
	{
		cout << "名字：" << getname() << "\t"
			<< "编号：" << getnum() << "\t"
			<< "年龄：" << getage() << "\t"
			<< "性别：" << getsex() << "\t"
			<< "工资：" << salary << endl;
	}
	virtual void getpay()
	{
		double n,m;
		cout << "请输入基本工资和奖金:";
		cin >> n>>m;
		salary = n + m;
	}
};
class HourlyWorker :public Person{
	double salary;
public:
	HourlyWorker(string a, string b, string c, string d) :Person(a, b, c, d){}
	virtual void print()
	{
		cout << "名字：" << getname() << "\t"
			<< "编号：" << getnum() << "\t"
			<< "年龄：" << getage() << "\t"
			<< "性别：" << getsex() << "\t"
			<< "工资：" << salary << endl;
	}
	virtual void getpay()
	{
		double n, m;
		cout << "请输入工作小时和小时单价:";
		cin >> n >> m;
		salary = n*m;
	}
};
class CommWorker :public Person{
	double salary;
public:
	CommWorker(string a, string b, string c, string d) :Person(a, b, c, d){}
	virtual void print()
	{
		cout << "名字：" << getname() << "\t"
			<< "编号：" << getnum() << "\t"
			<< "年龄：" << getage() << "\t"
			<< "性别：" << getsex() << "\t"
			<< "工资：" << salary << endl;
	}
	virtual void getpay()
	{
		double n, m;
		cout << "请输入基本工资和营销利润:";
		cin >> n >> m;
		salary = n + 0.15*m;
	}
};
int main()
{
	Boss a("xupinjie", "1", "18", "m");
	a.getpay(); a.print();
	Employee b("shenglongshuai", "2", "19", "m");
	b.getpay(); b.print();
	HourlyWorker c("wangdi", "3", "35", "m");
	c.getpay(); c.print();
	CommWorker d("dabo", "4", "99", "w");
	d.getpay(); d.print();
	return 0;
}