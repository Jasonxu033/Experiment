//main.cpp
#include <iostream>              
#include <iomanip>              
#include "shape.h"              
#include "point.h"              
#include "circle.h"              
#include "cylinder.h"              
using namespace std;
void vpf(const Shape *bptr) {//利用基类shape的指针作接口访问派生类
    bptr->printShapeName();	//打印对象所在的类名        
    bptr->print(); 			//打印对象的数据成员
    cout<<"\nArea = "<<bptr->area()	//输出对象的面积
        <<"\nVolume = "<<bptr->volume()<<"\n\n";//输出对象的体积
}               
               
void vrf(const Shape &bref){//利用基类shape的引用作接口访问派生类
    bref.printShapeName();			//打印对象所在的类名
    bref.print();					//打印对象的数据成员
    cout<<"\nArea = "<<bref.area()				//输出对象的面积
       <<"\nVolume = "<<bref.volume()<<"\n\n";	//输出对象的体积 
} 
int main(){              
    cout << setiosflags(ios::fixed) << setprecision(2); 	//设置数据输出格式，保留小数点后2位有效数字 
    Point point(7, 11); 
    Circle circle(3.5, 22, 8);  
    Cylinder cylinder(10, 3.3, 10, 10);  
    Shape *arrayOfShapes[3];	//定义基类对象的指针数组
    arrayOfShapes[0] = &point;              
    arrayOfShapes[1] = &circle;           
    arrayOfShapes[2] = &cylinder;           
    cout << "----- 通过基类指针访问虚函数 ----------\n"; 
    for(int i = 0; i < 3; i++)     
        vpf(arrayOfShapes[i]);           
    cout << "----- 通过基类引用访问虚函数 ----------\n"; 
    for(int j = 0; j < 3; j++)     
        vrf(*arrayOfShapes[j]);           
    return 0;              
}
