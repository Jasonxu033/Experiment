//point.h
#ifndef POINT_H 
#define POINT_H 
#include "shape.h" 
#include <iostream>
using namespace std;
class Point : public Shape { 
public:               
    Point(int = 0, int = 0);//构造函数，将坐标值初始化为（0，0）
    void setPoint(int, int);//设置点的坐标值  
    int getX() const{ return x; }//返回横坐标值  
    int getY() const{ return y; }//返回纵坐标值  
    virtual void printShapeName() const{ cout << "Point: "; }
//重定义纯虚函数
    virtual void print() const;
private:               
    int x, y;	//point的坐标值
};               
#endif
