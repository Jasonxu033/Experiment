//CH6-21.cpp:抽象图形类及其应用
#include <iostream>
using namespace std;
class Figure{
protected:
	double x, y;
public:
	void set(double i, double j){ x = i; y = j; }
	virtual void area() = 0;	 	//纯虚函数
};
class Triangle :public Figure{
public:
	void area(){ cout << "三角形面积：" << x*y*0.5 << endl; }
	//重写基类纯虚函数
};
class Rectangle :public Figure{
public:
	void area(int i){ cout << "这是矩形，它的面积是：" << x*y << endl; }
};
void main(){
	//Figure f1;			    
	//Rectangle r;			
	Triangle t;
	t.set(10, 20);
	Figure *pF;
	pF = &t;
	pF->area();
	Figure &rF = t;
	rF.set(20, 20);
	rF.area();
}
