/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50537
Source Host           : localhost:3306
Source Database       : dpm_curd

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2019-01-04 11:06:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `applicant`
-- ----------------------------
DROP TABLE IF EXISTS `applicant`;
CREATE TABLE `applicant` (
  `ac_jd` int(11) NOT NULL AUTO_INCREMENT,
  `ac_name` varchar(20) NOT NULL,
  `ac_birthday` datetime NOT NULL,
  `ac_sex` int(1) NOT NULL,
  `workingLife` int(2) NOT NULL,
  PRIMARY KEY (`ac_jd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of applicant
-- ----------------------------

-- ----------------------------
-- Table structure for `projectinfo`
-- ----------------------------
DROP TABLE IF EXISTS `projectinfo`;
CREATE TABLE `projectinfo` (
  `pi_id` int(11) NOT NULL AUTO_INCREMENT,
  `pi_projectName` varchar(50) NOT NULL,
  `pi_startDate` datetime NOT NULL,
  `pi_endDate` datetime NOT NULL,
  `pi_status` int(1) NOT NULL,
  `acId` int(11) NOT NULL,
  PRIMARY KEY (`pi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of projectinfo
-- ----------------------------
