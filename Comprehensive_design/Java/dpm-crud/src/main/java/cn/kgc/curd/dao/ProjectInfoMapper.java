package cn.kgc.curd.dao;

import cn.kgc.curd.bean.ProjectInfo;
import cn.kgc.curd.bean.ProjectInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProjectInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    long countByExample(ProjectInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int deleteByExample(ProjectInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int deleteByPrimaryKey(Integer piId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int insert(ProjectInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int insertSelective(ProjectInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    List<ProjectInfo> selectByExample(ProjectInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    ProjectInfo selectByPrimaryKey(Integer piId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int updateByExampleSelective(@Param("record") ProjectInfo record, @Param("example") ProjectInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int updateByExample(@Param("record") ProjectInfo record, @Param("example") ProjectInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int updateByPrimaryKeySelective(ProjectInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table projectinfo
     *
     * @mbg.generated Fri Jan 04 10:55:28 CST 2019
     */
    int updateByPrimaryKey(ProjectInfo record);
}