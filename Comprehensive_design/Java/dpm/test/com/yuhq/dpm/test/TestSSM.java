package com.yuhq.dpm.test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yuhq.dpm.mapper.ProjectInfoMapper;
import com.yuhq.dpm.model.ProjectInfo;
import com.yuhq.dpm.service.ProjectInfoService;

import sun.net.www.content.text.plain;

/**
 * 测试SSM环境搭建
 */
// 指定运行平台
@RunWith(SpringJUnit4ClassRunner.class)
// 加载spring配置文件
@ContextConfiguration("classpath*:applicationContext-public.xml")
public class TestSSM {

	@Resource
	private Date date;

	@Resource
	private ProjectInfoService projectInfoService;

	@Test
	public void testSpringMyBatis() {
		try {
			List<ProjectInfo> projectInfos = projectInfoService.findAll();

			projectInfos.forEach(projectInfo -> {
				System.out.println(projectInfo);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSave() {
		ProjectInfo projectInfo = new ProjectInfo();
		projectInfo.setPiProjectName("淘宝");
		projectInfo.setPiStartDate(new Date());
		projectInfo.setPiEndDate(new Date());
		projectInfo.setPiStatus(1);
		try {
			Boolean result = projectInfoService.save(projectInfo);
			System.err.println("结果:" + result);
		} catch (Exception e) {
			System.out.println("----" + e.getMessage());
			// e.printStackTrace();
		}
	}

	@Test
	public void testModify() {
		ProjectInfo projectInfo = new ProjectInfo();
		projectInfo.setPiId(5);
		projectInfo.setPiStatus(2);
		try {
			Boolean result = projectInfoService.modify(projectInfo);
			System.err.println("结果:" + result);
		} catch (Exception e) {
			System.err.println("----" + e.getMessage());
			// e.printStackTrace();
		}
	}

	@Test
	public void testRemove() {
		try {
			Boolean result = projectInfoService.remove(5);
			System.err.println("结果:" + result);
		} catch (Exception e) {
			System.err.println("----" + e.getMessage());
			// e.printStackTrace();
		}
	}

	@Test
	public void testSpring() {
		System.out.println("时间:" + date);
	}

	@Test
	public void testMyBatis() throws Exception {
		// 获取配置文件输入流
		InputStream inputStream = Resources.//
				getResourceAsStream("mybatis-config.xml");
		// 通过SqlSessionFactoryBuilder 构建session工厂
		SqlSessionFactory sqlSessionFactory = //
				new SqlSessionFactoryBuilder()//
						.build(inputStream);
		// 通过 sqlSessionFactory 获取 session
		SqlSession openSession = sqlSessionFactory.openSession();
		// 获取 mapper代理对象
		ProjectInfoMapper projectInfoMapper = openSession.//
				getMapper(ProjectInfoMapper.class);
		// 查询全部项目信息
		List<ProjectInfo> projectInfos = projectInfoMapper.fingAll();
		// 输出项目信息
		projectInfos.forEach(projectInfo -> {
			System.out.println(projectInfo);
		});
		// 关闭会话
		if (null != openSession) {
			openSession.close();
		}
	}

}
