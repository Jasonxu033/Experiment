# 搭建SSM框架
## 1.搭建spring开发环境
* 1.1:添加spring core jar包以及spring核心配置文件
* 1.2:配置bean java.util.Date
* 1.3:编写junit4测试类，测试spring环境是否搭建成功

## 2.搭建MyBatis开发环境
* 2.1:添加MyBatis core jar包以及核心配置文件
* 2.2:创建数据库以及数据表并添加测试数据
* 2.3:创建实体类
* 2.4:编写mapper.xml以及mapper接口
* 2.5:注册mapper.xml到MyBatis核心配置文件中
* 2.6:编写junit4进行测试

## 3.整合Spring和MyBatis
 * 3.1:编写spring配置文件 applicationContext-public.xml
 * 3.2.1:添加数据源
 * 3.2.2:添加会话工厂
 * 3.2.3:添加事务管理器
 * 3.2.4:添加aop切面表达式
 * 3.2.5:添加mapper扫描（用于生成动态代理对象）
 * 3.3:创建service层接口与实现（编写CURD方法）
 * 3.4:扫描注解
 * 3.5:使用junit4进行测试
 
## 整合SpringMVC 完成SSM搭建
 * 4.1:添加spring mvc jar包以及mvc配置文件
 * 4.2:配置SpringMVC配置文件
 * 4.3:配置web.xml
 * 4.4:编写controller控制器跳转到index.jsp首页














