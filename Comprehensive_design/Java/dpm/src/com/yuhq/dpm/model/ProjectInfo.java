package com.yuhq.dpm.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.servlet.DispatcherServlet;

/**
 * 项目信息实体
 */
public class ProjectInfo implements Serializable {
	// 项目Id
	private Integer piId;
	// 项目名称
	private String piProjectName;
	// 开始时间
	private Date piStartDate;
	// 结束时间
	private Date piEndDate;
	// 项目状态
	// 1：已申报 2:审核中 3:已通过
	private Integer piStatus;

	public String getStatus() {
		switch (this.piStatus) {
		case 1:
			return "已申报";
		case 2:
			return "审核中";
		default:
			return "已通过";
		}
	}

	@Override
	public String toString() {
		return "ProjectInfo [piId=" + piId + ", piProjectName=" + piProjectName + ", piStartDate=" + piStartDate
				+ ", piEndDate=" + piEndDate + ", piStatus=" + piStatus + "]";
	}

	// alt+shift+s

	public Integer getPiId() {
		return piId;
	}

	public void setPiId(Integer piId) {
		this.piId = piId;
	}

	public String getPiProjectName() {
		return piProjectName;
	}

	public void setPiProjectName(String piProjectName) {
		this.piProjectName = piProjectName;
	}

	public Date getPiStartDate() {
		return piStartDate;
	}

	public void setPiStartDate(Date piStartDate) {
		this.piStartDate = piStartDate;
	}

	public Date getPiEndDate() {
		return piEndDate;
	}

	public void setPiEndDate(Date piEndDate) {
		this.piEndDate = piEndDate;
	}

	public Integer getPiStatus() {
		return piStatus;
	}

	public void setPiStatus(Integer piStatus) {
		this.piStatus = piStatus;
	}

}
