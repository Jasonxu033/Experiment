package com.yuhq.dpm.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuhq.dpm.model.ProjectInfo;
import com.yuhq.dpm.service.ProjectInfoService;

/**
 * 项目信息 控制器
 */
@Controller
@RequestMapping("/projectInfo")
public class ProjectInfoController {

	@Resource
	private ProjectInfoService projectInfoService;

	@RequestMapping("/show")
	public String show(Model model) throws Exception {
		// 查询数据
		List<ProjectInfo> projectInfoList = //
				projectInfoService.findAll();
		// 传递到view视图
		model.addAttribute("projectInfoList", projectInfoList);
		return "projectInfo/show";
	}

	@RequestMapping("/remove")
	public String remove(Integer piId) throws Exception {
		// 删除
		projectInfoService.remove(piId);
		return "redirect:/projectInfo/show";
	}

	@RequestMapping("/save")
	public String save(ProjectInfo projectInfo, String startDate, String endDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		projectInfo.setPiStartDate(sdf.parse(startDate));
		projectInfo.setPiEndDate(sdf.parse(endDate));
		projectInfoService.save(projectInfo);
		return "redirect:/projectInfo/show";
	}

	@RequestMapping("/modify")
	public String modify(ProjectInfo projectInfo, String startDate, String endDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		projectInfo.setPiStartDate(sdf.parse(startDate));
		projectInfo.setPiEndDate(sdf.parse(endDate));
		projectInfoService.modify(projectInfo);
		return "redirect:/projectInfo/show";
	}

	@RequestMapping("/toModify")
	public String toModify(Integer piId, Model model) {
		ProjectInfo projectInfo = projectInfoService.findByPiId(piId);
		model.addAttribute("projectInfo", projectInfo);
		return "projectInfo/modify";
	}

}
