package com.yuhq.dpm.mapper;

import java.util.List;

import com.yuhq.dpm.model.ProjectInfo;

public interface ProjectInfoMapper {

	// 查询全部项目信息
	public List<ProjectInfo> fingAll();

	// 添加
	public Integer save(ProjectInfo projectInfo);

	// 更新
	public Integer modify(ProjectInfo projectInfo);

	// 删除
	public Integer remove(Integer piId);

	public ProjectInfo findByPiId(Integer piId);

}
