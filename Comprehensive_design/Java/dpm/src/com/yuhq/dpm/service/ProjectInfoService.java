package com.yuhq.dpm.service;

import java.util.List;

import com.yuhq.dpm.model.ProjectInfo;

public interface ProjectInfoService {

	// 查询全部数据
	public List<ProjectInfo> findAll() throws Exception;

	// 添加数据
	public Boolean save(ProjectInfo projectInfo) throws Exception;

	// 更新
	public Boolean modify(ProjectInfo projectInfo) throws Exception;

	// 删除
	public Boolean remove(Integer piId) throws Exception;

	//根据Id查询信息
	public ProjectInfo findByPiId(Integer piId);

}
