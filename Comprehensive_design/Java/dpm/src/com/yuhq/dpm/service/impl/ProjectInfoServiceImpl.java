package com.yuhq.dpm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.yuhq.dpm.mapper.ProjectInfoMapper;
import com.yuhq.dpm.model.ProjectInfo;
import com.yuhq.dpm.service.ProjectInfoService;

import cn.jiguang.common.utils.StringUtils;

/**
 * 项目信息 业务逻辑
 */
@Service("projectInfoService")
public class ProjectInfoServiceImpl implements ProjectInfoService {

	@Resource
	private ProjectInfoMapper projectInfoMapper;

	@Override
	public List<ProjectInfo> findAll() throws Exception {
		// TODO Auto-generated method stub
		return projectInfoMapper.fingAll();
	}

	@Override
	public Boolean save(ProjectInfo projectInfo) throws Exception {
		// TODO Auto-generated method stub
		if (StringUtils.isEmpty(projectInfo.getPiProjectName())) {
			System.out.println("项目名称不能为空");
			return false;
		}

		Integer result = projectInfoMapper.save(projectInfo);
		return result > 0;
	}

	@Override
	public Boolean modify(ProjectInfo projectInfo) throws Exception {
		// 断言 项目Id不为空
		/// import org.springframework.util.Assert;
		Assert.notNull(projectInfo.getPiId(), "项目Id不能为空");
		Integer result = projectInfoMapper.modify(projectInfo);
		return result > 0;
	}

	@Override
	public Boolean remove(Integer piId) throws Exception {
		// 断言 项目Id不为空
		Assert.notNull(piId, "项目Id不能为空");
		Integer result = projectInfoMapper.remove(piId);
		return result > 0;
	}

	@Override
	public ProjectInfo findByPiId(Integer piId) {
		Assert.notNull(piId, "项目Id不能为空");
		return projectInfoMapper.findByPiId(piId);
	}

}
