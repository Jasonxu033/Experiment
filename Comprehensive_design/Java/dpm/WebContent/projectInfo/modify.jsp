<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加数据</title>
</head>
<body>

	<form action="${pageContext.request.contextPath}/projectInfo/modify">
		<input type="hidden" value="${projectInfo.piId }" name="piId">
		项目名称:<input type="text" name="piProjectName"
			value="${projectInfo.piProjectName }"> <br />
		开始时间(yyyy-MM-dd):<input type="text" name="startDate"
			value="<fmt:formatDate value='${projectInfo.piStartDate }'
								pattern="yyyy-MM-dd" />"><br /> 结束时间(yyyy-MM-dd):<input
			type="text" name="endDate" value="<fmt:formatDate value='${projectInfo.piEndDate }'
								pattern="yyyy-MM-dd" />"><br />
		状态: <select name="piStatus">
			<option value="1" ${projectInfo.piStatus==1?"selected":"" }>已申报</option>
			<option value="2"  ${projectInfo.piStatus==2?"selected":"" }>审核中</option>
			<option value="3"  ${projectInfo.piStatus==3?"selected":"" }>已通过</option>
		</select><br /> <input type="submit" value="提交">
	</form>

</body>
</html>