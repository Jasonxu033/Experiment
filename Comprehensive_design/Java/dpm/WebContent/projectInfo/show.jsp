<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户信息列表</title>
<style type="text/css">
div {
	width: 800px;
	height: 400px;
	border: 1px solid black;
	margin: 0 auto;
	text-align: center;
}

table {
	width: 100%;
}
</style>
</head>
<body>
	<div>
		<h1>显示用户信息列表</h1>
		<a href="${pageContext.request.contextPath}/projectInfo/save.jsp">添加</a>
		<table border="1">
			<thead>
				<tr>
					<th>项目名称</th>
					<th>开始时间</th>
					<th>结束时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="projectInfo" items="${projectInfoList }">
					<tr>
						<td>${projectInfo.piProjectName }</td>
						<td><fmt:formatDate value="${projectInfo.piStartDate }"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><fmt:formatDate value="${projectInfo.piEndDate }"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>${projectInfo.status }</td>
						<td>
							<a href="${pageContext.request.contextPath}/projectInfo/toModify?piId=${projectInfo.piId}">更新</a>
							<a href="${pageContext.request.contextPath}/projectInfo/remove?piId=${projectInfo.piId}">删除</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>