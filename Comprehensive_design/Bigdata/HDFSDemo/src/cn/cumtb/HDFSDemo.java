package cn.cumtb;

import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class HDFSDemo {
	// update file
	// hdfs dfs -put [filepath] [dispath]
	public static void putData(String src, String dst) throws IOException{
		//1. get the config info
		Configuration conf = new Configuration();
		
		//2. get the fileSystem object
		FileSystem hdfs = FileSystem.get(conf);
		
		//3. operation
		Path srcPath = new Path(src);  //mu biao lu jing
		Path dstPath = new Path(dst);  //ben di lu jing
		hdfs.copyFromLocalFile(srcPath, dstPath);
		
		//4. close
		hdfs.close();
		
	}
	
	//rename, move
	//hdfs dfs -mv [src] [dst]
	public static void reName(String src, String dst) throws IOException{
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		
		hdfs.rename(new Path(src), new Path(dst));
		hdfs.close();
	}
	
	//view the file info
	//hdfs dfs -mv [src]
	public static void catFile(String src, String dst) throws IOException{
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
			
		InputStream inputStream = null;
		inputStream = hdfs.open(new Path(src));
		IOUtils.copyBytes(inputStream, System.out, 1024, false);
		IOUtils.closeStream(inputStream);
	}	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			HDFSDemo.putData("/opt/datas/cat.txt", "/input");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
