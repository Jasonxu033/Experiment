#pragma once
#include<math.h>
//#include "GreyStretch.h"

namespace Image33 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Form1 摘要
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		//GreyStretch greystretch;
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: 在此处添加构造函数代码
			//
		}

	protected:
		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  thresholdSegmentationToolStripMenuItem;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::ToolStripMenuItem^  迭代阈值分割ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  lOG分割ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  最大熵分割ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  边缘检测ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sobelToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  prewitteeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  laplacianToolStripMenuItem;



	private:
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->边缘检测ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sobelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->prewitteeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->laplacianToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->thresholdSegmentationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->迭代阈值分割ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->lOG分割ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->最大熵分割ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->fileToolStripMenuItem, 
				this->边缘检测ToolStripMenuItem, this->thresholdSegmentationToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
			this->menuStrip1->Size = System::Drawing::Size(766, 25);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->openToolStripMenuItem, 
				this->saveToolStripMenuItem});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 21);
			this->fileToolStripMenuItem->Text = L"file";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(106, 22);
			this->openToolStripMenuItem->Text = L"open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(106, 22);
			this->saveToolStripMenuItem->Text = L"save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveToolStripMenuItem_Click);
			// 
			// 边缘检测ToolStripMenuItem
			// 
			this->边缘检测ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->sobelToolStripMenuItem, 
				this->prewitteeToolStripMenuItem, this->laplacianToolStripMenuItem});
			this->边缘检测ToolStripMenuItem->Name = L"边缘检测ToolStripMenuItem";
			this->边缘检测ToolStripMenuItem->Size = System::Drawing::Size(68, 21);
			this->边缘检测ToolStripMenuItem->Text = L"边缘检测";
			// 
			// sobelToolStripMenuItem
			// 
			this->sobelToolStripMenuItem->Name = L"sobelToolStripMenuItem";
			this->sobelToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->sobelToolStripMenuItem->Text = L"sobel";
			this->sobelToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::sobelToolStripMenuItem_Click);
			// 
			// prewitteeToolStripMenuItem
			// 
			this->prewitteeToolStripMenuItem->Name = L"prewitteeToolStripMenuItem";
			this->prewitteeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->prewitteeToolStripMenuItem->Text = L"prewitt";
			this->prewitteeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::prewitteeToolStripMenuItem_Click);
			// 
			// laplacianToolStripMenuItem
			// 
			this->laplacianToolStripMenuItem->Name = L"laplacianToolStripMenuItem";
			this->laplacianToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->laplacianToolStripMenuItem->Text = L"laplacian";
			this->laplacianToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::laplacianToolStripMenuItem_Click);
			// 
			// thresholdSegmentationToolStripMenuItem
			// 
			this->thresholdSegmentationToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->迭代阈值分割ToolStripMenuItem, 
				this->lOG分割ToolStripMenuItem, this->最大熵分割ToolStripMenuItem});
			this->thresholdSegmentationToolStripMenuItem->Name = L"thresholdSegmentationToolStripMenuItem";
			this->thresholdSegmentationToolStripMenuItem->Size = System::Drawing::Size(68, 21);
			this->thresholdSegmentationToolStripMenuItem->Text = L"阈值分割";
			this->thresholdSegmentationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::thresholdSegmentationToolStripMenuItem_Click);
			// 
			// 迭代阈值分割ToolStripMenuItem
			// 
			this->迭代阈值分割ToolStripMenuItem->Name = L"迭代阈值分割ToolStripMenuItem";
			this->迭代阈值分割ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->迭代阈值分割ToolStripMenuItem->Text = L"迭代阈值分割";
			this->迭代阈值分割ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::迭代阈值分割ToolStripMenuItem_Click);
			// 
			// lOG分割ToolStripMenuItem
			// 
			this->lOG分割ToolStripMenuItem->Name = L"lOG分割ToolStripMenuItem";
			this->lOG分割ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->lOG分割ToolStripMenuItem->Text = L"LOG分割";
			this->lOG分割ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::lOG分割ToolStripMenuItem_Click);
			// 
			// 最大熵分割ToolStripMenuItem
			// 
			this->最大熵分割ToolStripMenuItem->Name = L"最大熵分割ToolStripMenuItem";
			this->最大熵分割ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->最大熵分割ToolStripMenuItem->Text = L"最大熵分割";
			this->最大熵分割ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::最大熵分割ToolStripMenuItem_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox1->Location = System::Drawing::Point(4, 19);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(318, 294);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &Form1::pictureBox1_Click);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox2->Location = System::Drawing::Point(4, 19);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(342, 294);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 2;
			this->pictureBox2->TabStop = false;
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(23, 59);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(326, 313);
			this->groupBox1->TabIndex = 3;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"原图像";
			this->groupBox1->Enter += gcnew System::EventHandler(this, &Form1::groupBox1_Enter);
			// 
			// groupBox2
			// 
			this->groupBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(353, 59);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(367, 313);
			this->groupBox2->TabIndex = 4;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"结果图";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(766, 424);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	public:
		typedef  unsigned char BYTE;
		Bitmap^  original_bmap;
		Bitmap^  processed_bmap;

		int nWidth;                 //图像宽
		int nHeight;                //图像高

		BYTE **oriImage;           //原图灰度
		BYTE **proImage;          //处理后灰度

		BYTE ***pSrcImage;           
		BYTE ***pDestImage; 

		///////////////////////////////////////////
		int Thresh_value(double histArray[256])
		{
			double u0,u1;   //c0 c1的均值
			int count0;     //c0组的像素数
			int MaxT = 0, MaxT0 = 255;     //记录当前阈值，前次阈值
			double E0 = 1;   //两个阈值之差
			int i;
			//统计直方图的像素的个数，存放在sum 中
			int sum=0;
			for(i=0;  i<256;  i++)
			{
				sum = sum + histArray[i];
			}
			while(fabs ( (double)(MaxT- MaxT0)) > E0)
			{
				MaxT0 = MaxT;
				//计算 MaxT 时，c0组的均值
				u0=0;
				count0=0;
				for(i=0;  i<=MaxT;  i++)
				{
					u0 += i*histArray[i];
					count0 += histArray[i];
				}
				if(count0 == 0)
					u0 = 0;
				else
				{
					u0 = u0/count0;
				}
				//计算MaxT时，c1组的均值
				u1 = 0;
				for(i=MaxT+1; i<256 ; i++)
					u1+= i*histArray[i];
				if(sum - count0 == 0)
					u1 = 0;
				else
				{
					u1 = u1/(sum-count0);
				}
				//计算阈值
				MaxT = (u1+u0)/2;
			}
			return MaxT;

		}
	
		/////////////////////////////////////////
		int  MaxEntropy (double histArray[256])
		{
			double E0,E1;  //两类熵
			double pt;  // 0 组的概率和
			double p[256];  //每个灰度级的概率
			double Entropy [256];  //保存每个阈值的熵值和
			int i,t,MaxT=0;	
			double max=0.0;
			for( i=0; i<256; i++)
			{  
				Entropy [i] = 0.0;
				p[i] = 0.0;
			}
			for(i=0; i<256; i++)
			{
				p[i] = ( double)histArray[i]/(nHeight*nWidth);

			}

			pt = 0.0;
			for(t=0;t<256;t++)
			{                     
				E0  =  0;
				E1  =  0;
				pt=p[t]+pt;    //统计O组概率和 	
				for(i=0; i<=t; i++)
				{
					if(p[i] == 0 ) continue;
					E0+= -p[i]/pt*log(p[i]/pt);	
				}
				for(i=t+1; i<256; i++)
				{
					if(p[i] == 0 ) continue;
					E1+=  -p[i]/(1.0-pt)*log(p[i]/(1-pt));
				}
				Entropy[t]=E0+E1;
			}
			max=0;
			for(i=0;i<256;i++)//找到使熵值最大的灰度级
			{
				if(max<Entropy [i])  
				{		
					max = Entropy [i];
					MaxT = i; 
				}
			}
			return MaxT;
		}


		void Sobel(double **tenA,double **tenB,int tenNo,int nWidth, int nHeight,BYTE **formerImage,BYTE **imageT)
		{
			double **imageT1;
			double **imageT2;
			double sum = 0;
			imageT1 = new double *[nHeight];
			imageT2 = new double *[nHeight];
			for(int i=0;i<nHeight;i++)
			{
				imageT1[i]=new double[nWidth];
				imageT2[i]=new double[nWidth];
			}
			Template(tenA,tenNo,nWidth,nHeight,formerImage,imageT1);
			Template(tenB,tenNo,nWidth,nHeight,formerImage,imageT2);
			for( int i=0; i<nHeight; i++ )
			{
				for( int j=0; j<nWidth; j++)
				{
					sum= Math::Pow(imageT1[i][j]*imageT1[i][j] +imageT2[i][j]*imageT2[i][j],1);	         
					if(sum>255)      sum=255;		
					if(sum<0)        sum=0;	
					imageT[i][j] = (BYTE)sum;
				}
			}
			for( int i=0; i<nHeight; i++ )
			{
				delete []imageT1[i];
				delete []imageT2[i];
			}
			delete []imageT1;
			delete []imageT2;
		}

		void Template(double **ten,int tenNo,int nWidth, int nHeight,BYTE **formerImage,double **imageT)
		{
			//模板四个方向上的宽度变量
			int No=tenNo/2;

			//模板四个方向和原图形的重叠量
			int front=0;
			int back=0;
			int left=0;
			int right=0;

			double sum=0;
			for(int i=0;i<nHeight;i++)
			{
				for(int j=0;j<nWidth;j++)
				{
					//求模板四个方向和原图形的重叠量
					if(i<No)                         front=i;	
					else                                front=No;	
					if(nHeight-i-1<No)      back=nHeight-i-1;	
					else                                back=No;	
					if(j<No)                         right=j;	
					else                                right=No;
					if(nWidth-j-1<No)       left=nWidth-j-1;
					else                                left=No;
					sum=0;
					//求取当前点区域和模板卷积的结果
					for(int h=-front;h<=back;h++)
					{
						for(int k=-right;k<=left;k++)
						{			   
							sum+=ten[h+No][k+No]*((double)formerImage[i+h][j+k]);
						}
					}

					//存储处理结果
					imageT[i][j]=sum;
				}	
			}
		} 	

		void display_image(Bitmap^bmap, BYTE **grey)
		{
			for(int i=0; i<nHeight; i++)
			{
				for(int j=0; j<nWidth; j++)
				{
					bmap->SetPixel(j,i,Color::FromArgb(grey[i][j],grey[i][j],grey[i][j])); 
				}
			}
		}
		//统计直方图
		void computeHistogram(double m_histArray[256])
		{

			int temp;
			for(int i=0;i<256;i++)
				m_histArray[i] = 0;
			for(int i=0;i<nHeight;i++)
			{
				for(int j=0; j<nWidth; j++)
				{
					temp = oriImage[i][j];
					m_histArray[temp]++;
				}
			}

		}
		void Laplace(double **tenA, int tenNo, int nWidth, int nHeight, BYTE **formerImage, BYTE **imageT)
		{
			double **imageT1;
			double **imageT2;
			double sum = 0;
			imageT1 = new double *[nHeight];
			imageT2 = new double *[nHeight];
			for(int i=0;i<nHeight;i++)
			{
				imageT1[i]=new double[nWidth];
				imageT2[i]=new double[nWidth];
			}
			Template(tenA,tenNo,nWidth,nHeight,formerImage,imageT1);
			//Template(tenB,tenNo,nWidth,nHeight,formerImage,imageT2);
			for( int i=0; i<nHeight; i++ )
			{
				for( int j=0; j<nWidth; j++)
				{
					//sum= Math::Pow(imageT1[i][j]*imageT1[i][j]+imageT2[i][j]*imageT2[i][j],1);	  
					sum = imageT1[i][j];
					if(sum>255)      sum=255;		
					if(sum<0)        sum=0;	
					imageT[i][j] = (BYTE)sum;
				}
			}
			for( int i=0; i<nHeight; i++ )
			{
				delete []imageT1[i];
				delete []imageT2[i];
			}
			delete []imageT1;
			delete []imageT2;
		}

		////////////////
		void GetGuassFilter(double *dest, double sigma, int N)
		{
			double **ftmp = new double*[2 * N + 1];
			int **tmp = new int*[2 * N + 1];
			for (int i = 0; i < 2 * N + 1; ++i)
			{
				ftmp[i] = new double [2 * N + 1]();
				tmp[i] = new int[2 * N + 1]();
			}

			for(int i = -N; i <= N; i++)
				for(int j = -N; j <= N; j++)
					ftmp[i+N][j+N] = exp(((-1.0)*(i*i+j*j)/2*sigma*sigma));
			int C = (int)(1/ftmp[0][0]+1.5);     //归一系数
			int avg = 0;
			for(int i = -N; i <= N; i++)
				for(int j = -N; j <= N; j++)
				{
					tmp[i+N][j+N] =  (int)(1/ftmp[i][j]*C+0.5);
					avg += tmp[i+N][j+N];
				}			
				for (int i = 0; i < (2 * N + 1); ++i)
				{
					for (int j = 0; j < (2 * N + 1) ;++j)
					{
						dest[i*(2 * N + 1)+j] = tmp[i][j]/avg;
					}
				}
		}

		//////////////////




#pragma endregion
	private: System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 SaveFileDialog ^ saveFile1 = gcnew SaveFileDialog ();
				 saveFile1 -> InitialDirectory = "E:\\";
				 saveFile1 -> Filter = "Bitmap Image|*.bmp|JPeg Image|*.jpg|Gif Image|*.gif";
				 saveFile1 -> Title = "Save an Image File";
				 saveFile1 -> ShowDialog ();
				 if ( saveFile1 ->FileName != " ")
				 {
					 switch( saveFile1->FilterIndex )
					 {
					 case 1:
						 this->processed_bmap->Save(saveFile1->   FileName , System:: Drawing :: Imaging::   ImageFormat::Jpeg);
						 break;
					 case 2:
						 this->processed_bmap->Save(saveFile1->FileName ,System ::Drawing :: Imaging :: ImageFormat ::Bmp);
						 break;

					 case 3:
						 this->processed_bmap->Save(saveFile1->FileName,System ::Drawing :: Imaging :: ImageFormat ::Gif);
						 break;
					 } //end switch
				 } // end if
			 }
	private: System::Void thresholdSegmentationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void groupBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 OpenFileDialog^ openFile1 = gcnew OpenFileDialog ( );
				 openFile1->InitialDirectory = "E:\\";
				 openFile1 ->Filter = "Bitmap Image(*.bmp)|*.bmp|Image(*.jpeg)|*.jpg";
				 openFile1 ->RestoreDirectory = true;

				 if (openFile1 ->ShowDialog() == Windows::Forms::DialogResult ::OK) 
				 {
					 String^ fileName = openFile1 ->FileName->ToString();

					 original_bmap  = gcnew Bitmap ( fileName, true );
					 processed_bmap = gcnew Bitmap ( fileName, true );

					 nWidth  = original_bmap->Width;
					 nHeight = original_bmap->Height;
					 pictureBox1->Image = original_bmap;
					 pictureBox2->Image = processed_bmap;
					 pictureBox1->Refresh();
					 pictureBox2->Refresh();

					 oriImage = new BYTE*[nHeight];
					 proImage = new BYTE *[nHeight];

					 for ( int i = 0; i < nHeight; i++ )
					 {
						 oriImage[i] = new BYTE[nWidth];
						 proImage[i] = new BYTE[nWidth];
					 }
					 for ( int i = 0; i < nHeight; i++ )
					 {
						 for ( int j = 0; j < nWidth; j++ )
						 {
							 oriImage[i][j] = (BYTE)(original_bmap->GetPixel(j,i).R*0.299
								 + original_bmap->GetPixel(j,i).G*0.587  
								 + original_bmap->GetPixel(j,i).B*0.114);
						 }
					 }				 
				 } // end if
			 }
	private: System::Void 迭代阈值分割ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 double m_histArray[256];
				 computeHistogram(m_histArray);  //统计直方图得到灰度级数组
				 int threshvalue= Thresh_value(m_histArray);  //根据直方图，自适应算法获得阈值

				 for( int i=0; i<nHeight; i++)
					 for( int j=0; j<nWidth; j++)
					 {
						 if(oriImage[i][j]>threshvalue)
							 proImage[i][j] = 255;
						 else
							 proImage[i][j] = 0;
					 }

					 display_image(processed_bmap,proImage);
					 pictureBox2->Refresh();

			 }
	private: System::Void 最大熵分割ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 double m_histArray[256];
				 computeHistogram(m_histArray);//统计直方图得到灰度级数组
				 int threshvalue= MaxEntropy(m_histArray);//根据直方图，自适应算法获得阈值
				 for(int i=0;i<nHeight;i++)
					 for(int j=0;j<nWidth;j++)
					 {
						 if(oriImage[i][j]>threshvalue)
							 proImage[i][j]=255;
						 else
							 proImage[i][j]=0;
					 }

					 display_image(processed_bmap,proImage);
					 pictureBox2->Refresh();
			 }
	private: System::Void prewitteeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 double ten11[3][3] = {{-1.0/8.0,-1.0/8.0,-1.0/8.0},{0,0,0},{1.0/8.0,1.0/8.0,1.0/8.0}};
				 double ten22[3][3] = {{1.0/8.0,0,-1.0/8.0},{1.0/8.0,0,-1.0/8.0},{1.0/8.0,0,-1.0/8.0}}; 
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 ten2[i][j] = ten22[i][j];
					 }
				 }
				 Sobel(ten1, ten2, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 delete []ten2[i];
				 }
				 delete []ten1;
				 delete []ten2;
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
			 }

	private: System::Void sobelToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 double ten11[3][3] = {{-1.0/8.0,-2.0/8.0,-1.0/8.0},{0,0,0},{1.0/8.0,2.0/8.0,1.0/8.0}};
				 double ten22[3][3] = {{-1.0/8.0,0,1.0/8.0},{-2.0/8.0,0,2.0/8.0},{-1.0/8.0,0,1.0/8.0}}; 
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 ten2[i][j] = ten22[i][j];
					 }
				 }
				 Sobel(ten1, ten2, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 delete []ten2[i];
				 }
				 delete []ten1;
				 delete []ten2;
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
			 }
	private: System::Void laplacianToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 // 换汤不换药，直接换模板就行
				 double ten11[3][3] = {{-1.0/8.0, -1.0/8.0, -1.0/8.0},
				 {-1.0/8.0,  8.0/8.0, -1.0/8.0},
				 {-1.0/8.0, -1.0/8.0 ,-1.0/8.0}};

				 //double ten22[3][3] = {{    0   , -1.0/8.0,     0   },
				 //			             {-1.0/8.0,  4.0/8.0, -1.0/8.0},
				 //					     {    0   , -1.0/8.0,     0   }};
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 //ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 //ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 //ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Laplace(ten1, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 //delete []ten2[i];
				 }
				 delete []ten1;
				 //delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
			 }
	private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void lOG分割ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 ///////////////////
				 double ten11[5][5] = {{-2, -4, -4, -4, -2},
									   {-4, 0, 8, 0, -4},
									   {-4, 8, 24, 8, -4},
									   {-4, 0, 8, 0, -4},
									   {-2, -4, -4, -4, -2},};

				 double **ten1;
				 double **ten2;
				 int tenNo = 5;
				 ten1 = new double *[tenNo];
				 //ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 //ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 //ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Laplace(ten1, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 //delete []ten2[i];
				 }
				 delete []ten1;
				 //delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
				 ///////////////////
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
			 }
	};
}

