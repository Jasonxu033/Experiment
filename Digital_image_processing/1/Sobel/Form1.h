#pragma once

// 名称空间Sobel，所有代码都在这个空间里
namespace Soebl {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}
	
	//初始化图像的属性
	public:
	    // 所谓的BYTE类型在这里定义的
		typedef  unsigned char BYTE;
	    Bitmap^  original_bmap;
	    Bitmap^  processed_bmap;

	    int nWidth;                 //图像宽
	    int nHeight;                //图像高

	    BYTE **oriImage;            //原图灰度
		BYTE **proImage;            //处理后灰度
	
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::ToolStripMenuItem^  laplasseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  prewittToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::ToolStripMenuItem^  laplasse改ToolStripMenuItem;

	public: 
	private: System::Windows::Forms::PictureBox^  pictureBox2;		 

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  文件ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  打开ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  保存ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  边缘检测ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sobelToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

// 绘制菜单
#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->文件ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->打开ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->保存ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->边缘检测ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sobelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->laplasseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->prewittToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->laplasse改ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->文件ToolStripMenuItem, 
				this->边缘检测ToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(829, 25);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			this->menuStrip1->ItemClicked += gcnew System::Windows::Forms::ToolStripItemClickedEventHandler(this, &Form1::menuStrip1_ItemClicked);
			// 
			// 文件ToolStripMenuItem
			// 
			this->文件ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->打开ToolStripMenuItem, 
				this->保存ToolStripMenuItem});
			this->文件ToolStripMenuItem->Name = L"文件ToolStripMenuItem";
			this->文件ToolStripMenuItem->Size = System::Drawing::Size(44, 21);
			this->文件ToolStripMenuItem->Text = L"文件";
			// 
			// 打开ToolStripMenuItem
			// 
			this->打开ToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"打开ToolStripMenuItem.Image")));
			this->打开ToolStripMenuItem->Name = L"打开ToolStripMenuItem";
			this->打开ToolStripMenuItem->Size = System::Drawing::Size(100, 22);
			this->打开ToolStripMenuItem->Text = L"打开";
			this->打开ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::打开ToolStripMenuItem_Click);
			// 
			// 保存ToolStripMenuItem
			// 
			this->保存ToolStripMenuItem->Name = L"保存ToolStripMenuItem";
			this->保存ToolStripMenuItem->Size = System::Drawing::Size(100, 22);
			this->保存ToolStripMenuItem->Text = L"保存";
			this->保存ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::保存ToolStripMenuItem_Click);
			// 
			// 边缘检测ToolStripMenuItem
			// 
			this->边缘检测ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->sobelToolStripMenuItem, 
				this->laplasseToolStripMenuItem, this->prewittToolStripMenuItem, this->laplasse改ToolStripMenuItem});
			this->边缘检测ToolStripMenuItem->Name = L"边缘检测ToolStripMenuItem";
			this->边缘检测ToolStripMenuItem->Size = System::Drawing::Size(68, 21);
			this->边缘检测ToolStripMenuItem->Text = L"边缘检测";
			this->边缘检测ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::边缘检测ToolStripMenuItem_Click);
			// 
			// sobelToolStripMenuItem
			// 
			this->sobelToolStripMenuItem->Name = L"sobelToolStripMenuItem";
			this->sobelToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->sobelToolStripMenuItem->Text = L"Sobel";
			this->sobelToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::sobelToolStripMenuItem_Click);
			// 
			// laplasseToolStripMenuItem
			// 
			this->laplasseToolStripMenuItem->Name = L"laplasseToolStripMenuItem";
			this->laplasseToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->laplasseToolStripMenuItem->Text = L"Laplasse";
			this->laplasseToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::laplasseToolStripMenuItem_Click);
			// 
			// prewittToolStripMenuItem
			// 
			this->prewittToolStripMenuItem->Name = L"prewittToolStripMenuItem";
			this->prewittToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->prewittToolStripMenuItem->Text = L"Prewitt";
			this->prewittToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::prewittToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->AutoSize = true;
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(27, 39);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(366, 345);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"原图";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox1->Location = System::Drawing::Point(6, 20);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(354, 319);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &Form1::pictureBox1_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox2->AutoSize = true;
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(426, 39);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(366, 345);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"处理图";
			this->groupBox2->Enter += gcnew System::EventHandler(this, &Form1::groupBox2_Enter);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox2->Location = System::Drawing::Point(6, 20);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(354, 319);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->Click += gcnew System::EventHandler(this, &Form1::pictureBox2_Click);
			// 
			// groupBox3
			// 
			this->groupBox3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox3->AutoSize = true;
			this->groupBox3->Location = System::Drawing::Point(393, 39);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(27, 339);
			this->groupBox3->TabIndex = 3;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L" ";
			this->groupBox3->Enter += gcnew System::EventHandler(this, &Form1::groupBox3_Enter);
			// 
			// laplasse改ToolStripMenuItem
			// 
			this->laplasse改ToolStripMenuItem->Name = L"laplasse改ToolStripMenuItem";
			this->laplasse改ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->laplasse改ToolStripMenuItem->Text = L"Laplasse(改)";
			this->laplasse改ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::laplasse改ToolStripMenuItem_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(829, 396);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->groupBox2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	// 打开图片实现
	private: System::Void 打开ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 // 弹出选择文件夹框
				 OpenFileDialog^ openFile1 = gcnew OpenFileDialog ( );
				 // 默认图片读取路径
				 openFile1->InitialDirectory = "C:\\Users\\Jason\\Pictures";
				 // 可以读取bmp，jpg，gif
				 openFile1 ->Filter = "Bitmap Image(*.jpeg)|*.jpg|Image(*.bmp)|*.bmp|Image(*.gif)|*.gif";
				 openFile1 ->RestoreDirectory = true;

				 if (openFile1->ShowDialog() == Windows::Forms::DialogResult::OK) 
				 {
					 // 文件名
					 String^ fileName = openFile1 ->FileName->ToString();

					 // 初始化原图和处理后的图
					 original_bmap  = gcnew Bitmap ( fileName, true );
					 processed_bmap = gcnew Bitmap ( fileName, true );

					 // 初始化图像的长宽
					 nWidth  = original_bmap->Width;
					 nHeight = original_bmap->Height;
					 
					 // 将初始的图片写入两个picBOX中
					 pictureBox1->Image = original_bmap;
					 pictureBox2->Image = processed_bmap;
					 pictureBox1->Refresh();
					 pictureBox2->Refresh();

					 // 为原始和处理后的图像申请内存
					 // 先申请了nHeight行，再给每行申请nWidth列
					 oriImage = new BYTE *[nHeight];
					 proImage = new BYTE *[nHeight];
					 for ( int i = 0; i < nHeight; i++ )
					 {
						 oriImage[i] = new BYTE[nWidth];
						 proImage[i] = new BYTE[nWidth];
					 }

					 // 将图像灰度化 New=0.299*R+0.587*G+0.114*B
					 for ( int i = 0; i < nHeight; i++ )
					 {
						 for ( int j = 0; j < nWidth; j++ )
						 {
							 oriImage[i][j] = (BYTE)(original_bmap->GetPixel(j,i).R*0.299
												   + original_bmap->GetPixel(j,i).G*0.587  
												   + original_bmap->GetPixel(j,i).B*0.114);
						 }
					 }
				 }
			 }
	// 保存图片实现
	private: System::Void 保存ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 // 弹出选择文件夹框
				 SaveFileDialog ^ saveFile1 = gcnew SaveFileDialog ();
				 // 默认文件夹
				 saveFile1 -> InitialDirectory = "E:\\";
				 // 支持的存储格式
				 saveFile1 -> Filter = "Bitmap Image|*.bmp|JPeg Image|*.jpg|Gif Image|*.gif";
				 saveFile1 -> Title = "Save an Image File";
				 saveFile1 -> ShowDialog ();
				 if ( saveFile1 ->FileName != " ")
				 {
					 // 匹配保存的文件格式
					 switch( saveFile1->FilterIndex )
					 {
						 case 1:
							 this->processed_bmap->Save(saveFile1->FileName, System:: Drawing :: Imaging:: 
								 ImageFormat::Jpeg);
							 break;
						 case 2:
							 this->processed_bmap->Save(saveFile1->FileName, System ::Drawing :: Imaging :: 
								 ImageFormat ::Bmp);
							 break;
						 case 3:
							 this->processed_bmap->Save(saveFile1->FileName, System ::Drawing :: Imaging :: 
								 ImageFormat ::Gif);
							 break;
					 }
				 }
			 }
	// Sobel算子实现
	private: System::Void sobelToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 // 模板
				 double ten11[3][3] = {{-1.0/8.0, -2.0/8.0, -1.0/8.0},
									   {0       , 0       , 0       },
									   {1.0/8.0 , 2.0/8.0 , 1.0/8.0 }};
				 
				 double ten22[3][3] = {{-1.0/8.0, 0       , 1.0/8.0 },
									   {-2.0/8.0, 0       , 2.0/8.0 },
									   {-1.0/8.0, 0       , 1.0/8.0 }};
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Sobel(ten1, ten2, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 delete []ten2[i];
				 }
				 delete []ten1;
				 delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
			 }
	private: System::Void menuStrip1_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^  e) {
			 }
	private: System::Void groupBox2_Enter(System::Object^  sender, System::EventArgs^  e) {
			 }

void Sobel(double **tenA, double **tenB, int tenNo, int nWidth, int nHeight, BYTE **formerImage, BYTE **imageT)
{
	double **imageT1;
	double **imageT2;
	double sum = 0;
	imageT1 = new double *[nHeight];
	imageT2 = new double *[nHeight];
	for(int i=0;i<nHeight;i++)
	{
		imageT1[i]=new double[nWidth];
		imageT2[i]=new double[nWidth];
	}
	Template(tenA,tenNo,nWidth,nHeight,formerImage,imageT1);
	Template(tenB,tenNo,nWidth,nHeight,formerImage,imageT2);
	for( int i=0; i<nHeight; i++ )
	{
		for( int j=0; j<nWidth; j++)
		{
		           sum= Math::Pow(imageT1[i][j]*imageT1[i][j]+imageT2[i][j]*imageT2[i][j],1);	           
				   if(sum>255)      sum=255;		
		           if(sum<0)        sum=0;	
		           imageT[i][j] = (BYTE)sum;
		}
	}
	for( int i=0; i<nHeight; i++ )
	{
	   	 delete []imageT1[i];
	  	 delete []imageT2[i];
	}
   delete []imageT1;
   delete []imageT2;
}

void Laplace(double **tenA, int tenNo, int nWidth, int nHeight, BYTE **formerImage, BYTE **imageT)
{
	double **imageT1;
	double **imageT2;
	double sum = 0;
	imageT1 = new double *[nHeight];
	imageT2 = new double *[nHeight];
	for(int i=0;i<nHeight;i++)
	{
		imageT1[i]=new double[nWidth];
		imageT2[i]=new double[nWidth];
	}
	Template(tenA,tenNo,nWidth,nHeight,formerImage,imageT1);
	//Template(tenB,tenNo,nWidth,nHeight,formerImage,imageT2);
	for( int i=0; i<nHeight; i++ )
	{
		for( int j=0; j<nWidth; j++)
		{
		           //sum= Math::Pow(imageT1[i][j]*imageT1[i][j]+imageT2[i][j]*imageT2[i][j],1);	  
				   sum = imageT1[i][j];
				   if(sum>255)      sum=255;		
		           if(sum<0)        sum=0;	
		           imageT[i][j] = (BYTE)sum;
		}
	}
	for( int i=0; i<nHeight; i++ )
	{
	   	 delete []imageT1[i];
	  	 delete []imageT2[i];
	}
   delete []imageT1;
   delete []imageT2;
}

void display_image(Bitmap^ bmap, BYTE **grey)                                                        
{
	 for(int i=0;i<nHeight;i++)
      {
		for(int j=0;j<nWidth;j++)
		 {						  
		       bmap->SetPixel(j,i,Color::FromArgb(grey[i][j],grey[i][j],grey[i][j]));  
		}
	}
}

void Template(double **ten, int tenNo, int nWidth, int nHeight, BYTE **formerImage, double **imageT)
{
	//模板四个方向上的宽度变量
	int No=tenNo/2;

	//模板四个方向和原图形的重叠量
	int front=0;
	int back=0;
	int left=0;
	int right=0;

	double sum=0;

	for(int i=0;i<nHeight;i++)
	{
		for(int j=0;j<nWidth;j++)
		{
			//求模板四个方向和原图形的重叠量
			if(i<No)                front=i;	
			else                    front=No;	
			if(nHeight-i-1<No)      back=nHeight-i-1;	
			else                    back=No;	
			if(j<No)                right=j;	
			else                    right=No;
			if(nWidth-j-1<No)       left=nWidth-j-1;
			else                    left=No;
			sum=0;
			
			//求取当前点区域和模板卷积的结果
			for(int h=-front;h<=back;h++)
			{
				for(int k=-right;k<=left;k++)
				{			   
					sum+=ten[h+No][k+No]*((double)formerImage[i+h][j+k]);
				}
			}
	        
			//存储处理结果
			imageT[i][j]=sum;
		}	
	}
} 

private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void pictureBox2_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void 边缘检测ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		 }

// 拉普拉斯算子实现
private: System::Void laplasseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 // 换汤不换药，直接换模板就行
				 double ten11[3][3] = {{-1.0/8.0, -1.0/8.0, -1.0/8.0},
									   {-1.0/8.0,  8.0/8.0, -1.0/8.0},
									   {-1.0/8.0, -1.0/8.0 ,-1.0/8.0}};
				 
				 //double ten22[3][3] = {{    0   , -1.0/8.0,     0   },
				 //			             {-1.0/8.0,  4.0/8.0, -1.0/8.0},
				 //					     {    0   , -1.0/8.0,     0   }};
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 //ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 //ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 //ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Laplace(ten1, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 //delete []ten2[i];
				 }
				 delete []ten1;
				 //delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
		 }

// Prewitt算子实现
private: System::Void prewittToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 // 换汤不换药，直接换模板就行
				 double ten11[3][3] = {{-1.0/8.0, -1.0/8.0, -1.0/8.0},
									   {0       , 0       , 0       },
									   {1.0/8.0 , 1.0/8.0 , 1.0/8.0 }};
				 
				 double ten22[3][3] = {{-1.0/8.0, 0       , 1.0/8.0 },
									   {-1.0/8.0, 0       , 1.0/8.0 },
									   {-1.0/8.0, 0       , 1.0/8.0 }};
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Sobel(ten1, ten2, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 delete []ten2[i];
				 }
				 delete []ten1;
				 delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
		 }
private: System::Void groupBox3_Enter(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void laplasse改ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 // 换汤不换药，直接换模板就行
				 double ten11[3][3] = {{-1.0/8.0, -1.0/8.0, -1.0/8.0},
									   {-1.0/8.0,  8.0/8.0, -1.0/8.0},
									   {-1.0/8.0, -1.0/8.0 ,-1.0/8.0}};
				 
				 double ten22[3][3] = {{    0   , -1.0/8.0,     0   },
				 			             {-1.0/8.0,  4.0/8.0, -1.0/8.0},
				 					     {    0   , -1.0/8.0,     0   }};
				 double **ten1;
				 double **ten2;
				 int tenNo = 3;
				 ten1 = new double *[tenNo];
				 ten2 = new double *[tenNo];
				 for( int i=0; i<tenNo; i++ )
				 {
					 ten1[i] = new double [tenNo];
					 ten2[i] = new double [tenNo];
				 }
				 for( int i=0; i<tenNo; i++ )
				 {
					 for( int j=0; j<tenNo; j++ )
					 {		
						 ten1[i][j] = ten11[i][j];
						 ten2[i][j] = ten22[i][j];
					 }
				 }
				 // 调用
				 Sobel(ten1, ten2, tenNo, nWidth, nHeight, oriImage, proImage);
				 for( int i=0; i<tenNo; i++)
				 {
					 delete []ten1[i];
					 delete []ten2[i];
				 }
				 delete []ten1;
				 delete []ten2;
				 // 调用
				 display_image( processed_bmap, proImage );
				 pictureBox2->Refresh();
		 }
};
}

