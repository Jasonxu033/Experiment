#pragma once
#include "GreyStretch.h"

namespace tuxiang4 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Form1 摘要
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		GreyStretch greystretch;

		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: 在此处添加构造函数代码
			//
		}

	protected:
		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected: 
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  文件ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  打开ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  保存ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  图像增强ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  灰度拉伸ToolStripMenuItem;

	private:
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->文件ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->打开ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->保存ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->图像增强ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->灰度拉伸ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->BeginInit();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->Controls->Add(this->pictureBox2);
			this->groupBox1->Location = System::Drawing::Point(408, 54);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(291, 234);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"处理图";
			// 
			// pictureBox2
			// 
			this->pictureBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox2->Location = System::Drawing::Point(6, 12);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(279, 216);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			// 
			// groupBox2
			// 
			this->groupBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox2->Controls->Add(this->pictureBox1);
			this->groupBox2->Location = System::Drawing::Point(27, 54);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(283, 222);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"原图像";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->pictureBox1->Location = System::Drawing::Point(6, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(271, 204);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->文件ToolStripMenuItem, 
				this->图像增强ToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(711, 25);
			this->menuStrip1->TabIndex = 2;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// 文件ToolStripMenuItem
			// 
			this->文件ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->打开ToolStripMenuItem, 
				this->保存ToolStripMenuItem});
			this->文件ToolStripMenuItem->Name = L"文件ToolStripMenuItem";
			this->文件ToolStripMenuItem->Size = System::Drawing::Size(44, 21);
			this->文件ToolStripMenuItem->Text = L"文件";
			// 
			// 打开ToolStripMenuItem
			// 
			this->打开ToolStripMenuItem->Name = L"打开ToolStripMenuItem";
			this->打开ToolStripMenuItem->Size = System::Drawing::Size(100, 22);
			this->打开ToolStripMenuItem->Text = L"打开";
			this->打开ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::打开ToolStripMenuItem_Click);
			// 
			// 保存ToolStripMenuItem
			// 
			this->保存ToolStripMenuItem->Name = L"保存ToolStripMenuItem";
			this->保存ToolStripMenuItem->Size = System::Drawing::Size(100, 22);
			this->保存ToolStripMenuItem->Text = L"保存";
			this->保存ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::保存ToolStripMenuItem_Click);
			// 
			// 图像增强ToolStripMenuItem
			// 
			this->图像增强ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->灰度拉伸ToolStripMenuItem});
			this->图像增强ToolStripMenuItem->Name = L"图像增强ToolStripMenuItem";
			this->图像增强ToolStripMenuItem->Size = System::Drawing::Size(68, 21);
			this->图像增强ToolStripMenuItem->Text = L"图像增强";
			// 
			// 灰度拉伸ToolStripMenuItem
			// 
			this->灰度拉伸ToolStripMenuItem->Name = L"灰度拉伸ToolStripMenuItem";
			this->灰度拉伸ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->灰度拉伸ToolStripMenuItem->Text = L"灰度拉伸";
			this->灰度拉伸ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::灰度拉伸ToolStripMenuItem_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(711, 344);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->EndInit();
			this->groupBox2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
public:
	public:
	    typedef  unsigned char BYTE;
	    Bitmap^  original_bmap;
	    Bitmap^  processed_bmap;

	     int nWidth;                 //图像宽
	     int nHeight;                //图像高

	     BYTE **oriImage;           //原图灰度
	     BYTE **proImage;          //处理后灰度

		 BYTE ***pSrcImage;
		 BYTE ***pDestImage;

		// GreyStretch greystretch;

private: System::Void 打开ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		    OpenFileDialog^ openFile1 = gcnew OpenFileDialog ( );
			openFile1->InitialDirectory = "E:\\";
			openFile1 ->Filter = "Bitmap Image(*.bmp)|*.bmp|Image(*.jpeg)|*.jpg";
			openFile1 ->RestoreDirectory = true;

			if (openFile1 ->ShowDialog() == Windows::Forms::DialogResult ::OK) 
			{
				String^ fileName = openFile1 ->FileName->ToString();

				original_bmap  = gcnew Bitmap ( fileName, true );
				processed_bmap = gcnew Bitmap ( fileName, true );

				nWidth  = original_bmap->Width;
				nHeight = original_bmap->Height;
				pictureBox1->Image = original_bmap;
				pictureBox2->Image = processed_bmap;
				pictureBox1->Refresh();
				pictureBox2->Refresh();

				oriImage = new BYTE*[nHeight];
				proImage = new BYTE *[nHeight];

			for ( int i = 0; i < nHeight; i++ )
			{
                oriImage[i] = new BYTE[nWidth];
				proImage[i] = new BYTE[nWidth];
			}
			for ( int i = 0; i < nHeight; i++ )
			{
               for ( int j = 0; j < nWidth; j++ )
	          {
					oriImage[i][j] = (BYTE)(original_bmap->GetPixel(j,i).R*0.299
					+ original_bmap->GetPixel(j,i).G*0.587  
					 + original_bmap->GetPixel(j,i).B*0.114);
			  }
			}				 
		} // end if
 }
private: System::Void 保存ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 SaveFileDialog ^ saveFile1 = gcnew SaveFileDialog ();
			saveFile1 -> InitialDirectory = "E:\\";
			saveFile1 -> Filter = "Bitmap Image|*.bmp|JPeg Image|*.jpg|Gif Image|*.gif";
			saveFile1 -> Title = "Save an Image File";
			saveFile1 -> ShowDialog ();
			if ( saveFile1 ->FileName != " ")
			{
				switch( saveFile1->FilterIndex )
				{
					case 1:
						this->processed_bmap->Save(saveFile1->   FileName , System:: Drawing :: Imaging::   ImageFormat::Jpeg);
		            break;
					case 2:
						this->processed_bmap->Save(saveFile1->FileName ,System ::Drawing :: Imaging :: ImageFormat ::Bmp);
		            break;

					case 3:
						this->processed_bmap->Save(saveFile1->FileName,System ::Drawing :: Imaging :: ImageFormat ::Gif);
		            break;
				} //end switch
			} // end if
	 }
private: System::Void 灰度拉伸ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			  /* 如果不为空，则释放内存*/
			 if (pSrcImage) {
				for (int i = 0; i < nHeight; i++)
				{
					for (int j = 0; j < nWidth; j++)
					{
						delete [] pSrcImage[i][j];
					}
					delete [] pSrcImage[i];
				}
				delete [] pSrcImage;
				pSrcImage = 0;
			 }

			 if (pDestImage){
				 for (int i = 0; i < nHeight; i++)
				 {
					 for (int j = 0; j < nWidth; j++)
					 {
						 delete [] pDestImage[i][j];
					 }
					 delete [] pDestImage[i];
				 }
				 delete [] pDestImage;
				 pDestImage = 0;
			 }	

			  /* 为图像分配三维数组*/
			 pSrcImage = new unsigned char**[nHeight];
			 pDestImage = new unsigned char**[nHeight];
			 for (int i = 0; i < nHeight; i++) 
			 {
				 pSrcImage[i] = new unsigned char*[nWidth];
				 pDestImage[i] = new unsigned char*[nWidth];
				 for (int j = 0; j < nWidth; j++)
				 {
					 pSrcImage[i][j] = new unsigned char[3];
					 pDestImage[i][j] = new unsigned char[3];
				 }
			 }
			 for (int i = 0; i < nHeight; i++)
			 {
				 for (int j = 0; j < nWidth; j++) 
				 {
					 pSrcImage[i][j][0] = pSrcImage[i][j][1] = pSrcImage[i][j][2]
									    = (unsigned char)(original_bmap->GetPixel(j,i).R*0.299 +
									      original_bmap->GetPixel(j,i).G*0.587 +
									      original_bmap->GetPixel(j,i).B*0.114);
				 }
			 }
			  /* 将图像指针传给灰度拉伸窗口*/
			 
			 greystretch.SetImage(pSrcImage, pDestImage, nHeight, nWidth, processed_bmap, pictureBox2);
			 greystretch.Show();
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}

