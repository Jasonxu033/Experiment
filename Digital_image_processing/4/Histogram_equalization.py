import numpy as np
import cv2
import os


def batch_process():
    or_folder = "crack-or/"
    gr_folder = "crack-his/"
    filelist = os.listdir(or_folder)
    for each in filelist:
        img = cv2.imread(or_folder + each, 0)
        # cv2.imshow('image', img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        eq = cv2.equalizeHist(img)
        cv2.imwrite(gr_folder + each.replace("tif", 'jpg'), eq)


def single_process():
    image = cv2.imread("2.jpg", 0)  # 读取灰度图像
    # cv2.imshow("Original", image)
    # cv2.waitKey(0)
    eq = cv2.equalizeHist(image)  # 灰度图像直方图均衡化
    # 对比显示
    # cv2.imshow("Histogram Equalization", np.hstack([image, eq]))
    # 单张显示
    # cv2.imshow("Histogram Equalization", eq)
    # cv2.waitKey(0)
    cv2.imwrite("2.jpg", eq)


if __name__ == '__main__':
    single_process()
    # batch_process()
