from matplotlib import pyplot as plt
import cv2

image = cv2.imread("2.jpg")

# 图像直方图
hist = cv2.calcHist([image], [0], None, [256], [0, 256])

plt.figure()
plt.title("Grayscale Histogram")
plt.xlabel("Bins")
plt.ylabel("num of Pixels")
x, y = [], []
for i in range(256):
    y.append(hist[i][0])
    x.append(i)
# print(h)
# print("len:" + str(len(hist)))
plt.bar(x, y, width=0.5)
plt.xlim([0, 256])
plt.ylim([0, 7000])
plt.savefig("1Grayscale Histogram.jpg")
plt.show()
