import cv2
import os


def batch_gray():
    or_folder = "crack-or/"
    gr_folder = "crack-gray/"
    filelist = os.listdir(or_folder)
    for each in filelist:
        img = cv2.imread(or_folder + each, 0)
        # cv2.imshow('image', img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        cv2.imwrite(gr_folder + each.replace("tif", 'jpg'), img)


if __name__ == '__main__':
    batch_gray()
