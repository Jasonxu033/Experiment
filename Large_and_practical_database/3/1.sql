declare 
  first_name employees.first_name%TYPE;
  last_name employees.last_name%TYPE;
  maxx number;
begin
  select max(salary) into maxx
  from employees;
  select first_name, last_name into first_name, last_name
  from employees
  where salary = maxx;
  dbms_output.put_line('最高工资员工的姓名：' || first_name || ' ' || last_name);
end;