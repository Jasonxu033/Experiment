declare
  max_deptno departments.department_id%type;
  max_deptno_name departments.department_name%type;
begin
  select max(department_id) into max_deptno
  from departments;
  select department_name into max_deptno_name
  from departments
  where department_id = max_deptno;
  dbms_output.put_line('部门编号：' || max_deptno || '部门名字：' || max_deptno_name);
end;