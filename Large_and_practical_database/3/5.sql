declare
  cursor emp_cursor is
    select employee_id, last_name, salary
    from employees
    where department_id = 50;
begin
  for emp_record in emp_cursor loop
    dbms_output.put_line('employee_id: ' || emp_record.employee_id || '     last_name: ' || emp_record.last_name || '     salary: ' || emp_record.salary);
  end loop;
end;