create or replace procedure search_salary(empid number, sal in out number)
is
begin
  select salary into sal
  from employees
  where employee_id = empid;
end;

declare
  sals employees.salary%type;
begin
  search_salary(114, sals);
  dbms_output.put_line('员工编号为114的员工， 工资为：' || sals);
end;