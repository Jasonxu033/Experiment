create or replace function com_sal(empid employees.employee_id%type)
return boolean
is
  eva_sal employees.salary%type;
  res_sal employees.salary%type;
begin
  select avg(salary) into eva_sal
  from employees;
  
  select salary into res_sal
  from employees
  where employee_id = empid;
  
  if eva_sal > res_sal then
      return false;
    else
      return true;
    end if;
  
  exception
    when no_data_found then
      return null;
end;


declare 
  var_empid employees.employee_id%type := &id;
  res boolean;
begin
  res := com_sal(var_empid);
  if res is null then
    dbms_output.put_line('编号为' || var_empid || '的员工不存在。');
  elsif res then
    dbms_output.put_line('编号为' || var_empid || '的工资大于部门平均工资。');
  else
    dbms_output.put_line('编号为' || var_empid || '的工资不大于部门平均工资。');
  end if;
end;