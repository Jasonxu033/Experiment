begin
  update employees
  set salary = salary*1.1, commission_pct = NVL(commission_pct, 0)+0.08
  where salary>8000 and salary<15000;
end;
