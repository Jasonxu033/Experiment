# include<iostream>
# include<string>
# include<stack>
# include<fstream>
# include<cstdio>
using namespace std;

int count = 0;
const int MAX = 50;
char buffer[1000];

//索引                      0        1        2            3         4         5               6              7       8          9
string operatorsKey[MAX] = {"plus" , "sub"  , "multi"    , "devide", "pound" , "greatOrEqual", "lessOrEqual", "less", "greater", "percent"};
string operators[MAX]    = {"+"    , "-"    , "*"        , "/"     , "="     , ">="          , "<="         , "<"   , ">"      , "%"      };
string reservedWord[MAX] = {"main" , "if"   , "else"     , "int"   , "return", "do"          , "continue"   , "break"                     };
string borderKey[MAX]    = {"comma", "point", "semicolon", "lparen", "rparen", "lbparen"     , "rbparen"                                  };
string border[MAX]       = {","    , "."    , ";"        , "("     , ")"     , "{"           , "}"                                        };

// 读取一个字符
char getch()
{
	char temp = buffer[count];
	count++;
	return temp;
}

// 是否字母
bool isLetter(char c) {
	if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') 
		return true;
	else 
		return false;
}

// 是否数字 
bool isNumber(char c) {
	if(c >= '0' && c <= '9')
		return true;
	else
		return false;
}

// 是否空格 
bool isPace(char c) {
	if(c==' ' || c=='\n' || c=='\t')
		return true;
	else
		return false;
}

// 是否界符 
int isBorder(string s) {
	for(int i=0; i<MAX; i++)
	{
		if(s==(border[i])) 
		{
			//cout<<s<<"\n"; 
			return (i+1);
		}
	}
	return false;
}

// 是否保留字
bool isReservedWord(string s){
	for(int i=0;i<MAX;i++)
	{
		if(s==reservedWord[i] && !s.empty()) return true;
	}
	return false;
}

// 是否运算符
int isOperator(string s) {
	for(int i=0; i<MAX; i++)
	{
		if(s==(operators[i]))
		{
			//cout<<s<<"\n"; 
			return (i+1);
		}
	}
	return false;
}

// 以符号为基准，分割串
void strSplit(string s, string arg){
	string temp, t1, t2;
	int i;
	for(i=0; i<s.length(); i++)
	{
		//cout<<s[i]<<endl;
		t1="";
		t1.insert(t1.end(), s[i]);
		//t1.insert(t1.end(), s[i]);
		//cout<<t1<<endl;
		if(isBorder(t1))
		{
			if(!temp.empty()) 
			{
				if(isReservedWord(temp))
					cout<<temp<<"sys,\t\t"<<temp<<endl;
				else
				{
					if(arg=="NUMBER")
						cout<<"number,\t\t"<<temp<<endl;
					else if(arg=="LETTER")
						cout<<"ident,\t\t"<<temp<<endl;
				}
			}
			temp="";
			cout<<borderKey[isBorder(t1)-1]<<",\t\t"<<s[i]<<endl;
		}
		else if(isOperator(t1))
		{
			if(!temp.empty()) cout<<"str:"<<temp<<endl;
			temp="";
			cout<<operatorsKey[isOperator(t1)-1]<<",\t\t"<<s[i]<<endl;
		}
		else
		{
			temp.insert(temp.end(), s[i]);
		}
	}
	if(!temp.empty()) 
	{
		if(isReservedWord(temp))
			cout<<temp<<"sys,\t\t"<<temp<<endl;
		else
		{
			if(arg=="NUMBER")
				cout<<"number,\t\t"<<temp<<endl;
			else if(arg=="LETTER")
				cout<<"ident,\t\t"<<temp<<endl;
		}
	}
}

void lexical_analysis()
{
	string type;
	string temp, t;
	char ch = getch();
	
	// 读取并抛弃空字符
	while(isPace(ch)) ch=getch();
	
	t.insert(t.end(), ch);

	// 读取的是首字符是字母
	if(isLetter(ch))
	{
		temp.insert(temp.end(), ch);
		// cout<<temp<<endl;
		while(1)
		{
			ch = getch();
			if(!isPace(ch)) temp.insert(temp.end(), ch);
			else
			{
				//cout<<temp<<endl;
				strSplit(temp, "LETTER");
				temp="";
				break;
			}
		}
	}
	// 读取的是首字符是数字
	else if(isNumber(ch))
	{
		temp.insert(temp.end(), ch);
		// cout<<temp<<endl;
		while(1)
		{
			ch = getch();
			if(!isPace(ch)) temp.insert(temp.end(), ch);
			else
			{
				//cout<<temp<<endl;
				strSplit(temp, "NUMBER");
				temp="";
				break;
			}
		}
	}
	// 读取的是首字符是界符
	else if(isBorder(t))
	{
		cout<<borderKey[isBorder(t)-1]<<",\t\t"<<t<<endl;
	}
	// 读取的是首字符是运算符
	else if(isOperator(t))
	{
		cout<<operatorsKey[isOperator(t)-1]<<",\t\t"<<t<<endl;
	}
}

int main()
{
	// readfile
	char a;
	int i = 0;
	freopen("test.txt", "r", stdin);
	while(~scanf("%c", &a)) 
	{
		buffer[i] = a;
		i++;
	}
	// frame
	while(count<i)
	{
		lexical_analysis();
	}
	return 0;
}