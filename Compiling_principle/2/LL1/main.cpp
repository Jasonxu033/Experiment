# include<iostream>
# include<string>
# include<vector>
# include<stack>
# include<list>
# include<cstdio>
# include<stack>
# include<cmath>
using namespace std;

stack<char> aStack;
stack<char> sStack;
stack<int> num_stack;
stack<char> boring;
char buffer[1000];
const int MAX = 50;
const int Y = 8;   // 预测分析表的横排
const int X = 5;   // 预测分析表的纵排

int flag = 1;
// 预测分析表          i     +      -      *      /      (      )    #
//                     0     1      2      3      4      5      6    7
string table[X][Y] = {{"TA", ""   , ""   , ""   , ""   , "TA" , "" , "" },  // 0 E
				      {""  , "+TA", "-TA", ""   , ""   , ""   , "0", "0"},  // 1 A
                      {"FB", ""   , ""   , ""   , ""   , "FB" , "" , "" },  // 2 T 
                      {""  , "0"  , "0"  , "*FB", "/FB", ""   , "0", "0"},  // 3 B
                      {"i" , ""   , ""   , ""   , ""   , "(E)", "" , "" }}; // 4 F
//　符号顺序对照表
char yy[] = {'i', '+', '-', '*', '/', '(', ')' ,'#'};
char xx[] = {'E', 'A', 'T', 'B', 'F'};

// 分析
void analysis()
{
	cout<<"序号\t"<<"分析栈顶\t"<<"输入栈顶\t"<<"查表\t"<<endl;
	cout<<"--------------------------------------------------"<<endl;
	char A, B;
	int Pos=0;
	int num=1;
	while(1)
	{
		A = aStack.top();
		aStack.pop();
		B = sStack.top();
		
		//　接受时
		if (A=='#' && B=='#')
		{
			cout<<"\n输入合法！"<<endl;
			break;
		}

		// 当'ｉ'匹配时
		if(A==B)
		{
			/*
			if(A=='i')
			{
				//cout<<num_stack.top();
				//num_stack.pop();
			}
			else
				cout<<B;
			*/
			sStack.pop();
			Pos++;
			continue;
		}
		if(A!='E' && A!='A' && A!='T' && A!='B' && A!='F' && A!=B)
		{
			cout<<"\n出错在位置x: "<<Pos<<endl;
			flag = 0;
			break;
		}

		// 确定当前栈顶元素在预测分析表的位置
		int xPos, yPos;
		for(int i=0; i<X; i++)
		{
			 if(A == xx[i])
			 {
				 xPos = i;
				 break;
			 }
			 if(i==X)
			 {
				 cout<<"\n出错在位置x: "<<Pos<<endl;
				 flag = 0;
				 break;
			 }
		}
		for(int i=0; i<Y; i++)
		{
			 if(B == yy[i])
			 {
				 yPos = i;
				 break;
			 }
			 if(i==Y)
			 {
				 cout<<"\n出错在位置y: "<<Pos<<endl;
				 flag = 0;
				 break;
			 }
		}

		// 预测的分析表对应位置的内容
		string content = table[xPos][yPos];
		cout<<num<<"\t"<<A<<"\t\t"<<B;
		num++;
		cout<<"\t\t"<<content<<endl;
		//cout<<content<<endl;
		if(content == "")
		{
			cout<<"\n出错在位置z: "<<Pos<<endl;
			flag = 0;
			break;
		}
		if(content == "0") continue;

		for(int i=content.length()-1; i>=0; i--)
			aStack.push(content[i]);
	}
}

//判断是否是数字 
bool isNumber(char c) 
{
	if (c >= '0' && c <= '9') 
		return true;
	else 
		return false;
}

// 将表达式的数字转化成i
int len = 0;
void change_to_i()
{
	int num=0;
	int count = 0;
	double x = 10;
	for(int i=len-1; i>=0; i--)
	{
		if(isNumber(buffer[i]))
		{
			count++;
			if(count==1)
			{
				sStack.push('i');
				boring.push('i');
				//cout<<boring.top();
			}
			num = num + (buffer[i]-'0')*pow(x, count-1);
		}
		else
		{
			sStack.push(buffer[i]);
			boring.push(buffer[i]);
			//cout<<boring.top();
			if(num!=0) num_stack.push(num);
			//cout<<buffer[i];
			count = 0;
			num = 0;
		}
		if(num!=0 && i==0) num_stack.push(num);
	}
	//cout<<endl;
}

struct Symbol
{
	char value;
	int  prio;
};

// 逆波兰式
void reverse_polish()
{
	// 为符号添加优先级
	vector<struct Symbol> xx;
	while(1)
	{
		if(boring.empty())
			break;
		struct Symbol temp;
		temp.value = boring.top();
		xx.insert(xx.end(), temp);
		//cout<<temp.value<<endl;
		boring.pop();
	}
	for(int i=0; i<xx.size(); i++)
	{
		if(xx[i].value == 'i') xx[i].prio = -1;
		else if(xx[i].value == '+' || xx[i].value == '-') xx[i].prio = 0;
		else if(xx[i].value == '*' || xx[i].value == '/') xx[i].prio = 1;
		else if(xx[i].value == '(' || xx[i].value == ')') xx[i].prio = 3;
	}

	stack<struct Symbol> ss;
	// 计算逆波兰式
	int pro_prio = 0; 
	for(int i=0; i<xx.size();i++)
	{
		//cout<<i;
		if(xx[i].value == 'i')
		{
			cout<<num_stack.top()<<" ";
			num_stack.pop();
		}
		else if(xx[i].value == '(') ss.push(xx[i]);
		else if(xx[i].value == ')')
		{
			while(true)
			{
				if(ss.top().value=='(')
				{
					ss.pop();
					break;
				}
				cout<<ss.top().value<<" ";
				ss.pop();
			}
		}
		else if(ss.empty() || ss.top().value=='(' || xx[i].prio > ss.top().prio)
		{
			ss.push(xx[i]);
		}
		else if(xx[i].prio <= ss.top().prio)
		{
			cout<<ss.top().value<<" ";
			ss.pop();
			ss.push(xx[i]);
		}	
	}
	while(!ss.empty())
	{
		cout<<ss.top().value<<" ";
		ss.pop();
	}
	cout<<endl;
}

int main()
{
	// 读入数据
	char a;
	freopen("nin4.txt", "r", stdin);
	while(~scanf("%c", &a)) 
	{
		buffer[len] = a;
		len++;
	}
	
	// 初始化生于输入串栈
	sStack.push('#');
	change_to_i();

	// 初始化分析栈
	aStack.push('#');
	aStack.push('E');

	analysis();
	
	// 算逆波兰式
	if(flag)
		reverse_polish();
	return 0;
}