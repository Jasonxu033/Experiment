# include<iostream>
# include<string>
# include<vector>
# include<stack>
# include<list>
# include<cstdio>
# include<stack>
# include<cmath>
using namespace std;

stack<int>  aStack;
stack<char> sStack;
stack<int> num_stack;
stack<char> boring;
char buffer[1000];
const int MAX = 50;
const int Y = 8;    // 预测分析表的横排
const int X = 16;   // 预测分析表的纵排

int flag = 1;
// 空格:0 归约：<0 移进:>0
// 预测分析表       +   -   *   /   (   )    i   #
//                  0    1   2   3   4   5   6   7
int ACTION[X][Y] = {{0,  0,  0,  0,  4,  0,  5,  0 },  // 0 
				   { 6,  7,  0,  0,  0,  0,  0,  100 },  // 1 
                   {-3, -3,  8,  9,  0, -3,  0, -3 },  // 2 
                   {-6, -6, -6, -6,  0, -6,  0, -6 },  // 3
				   { 0,  0,  0,  0,  4,  0,  5,  0 },  // 4               
				   {-8, -8, -8, -8,  0, -8,  0, -8 },  // 5
				   { 0,  0,  0,  0,  4,  0,  5,  0 },  // 6 
				   { 0,  0,  0,  0,  4,  0,  5,  0 },  // 7 
				   { 0,  0,  0,  0,  4,  0,  5,  0 },  // 8
				   { 0,  0,  0,  0,  4,  0,  5,  0 },  // 9 
				   { 6,  7,  0,  0,  0, 15,  0,  0 },  // 10 
				   {-1, -1,  8,  9,  0, -1,  0, -1 },  // 11 
				   {-2, -2,  8,  9,  0, -2,  0, -2 },  // 12 
				   {-4, -4, -4, -4,  0, -4,  0, -4 },  // 13 
				   {-5, -5, -5, -5,  0, -5,  0, -5 },  // 14
				   {-7, -7, -7, -7,  0, -7,  0, -7 }}; // 15

                //   E   T   F
int GOTO[X][3] = { { 1,  2,  3},  // 0 
				   { 0,  0,  0},  // 1 
                   { 0,  0,  0},  // 2 
				   { 0,  0,  0},  // 3 
                   {10,  2,  3},  // 4
				   { 0,  0,  0},  // 5                 
				   { 0, 11,  3},  // 6
				   { 0, 12,  3},  // 7
				   { 0,  0, 13},  // 8
				   { 0,  0, 14},  // 9
				   { 0,  0,  0},  // 10
				   { 0,  0,  0},  // 11
				   { 0,  0,  0},  // 12
				   { 0,  0,  0},  // 13
				   { 0,  0,  0},  // 14
				   { 0,  0,  0}}; // 15 

int length[] = {3, 3, 3, 1, 3, 3, 1, 3, 1};
int lefts[]   = {9, 0, 0, 0, 1, 1, 1, 2, 2};
// string * biaodashi= "";

//　符号顺序对照表
char yy[] = {'+', '-', '*', '/', '(', ')', 'i', '#'};

// 规约
void reduced()
{

}

// 分析
void analysis()
{
	cout<<"序号\t"<<"状态栈\t"<<"输入栈顶\t"<<"查表\t"<<"操作"<<endl;
	cout<<"--------------------------------------------------"<<endl;
	int  A;
	char B;
	int Pos = 0;
	int num = 1;
	while(1)
	{
		A = aStack.top();
		B = sStack.top();
		
		cout<<num<<"\t"<<A<<"\t"<<B;
		num++;

		//　接受时
		if (A==1 && B=='#')
		{
			cout<<"\n输入合法！"<<endl;
			break;
		}

		// 确定当前栈顶元素在预测分析表的位置
		int xPos, yPos;
		for(int i=0; i<Y; i++)
		{
			 if(B == yy[i])
			 {
				 yPos = i;
				 break;
			 }
			 if(i==Y)
			 {
				 cout<<"\n出错在位置y: "<<Pos<<endl;
				 flag = 0;
				 break;
			 }
		}

		// 预测的分析表对应位置的内容
		// 空格:-1 等于号:0 小于号:1 大于号:2
		int content = ACTION[A][yPos];
		if(content == 0)
		{
			cout<<"\n出错在位置z: "<<Pos<<endl;
			flag = 0;
			break;
		}
		if(content>0)
		{
			cout<<"\t\tS"<<content<<"\t";
			cout<<"移进"<<endl;
			aStack.push(content);
			sStack.pop();
			Pos++;
		}
		// 规约
		if(content<0)
		{
			cout<<"\t\tr"<<-content<<"\t";
			cout<<"归约"<<endl;
			content = 0-content;
			int len = length[content];
			for(int i=0; i<len; i++)
				aStack.pop();
			int yyyy =  lefts[content];
			int G = GOTO[aStack.top()][yyyy];
			aStack.push(G);
			//aStack.pop();
		}
	}
}

//判断是否是数字 
bool isNumber(char c) 
{
	if (c >= '0' && c <= '9') 
		return true;
	else 
		return false;
}

// 将表达式的数字转化成i
int len = 0;
void change_to_i()
{
	int num=0;
	int count = 0;
	double x = 10;
	for(int i=len-1; i>=0; i--)
	{
		if(isNumber(buffer[i]))
		{
			count++;
			if(count==1)
			{
				sStack.push('i');
				boring.push('i');
				//cout<<boring.top();
			}
			num = num + (buffer[i]-'0')*pow(x, count-1);
		}
		else
		{
			sStack.push(buffer[i]);
			boring.push(buffer[i]);
			//cout<<boring.top();
			if(num!=0) num_stack.push(num);
			//cout<<buffer[i];
			count = 0;
			num = 0;
		}
		if(num!=0 && i==0) num_stack.push(num);
	}
	//cout<<endl;
}

struct Symbol
{
	char value;
	int  prio;
};

// 逆波兰式
void reverse_polish()
{
	cout<<"\n逆波兰式:"<<endl;
	// 为符号添加优先级
	vector<struct Symbol> xx;
	while(1)
	{
		if(boring.empty())
			break;
		struct Symbol temp;
		temp.value = boring.top();
		xx.insert(xx.end(), temp);
		//cout<<temp.value<<endl;
		boring.pop();
	}
	for(int i=0; i<xx.size(); i++)
	{
		if(xx[i].value == 'i') xx[i].prio = -1;
		else if(xx[i].value == '+' || xx[i].value == '-') xx[i].prio = 0;
		else if(xx[i].value == '*' || xx[i].value == '/') xx[i].prio = 1;
		else if(xx[i].value == '(' || xx[i].value == ')') xx[i].prio = 3;
	}

	stack<struct Symbol> ss;
	// 计算逆波兰式
	int pro_prio = 0; 
	for(int i=0; i<xx.size();i++)
	{
		//cout<<i;
		if(xx[i].value == 'i')
		{
			cout<<num_stack.top()<<" ";
			num_stack.pop();
		}
		else if(xx[i].value == '(') ss.push(xx[i]);
		else if(xx[i].value == ')')
		{
			while(true)
			{
				if(ss.top().value=='(')
				{
					ss.pop();
					break;
				}
				cout<<ss.top().value<<" ";
				ss.pop();
			}
		}
		else if(ss.empty() || ss.top().value=='(' || xx[i].prio > ss.top().prio)
		{
			ss.push(xx[i]);
		}
		else if(xx[i].prio <= ss.top().prio)
		{
			cout<<ss.top().value<<" ";
			ss.pop();
			ss.push(xx[i]);
		}	
	}
	while(!ss.empty())
	{
		cout<<ss.top().value<<" ";
		ss.pop();
	}
	cout<<endl;
}

int main()
{
	// 读入数据
	char a;
	freopen("nin5.txt", "r", stdin);
	while(~scanf("%c", &a)) 
	{
		buffer[len] = a;
		len++;
	}
	
	// 初始化输入串栈
	sStack.push('#');
	change_to_i();

	// 初始化分析栈
	aStack.push(0);
	//aStack.push('E');

	analysis();
	
	// 算逆波兰式
	if(flag)
		reverse_polish();
	return 0;
}