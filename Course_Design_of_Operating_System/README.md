# Course_Design_of_Operating_System
* Course: Course Design of Operating System
* project : Processor Scheduling System for Jobs and Processes  
* teacher: Jiajing Li
* Author : Pinjie Xu, Lisheng Xu, Qianying Yu, Guoxing Zheng, Di Wang

# License
```
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
            Version 2, December 2004

Copyright (C) 2018 Pinjie Xu <xupinjie321@outlook.com> 
              2018 Aaron Xu <Aaron.M.Xu@GodAaron.club>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

    DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
```
