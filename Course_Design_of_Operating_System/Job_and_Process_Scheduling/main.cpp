﻿# include"SCHEDULE.h"
#include <iostream>
# include<cstring>
int main(int argc, char **argv)
{
    char *name[]{
            // This is CommandOpts.
            const_cast<char *>("--help"),           //0
            const_cast<char *>("--flag"),           //1
            const_cast<char *>("--process"),        //2
            const_cast<char *>("--memory"),         //3
            const_cast<char *>("1"),                //4
            const_cast<char *>("2"),                //5
    };

    //是否开启进制转换功能，1：开启 2：不开启
    flagg = 1;

    //调度算法选择

    //MEM Default= BFA_; 1 = BFA_; 2 = FFA;
    MEMORY_ALLOCATION = BFA_;

    //Process Default = SJF_; 1 = FCFS; 2 = SJF;
    PROCESS_SCHEDULE = SJF_;

    for (int offset = 1; offset < argc; offset++){
        if (strcmp(argv[offset],name[0]) == 0){
            cout << "使用 \'--flag\'     参数设定是否开启进制转换功能，   1:开启 2:不开启" << endl;
            cout << "使用 \'--process\' 参数设定处理机调度算法,           1:FCFS 2:SJF" << endl;
            cout << "使用 \'--memory\'  参数设定内存调度算法,             1:BFA  2:FFA" << endl;
            cout << "使用 \'--help\'    参数显示该消息" << endl;
            exit(0);
        }


        else if (strcmp(argv[offset],name[1]) == 0){
            offset++;
            if (strcmp(argv[offset],name[4]) == 0) flagg = 1;
            else if (strcmp(argv[offset],name[5]) == 0) flagg = 2;
            else {
                cout << "无效的参数: " << argv[offset] << "\n";
                exit(123123);
            }
            //Test
            //cout << flagg << "Test flag!" << "\n";
            continue;
        }

        else if (strcmp(argv[offset],name[2]) == 0){
            offset++;
            if (strcmp(argv[offset],name[4]) == 0) PROCESS_SCHEDULE = FCFS_;
            else if (strcmp(argv[offset],name[5]) == 0) PROCESS_SCHEDULE = SJF_;
            else {
                cout << "无效的参数: " << argv[offset] << "\n";
                exit(123123);
            }
            //Test
            //cout << PROCESS_SCHEDULE << "Test process!" << "\n";
            continue;
        }

        else if (strcmp(argv[offset],name[3]) == 0){
            offset++;
            //TODO:memory
            if (strcmp(argv[offset],name[4]) == 0) MEMORY_ALLOCATION = BFA_;
            else if (strcmp(argv[offset],name[5]) == 0) MEMORY_ALLOCATION = FFA_;
            else {
                cout << "无效的参数: " << argv[offset] << "\n";
                exit(123123);
            }
            //Test
            //cout << MEMORY_ALLOCATION << "Test memory!" << "\n";
            continue;
        }

        else{
            cout << "无效的参数: " << argv[offset] << "\n";
            cout << "使用 '--flag'    参数设定是否开启进制转换功能，   1：开启 2：不开启\n";
            cout << "使用 '--process' 参数设定处理机调度算法,         1:FCFS 2:SJF\n";
            cout << "使用 '--memory'  参数设定内存调度算法,           1:BFA  2:FFA\n";
            cout << "使用 '--help'    参数显示该消息\n";
            exit(123123);
        }
    }

    /* Debug for arguments.
    exit(0);
    */

	//初始化
	init();

	//主调用
	while (true)
	{
		if (wait_for_memory_job_vector.empty() && process_vect.empty()) break;
		memory_allocation(free_chain, wait_for_memory_job_vector, process_vect);
		if (PROCESS_SCHEDULE == SJF_)
			SJF(process_vect);
		else if (PROCESS_SCHEDULE == FCFS_)
			FCFS(process_vect);
	}


    cout << "平均周转时间: " << turn_around_time_change(AverageTurnaroundTime(CalculateTurnaround)) << "\t";
    cout << "平均带权周转时间: " << (WeightedAverageTurnaroundTime(CalculateTurnaround)) << "\n";

	return 0;
}