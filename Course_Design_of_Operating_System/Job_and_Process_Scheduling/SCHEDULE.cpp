﻿# include"SCHEDULE.h"
# include<iostream>
# include<list>
# include<iterator>
# include<vector>
# include<algorithm>

//这里采用double，为了避免+-0.001的时候精度丢失
double end_time = 0;

int MEMORY_ALLOCATION = 0;
int PROCESS_SCHEDULE = 0;
int flagg = 1;

list<free_chain_node> free_chain;                  //自由链
vector<job>           wait_for_memory_job_vector;  //等待内存的作业向量
vector<process>       process_vect;                //等待处理机的进程向量
vector<double>         enter_processor_time_vect;  //进入处理机时间向量
vector<double>         end_time_vect;              //结束时间向量
vector<process_scheduling> CalculateTurnaround;

/*
	重载排序运算符
*/
bool operator<(const free_chain_node& lhs, const free_chain_node& rhs)
{
	if (MEMORY_ALLOCATION == 1)
		return lhs.head_address < rhs.head_address;
	else if (MEMORY_ALLOCATION == 2)
		return lhs.memory_size < rhs.memory_size;
	else cout << "请选择正确的调度算法序号！" << endl;
}

/*
	内存分配
*/
void memory_allocation(list<free_chain_node>& fl, vector<job>& jv, vector<process>& pv)
{
	vector<job>::iterator iterator_job;
	list<free_chain_node>::iterator iterator_chain;

	//刚进来的等待进程都置为可以运行
	for (auto iterator_process = pv.begin(); iterator_process != pv.end(); iterator_process++)
		iterator_process->flag = 1;
	
	//标记位，判断是否发生了作业向量节点删除  0：未发生删除  1：发生删除
	int flag = 0;
	//遍历等待作业向量
	for (iterator_job = jv.begin(); iterator_job != jv.end(); )
	{
		if (iterator_job->enter_time > end_time) break;
		//cout << "给作业num分配内存:" << iterator_job->num << endl;
		//为每一个等待作业遍历内存块
		iterator_chain = fl.begin();
		iterator_chain++;     //跳过第一个首地址为0，大小为0的标记内存块
		for (; iterator_chain != fl.end(); iterator_chain++)
		{
			//需要分配的内存大小
			int memory = iterator_job->memory;
			
			//当作业需要内存大于自由链节点大小，跳出循环
			//cout << "need:" <<memory << endl << "allow:" << iterator_chain->memory_size<<endl;
			if (memory > iterator_chain->memory_size) continue;
			
			//否则，创建进程节点，并初始化相应参数，加入进程等待队列
			process p1;
			p1.num = iterator_job->num;
			p1.head_address = iterator_chain->head_address + iterator_chain->memory_size - memory;
			    //p1.enter_processor_time = ;
			p1.memory  = memory;
			p1.runtime = iterator_job->runtime;
			p1.flag    = 0;
            p1.EnterTime = iterator_job->enter_time;     //Add enter system time.
			pv.push_back(p1);
			
			//删除等待内存作业，并置标记位
			iterator_job = jv.erase(iterator_job);
			flag = 1;
			
			//重构内存自由链
			if (iterator_chain->memory_size == memory)
				fl.erase(iterator_chain++);
			else
				iterator_chain->memory_size = iterator_chain->memory_size - memory;
			
			//Debug输出当前内存状况
			//list<free_chain_node>::iterator pp;
			//for (pp = fl.begin(); pp != fl.end(); pp++)
			//{
			//	cout << "首地址：" << pp->head_address << "\t内存块大小：" << pp->memory_size << endl;
			//}

			//内存已分配完成，直接退出该作业的内存分配环节
			break;
		}
		if (flag == 1) flag = 0;
		else iterator_job++;
	}
	//输出当前内存状况
	//if (MEMORY_ALLOCATION == FFA_)
	//{
	//	MEMORY_ALLOCATION = BFA_;
		fl.sort();
	//	MEMORY_ALLOCATION = FFA_;
	//}
	list<free_chain_node>::iterator pp;
	cout << "\n内存分配完成，当前内存状况：" << endl;
	pp = fl.begin();
	if (fl.size() != 1) pp++;
	for (; pp != fl.end(); pp++)
	{
		cout << "首地址：" << pp->head_address << "\t内存块大小：" << pp->memory_size << endl;
	}
}

/*
	内存回收
*/
list<free_chain_node>& memory_recover(list<free_chain_node>& fl, int head_address, int memory_size)
{
	// 如果采用最佳适应法，则把排序规则转换成按地址从小到大，方便插入,方法太暴力
	if (MEMORY_ALLOCATION == FFA_)
	{
		MEMORY_ALLOCATION = BFA_;
		fl.sort();
		MEMORY_ALLOCATION = FFA_;
	}
	
	list<free_chain_node>::iterator iterator_chain;   //当前节点
	list<free_chain_node>::iterator next;             //下一个节点
	iterator_chain = fl.begin();
	next = fl.begin();
	iterator_chain++;
	next++;
	next++;
	
	int flag = 0;
	
	for (; iterator_chain != fl.end(); iterator_chain++)
	{
		if (head_address > iterator_chain->head_address + iterator_chain->memory_size)
		{
			// 插入内存块位于当前节点之下，则跳出循环
			if (next != fl.end()) next++;  //跳出循环也不能忘了把表示下一个的迭代器往后移
			continue;
		}
		else if ((head_address == iterator_chain->head_address + iterator_chain->memory_size) &&
			next != fl.end() &&
			(head_address + memory_size == next->head_address))
		{
			// 当插入的内存块与上下块都可以合并的
			//cout << "debug_up_down\n";
			int memory = memory_size + iterator_chain->memory_size + next->memory_size;
			iterator_chain->memory_size = memory;
			fl.erase(next);
			flag = 1;
			break;
		}
		else if (head_address == iterator_chain->head_address + iterator_chain->memory_size)
		{
			//当插入内存块可以和上内存块合并
			//cout << "debug_up\n";
			iterator_chain->memory_size += memory_size;
			flag = 1;
			break;
		}
		else if (head_address + memory_size == iterator_chain->head_address)
		{
			//当插入内存块可以和下内存块合并
			//cout << "debug_dwon\n";
			iterator_chain->head_address = head_address;
			iterator_chain->memory_size += memory_size;
			flag = 1;
			break;
		}
		else
		{
			//当插入内存块不能合并
			//cout << "debug_alone\n";
			free_chain_node temp;
			temp.head_address = head_address;
			temp.memory_size  = memory_size;
			fl.push_back(temp);
			flag = 1;
			break;
		}
		if (next != fl.end()) next++;
	}
	if (flag == 0)
	{
		//此处有一个例外，如果插入内存块位于自由链尾部，且不能和合并
		//cout << "debug_alone_alone\n";
		free_chain_node temp;
		temp.head_address = head_address;
		temp.memory_size = memory_size;
		fl.push_back(temp);
	}
	fl.sort();

	//输出当前内存状况
	list<free_chain_node>::iterator pp;
	cout << "\n内存块回收完成,当前内存状态：" << endl;
	pp = fl.begin();
	if (fl.size() != 1) pp++;
	for (; pp != fl.end(); pp++)
	{
		cout << "首地址：" << pp->head_address << "\t内存块大小：" << pp->memory_size << endl;
	}
	return fl;
}

/*
	周转时间
*/
double AverageTurnaroundTime(vector<process_scheduling>& ct){
    auto CountNum = static_cast<int>(ct.size());
    double Count = 0;

    for (auto &Pointer : ct){
        Count += Pointer.TT;
    }

    return (Count / CountNum);
}

double WeightedAverageTurnaroundTime(vector<process_scheduling>& ct){
    auto CountNum = static_cast<int>(ct.size());
    double Count = 0;

    for (auto &Pointer : ct){
        Count += Pointer.WTT;
    }

    return (Count / CountNum);
}

/*
	进程调度
*/
bool ComForSJF(process  a, process  b) {
	return a.runtime < b.runtime;
}

int GodAaronFire(vector<process>& pv) {
	int countd = 0;
	if (pv.size() != 1) {
		for (auto TailPointer = pv.end() - 1; pv.begin() <= TailPointer; TailPointer--) {
			//cout << TailPointer->num << "\t" << TailPointer->runtime <<"\n";
			if (TailPointer == pv.begin() && TailPointer->flag == 0) {
				countd = 0;
				break;
			}

			if (TailPointer->flag == 0) {
				countd++;
				continue;
			}

			else break;
		}
	}
	return countd;
}

void SJF(vector<process>& pv) {
	// Declaration of the SJF function.

	int count = GodAaronFire(pv);

	sort(pv.begin(), pv.end() - count, ComForSJF);

	process temp{};
	auto HeadPointer = pv.begin();                                //Define iterator.
	temp.num = HeadPointer->num;
	temp.runtime = HeadPointer->runtime;
	temp.memory = HeadPointer->memory;
	temp.head_address = HeadPointer->head_address;
    temp.EnterTime = HeadPointer->EnterTime;

	cout << "\n作业号\t" << "开始时间\t" << "结束时间\t" << "周转时间\t" << "带权周转时间\t" << endl;
    cout << temp.num << "\t" << ten_to_sixty(end_time) << "\t\t" << ten_to_sixty(temp.runtime + end_time);

	/* Output test data.
	for (auto &i : pv) {
	cout << "Test Data: " << i.num << "\t" << i.runtime << "\n";
	}
	*/

	end_time += temp.runtime;                            //Renew end time.

    process_scheduling ps{};
    ps.num         =  temp.num;
    ps.EnterTime   =  temp.EnterTime;
    ps.runtime     =  temp.runtime;
    ps.FinishTime  =  end_time;
    ps.TT          =  ps.FinishTime - ps.EnterTime;
    ps.WTT         =  ps.TT / ps.runtime;
    CalculateTurnaround.push_back(ps);

    cout << "\t\t" << turn_around_time_change(ps.TT) << "\t\t" << (ps.WTT) << "\n";



    //Debug Turnaround Time.
    //cout << ten_to_sixty(ps.EnterTime) << "\t Test" << ten_to_sixty(ps.FinishTime);

	end_time -= 0.001;
	memory_allocation(free_chain, wait_for_memory_job_vector, process_vect);
	end_time += 0.001;
	//TODO: Call memory recovery function then continue memory allocation.
	memory_recover(free_chain, temp.head_address, temp.memory);

	if (pv.size() != 1)
		pv.erase(pv.begin());
	else if (pv.size() == 1)
		pv.clear();
}

void FCFS(vector<process>& pv)
{
	//进程号+执行时间+结束时间
	//cout  << pv.begin()->num<<"\t"<< pv.begin()->enter_processor_time <<"\t"<< pv.begin()->enter_processor_time + p->runtime << endl;
	//pv.pop_back();

    auto HeadPointer = pv.begin();
    process tempgx{};
    tempgx.num = HeadPointer->num;
    tempgx.runtime = HeadPointer->runtime;
    tempgx.memory = HeadPointer->memory;
    tempgx.head_address = HeadPointer->head_address;
    tempgx.EnterTime = HeadPointer->EnterTime;

    cout << "\n作业号\t" << "开始时间\t" << "结束时间\t" << "周转时间\t" << "带权周转时间\t" << endl;
    cout << tempgx.num << "\t" << ten_to_sixty(end_time) << "\t\t" << ten_to_sixty(tempgx.runtime + end_time);

    end_time += tempgx.runtime;                            //Renew end time.

    process_scheduling ps{};
    ps.num         =  tempgx.num;
    ps.EnterTime   =  tempgx.EnterTime;
    ps.runtime     =  tempgx.runtime;
    ps.FinishTime  =  end_time;
    ps.TT          =  (ps.FinishTime - ps.EnterTime);
    ps.WTT         =  (ps.TT / ps.runtime);
    CalculateTurnaround.push_back(ps);

    cout << "\t\t" << turn_around_time_change(ps.TT) << "\t\t" << (ps.WTT) << "\n";

    end_time -= 0.001;
    memory_allocation(free_chain, wait_for_memory_job_vector, process_vect);
    end_time += 0.001;

    memory_recover(free_chain, tempgx.head_address, tempgx.memory);

    if (pv.size() != 1)
        pv.erase(pv.begin());
    else if (pv.size() == 1)
        pv.clear();
}

/*
	进制转化
*/
double ten_to_sixty(double x)
{
	// 如果未开启进制转换功能，则直接返回原始值
	if (flagg == 0) return x;
	double temp = x - (int)x;
	temp = temp * 60 / 100;
	x = (int)x + temp;
	return x;
}

double sixty_to_ten(double x)
{
	double temp = x - (int)x;
	temp = temp * 100 / 60;
	x = (int)x + temp;
	return x;
}

double runtime_change(double x)
{
	return  x / 60;
}

double turn_around_time_change(double x)
{
	//如未开启进制转换，则返回原始值
	if (flagg == 0) return x;
	x = ten_to_sixty(x);
	double temp = (int)x * 60;
	x = 100 * (x - (int)x) + temp;

	return x;
}

/*
	初始化程序
*/
void init()
{
	//初始化内存块
	free_chain_node init;
	init.head_address = 0;
	init.memory_size = 0;
	free_chain.push_back(init);

	free_chain_node first;
	first.head_address = 0;
	first.memory_size = MEMORY_SIZE;
	free_chain.push_back(first);

	//数据输入
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		job p;
		cin >> p.num >> p.enter_time >> p.runtime >> p.memory;
		//启动进制转换
		if (flagg == 1)
		{
			p.enter_time = sixty_to_ten(p.enter_time);
			p.runtime = runtime_change(p.runtime);
		}
		wait_for_memory_job_vector.push_back(p);
	}
	end_time = wait_for_memory_job_vector.begin()->enter_time;

	for (int i = 0; i < n; i++)
		cout << wait_for_memory_job_vector[i].num << "\t" << wait_for_memory_job_vector[i].enter_time << "\t" << wait_for_memory_job_vector[i].runtime << endl;
}
