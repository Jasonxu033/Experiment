# ifndef _HSCHEDULE_H_
# define _HSCHEDULE_H_

# include<iostream>
# include<list>
# include<iterator>
# include<vector>
using namespace std;

extern double end_time;                //正占用处理机的进程的结束时间
const  int    MEMORY_SIZE = 100;       //内存总大小100
extern int    flagg;

//自由链节点   空闲内存块
struct free_chain_node
{
	int num;                   //区号
	int head_address;          //首地址
	int memory_size;           //占用内存大小
};

//作业
struct job
{
	int   num;                   //作业号
	double enter_time;            //进入时间
	double runtime;               //运行时间
	int   memory;                //所需内存
	//int   get_memory_time;       //被分配内存时间<我觉得这个没用>
};

//进程
struct process
{
	int   num;                   //进程号（与作业号对应）
	//double enter_processor_time;  //获得处理机时间
	double runtime;               //运行时间
	int   memory;                //所需内存
	int   head_address;          //占用内存的首地址
	int   flag = 1;              //1能调用，0不能调用
    double EnterTime;
};

//处理机分配表
struct process_scheduling
{
	int   num;                   //进程号
	double EnterTime;            //进入系统时间
	double runtime;              //运行时间
    double TT;                   //周转时间
    double WTT;                  //带权周转时间
    double FinishTime;           //结束时间
};

extern list<free_chain_node> free_chain;                 //自由链
extern vector<job>           wait_for_memory_job_vector; //等待内存的作业向量
extern vector<process>       process_vect;               //等待处理机的进程向量
extern vector<double>         enter_processor_time_vect;  //进入处理机时间向量
extern vector<double>         end_time_vect;              //结束时间向量
extern vector<process_scheduling> CalculateTurnaround;

/*
	枚举
	1是BFA算法
	2是FFA算法
	主要用来增加程序可读性
*/
enum memory_alg
{
	BFA_ = 1, FFA_ = 2
};

/*
	枚举
	1是FCFS算法
	2是SJF算法
	主要用来增加程序可读性
*/
enum process_alg
{
	FCFS_ = 1, SJF_ = 2
};

/*
	重载运算符，用来搞定结构体链表的排序
*/
extern int MEMORY_ALLOCATION;  // 1:最先适应算法  2:最佳适应算法
extern int PROCESS_SCHEDULE;   // 1:FCFS 2SJF

bool operator<(const free_chain_node& lhs, const free_chain_node& rhs);

/*
	内存分配
	输入：自由链，等待作业向量，等待进程向量
	传递的是引用值，所以返回值无所谓
*/
void memory_allocation(list<free_chain_node>& fl, vector<job>& jv, vector<process>& pv);

/*
	内存回收
	输入：自由链, flag=1:BFA  flag=2:FFA
	打印：当前自由链状态
	返回：自由链
*/
list<free_chain_node>& memory_recover(list<free_chain_node>& fl, int head_address, int memory_size);
//flag=1
//list<free_chain_node>& BFA(list<free_chain_node>& fl);
//flag=2
//list<free_chain_node>& FFA(list<free_chain_node>& fl);

/*
	先来先服务
	输入：等待处理机的进程向量pv
	打印：每次调度的作业号，开始时间，结束时间
	返回：结束时间
	注意：进程结束时要调用内存回收函数
*/
void FCFS(vector<process>& pv);

/*
	短作业优先
	输入：等待处理机的进程向量pv
	打印：每次调度的作业号，开始时间，结束时间
	注意：进程结束时要调用内存回收函数
	返回：进程结束时间
*/
void SJF(vector<process>& pv);

/*
	计算平均周转时间和平均带权周转时间
*/
double AverageTurnaroundTime(vector<process_scheduling>& ct);

double WeightedAverageTurnaroundTime(vector<process_scheduling>& ct);

/*
	进制间的转换，用来解决时间问题
*/
double ten_to_sixty(double x);
double sixty_to_ten(double x);
double runtime_change(double x);
double turn_around_time_change(double x);

/*
	初始化
*/
void init();

#endif