# encoding:utf-8
import os
import numpy as np
from beautifultable import BeautifulTable
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import cross_validate
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC, SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier
from lightgbm import LGBMClassifier
from sklearn import manifold
from time import time
import matplotlib.pyplot as plt
from sklearn import (manifold, datasets, decomposition, ensemble, random_projection)
from mpl_toolkits.mplot3d.axes3d import Axes3D
from sklearn import (manifold, datasets, decomposition, ensemble, random_projection)
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as lda

# LLE,Isomap,LTSA需要设置n_neighbors这个参数
n_neighbors = 30


def load_corpus(corpus_path):
    corpus = []
    labels = []  # 'Art', 'Enviornment', 'Space', 'Sports', 'Computer', 'Politics', 'Economy', 'Agriculture', 'History'

    with open(corpus_path, 'r', encoding='UTF-8') as f:
        for row in f:
            _content = row[row.index('@') + 1:]
            _label = row[:row.index('@')]
            corpus.append(_content)
            labels.append(_label)
    # 展示部分数据
    # print(corpus[1:3])
    # print(labels[1:3])
    return corpus, labels


def feature_select(corpus, labels, k=1000):
    """
    select top k features through chi-square test
    """
    bin_cv = CountVectorizer(binary=True)
    le = LabelEncoder()
    X = bin_cv.fit_transform(corpus)
    y = le.fit_transform(labels).reshape(-1, 1)

    k = min(X.shape[1], k)
    skb = SelectKBest(chi2, k=k)
    skb.fit(X, y)

    feature_ids = skb.get_support(indices=True)
    feature_names = bin_cv.get_feature_names()
    vocab = {}

    for new_fid, old_fid in enumerate(feature_ids):
        feature_name = feature_names[old_fid]
        vocab[feature_name] = new_fid

    # we only care about the final extracted feature vocabulary
    # print(vocab)
    return vocab


def feature_extract(corpus, labels, vocab):
    """
    feature extraction through TF-IDF
    """
    tfidf_vec = TfidfVectorizer(vocabulary=vocab)
    X = tfidf_vec.fit_transform(corpus)
    print(X)

    le = LabelEncoder()
    y = le.fit_transform(labels).reshape(-1)

    classes = le.classes_
    return X, y, classes


def init_estimators():
    return [
        {'classifier': 'NB', 'model': MultinomialNB()},
        {'classifier': 'LR', 'model': LogisticRegression(random_state=42)},
        {'classifier': 'L-SVM', 'model': LinearSVC(max_iter=1000, random_state=42)},
        {'classifier': 'RBF-SVM', 'model': SVC(max_iter=1000, random_state=42)},
        {'classifier': 'RF', 'model': RandomForestClassifier(n_estimators=100, random_state=42)},
        {'classifier': 'XGB', 'model': XGBClassifier(n_estimators=100, random_state=42)},
        {'classifier': 'LGBM', 'model': LGBMClassifier(n_estimators=100, random_state=42)},
    ]


def get_cv_scores(estimator, X, y, scoring, cv=5):
    return cross_validate(estimator, X, y, cv=cv, n_jobs=1,
                          scoring=scoring,
                          return_train_score=True)


# 将降维后的数据可视化,2维
def plot_embedding_2d(X, y, title=None):
    color = ['blue', 'green', 'red', 'cyan', 'magenta',
             'yellow', 'black', 'white', 'gray', 'pink']

    # 坐标缩放到[0,1]区间
    x_min, x_max = np.min(X, axis=0), np.max(X, axis=0)
    X = (X - x_min) / (x_max - x_min)
    # print(X)
    # print(y)

    # 降维后的坐标为（X[i, 0], X[i, 1]），在该位置画出对应的digits
    for i in range(X.shape[0]):
        plt.scatter(X[i, 0], X[i, 1], c=color[y[i]])

    if title is not None:
        plt.title(title)


# 将降维后的数据可视化,3维
def plot_embedding_3d(X, title=None):
    # 坐标缩放到[0,1]区间
    x_min, x_max = np.min(X, axis=0), np.max(X, axis=0)
    X = (X - x_min) / (x_max - x_min)

    # 降维后的坐标为（X[i, 0], X[i, 1],X[i,2]），在该位置画出对应的digits
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    for i in range(X.shape[0]):
        ax.text(X[i, 0], X[i, 1], X[i, 2], str(digits.target[i]),
                color=plt.cm.Set1(y[i] / 10.),
                fontdict={'weight': 'bold', 'size': 9})

    if title is not None:
        plt.title(title)


if __name__ == '__main__':
    '''
    数据读取，特征选择，特征提取
    '''
    corpus_path = os.path.join(os.path.dirname(__file__), '../../data/corpus/FDU_NLP_corpus_seg_balanced.txt')
    corpus, label = load_corpus(corpus_path)
    vocab = feature_select(corpus, label, k=1000)
    X, y, cls = feature_extract(corpus, label, vocab)
    print(X.shape)
    print(type(X))
    Xd = X.todense()
    # todense


    '''
    数据降维可视化
    '''
    # %%
    # 随机映射
    # print("Computing random projection")
    # t0 = time()
    # rp = random_projection.SparseRandomProjection(n_components=2, random_state=42)
    # X_projected = rp.fit_transform(X)
    # plot_embedding_2d(X_projected, y, "Random Projection (time %.2fs)" % (time() - t0))

    # %%
    # PCA
    print("Computing PCA projection")
    t0 = time()
    X_pca = decomposition.TruncatedSVD(n_components=2).fit_transform(Xd)
    plot_embedding_2d(X_pca[:, 0:2], y, "PCA 2D (time %.2fs)" % (time() - t0))
    # plot_embedding_3d(X_pca, "PCA 3D (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # LDA
    print("Computing LDA projection")
    X2 = Xd.copy()
    X2.flat[::X.shape[1] + 1] += 0.01  # Make X invertible
    t0 = time()
    X_lda = lda(n_components=3).fit_transform(Xd, y)
    plot_embedding_2d(X_lda[:, 0:2], y, "LDA 2D (time %.2fs)" % (time() - t0))
    # plot_embedding_3d(X_lda, "LDA 3D (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # Isomap
    print("Computing Isomap embedding")
    t0 = time()
    X_iso = manifold.Isomap(n_neighbors, n_components=2).fit_transform(Xd)
    print("Done.")
    plot_embedding_2d(X_iso, y, "Isomap (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # standard LLE
    print("Computing LLE embedding")
    clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='standard')
    t0 = time()
    X_lle = clf.fit_transform(Xd)
    print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
    plot_embedding_2d(X_lle, y, "Locally Linear Embedding (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # modified LLE
    print("Computing modified LLE embedding")
    clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='modified')
    t0 = time()
    X_mlle = clf.fit_transform(Xd)
    print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
    plot_embedding_2d(X_mlle, y, "Modified Locally Linear Embedding (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # HLLE
    # print("Computing Hessian LLE embedding")
    # clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='hessian')
    # t0 = time()
    # X_hlle = clf.fit_transform(Xd)
    # print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
    # plot_embedding_2d(X_hlle, y, "Hessian Locally Linear Embedding (time %.2fs)" % (time() - t0))

    # %%
    # LTSA
    # print("Computing LTSA embedding")
    # clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='ltsa')
    # t0 = time()
    # X_ltsa = clf.fit_transform(Xd)
    # print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
    # plot_embedding_2d(X_ltsa, y, "Local Tangent Space Alignment (time %.2fs)" % (time() - t0))

    # %%*dc
    # MDS
    print("Computing MDS embedding")
    clf = manifold.MDS(n_components=2, n_init=1, max_iter=100)
    t0 = time()
    X_mds = clf.fit_transform(Xd)
    print("Done. Stress: %f" % clf.stress_)
    plot_embedding_2d(X_mds, y, "MDS (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # Random Trees
    print("Computing Totally Random Trees embedding")
    hasher = ensemble.RandomTreesEmbedding(n_estimators=200, random_state=0, max_depth=5)
    t0 = time()
    X_transformed = hasher.fit_transform(Xd)
    pca = decomposition.TruncatedSVD(n_components=2)
    X_reduced = pca.fit_transform(X_transformed)

    plot_embedding_2d(X_reduced, y, "Random Trees (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # Spectral
    print("Computing Spectral embedding")
    embedder = manifold.SpectralEmbedding(n_components=2, random_state=0, eigen_solver="arpack")
    t0 = time()
    X_se = embedder.fit_transform(Xd)
    plot_embedding_2d(X_se, y, "Spectral (time %.2fs)" % (time() - t0))
    plt.show()

    # %%
    # t-SNE
    print("Computing t-SNE embedding")
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    t0 = time()
    X_tsne = tsne.fit_transform(Xd)
    print(X_tsne.shape)
    plot_embedding_2d(X_tsne[:, 0:2], y, "t-SNE 2D (time %.2fs)" % (time() - t0))
    # plot_embedding_3d(X_tsne, "t-SNE 3D (time %.2fs)" % (time() - t0))
    plt.show()

    '''
    分类及评估
    '''
    benchmark = BeautifulTable(max_width=100)
    benchmark.column_headers = [
        'classifier',
        'fit_time',
        'score_time',
        'test_precision_micro',
        'test_recall_micro',
        'test_f1_micro'
    ]

    estimators = init_estimators()

    for estimator in estimators:
        print('### %s ###' % estimator['classifier'])
        row = [estimator['classifier'], ]
        scores = get_cv_scores(estimator['model'], X, y, scoring=['precision_micro', 'recall_micro', 'f1_micro'])
        print(scores)
        for k in benchmark.column_headers[1:]:
            row.append(np.mean(scores[k]))
            print(k, np.mean(scores[k]))

        benchmark.append_row(row)

    print(benchmark)
