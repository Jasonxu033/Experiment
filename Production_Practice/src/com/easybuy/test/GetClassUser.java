package com.easybuy.test;

public class GetClassUser {
	public static void main(String[] args) throws ClassNotFoundException {
		//以反射形式获取类对象
		Class cls = Class.forName("com.easybuy.entity.User");
		//获取类所在包的位置
		Package pkg = cls.getPackage();
		System.out.println("类的名字是："+cls.getSimpleName());
		System.out.println("包名是："+pkg.getName());
	}
}
