package com.easybuy.web;

import java.io.IOException;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easybuy.util.EmptyUtils;
import com.easybuy.util.PrintUtil;
import com.easybuy.util.ReturnResult;

/**
 * 公共servlet抽象类（接受页面操作指令，并传递数据信息）
 * @author admin
 *
 */
public abstract class AbstractServlet extends HttpServlet {
	//创建一个抽象方法，用于获得当前操作类文件
	public abstract Class getServletClass();
	
	/**
	 * 页面发生get请求时，自动进到doGet函数
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	/**
	 * 页面发生post请求时，自动进到doPost函数
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//接受页面的操作请求信息
		String actionIndicator = req.getParameter("action");
		//声明方法和结果对象
		Method method = null;
		Object result = null;
		
		try {
			//判断操作信息是否能够获取
			if (EmptyUtils.isEmpty(actionIndicator)){
				result = Execute(req, resp);
			}
			else {
				//获取操作内容对应方法
				method = getServletClass().getDeclaredMethod(actionIndicator, HttpServletRequest.class,
						HttpServletResponse.class);
				result = method.invoke(this, req, resp);
			}
			toView(req, resp, result);
		} catch (NoSuchMethodException e) {
			String viewName = "404.jsp";
			req.getRequestDispatcher(viewName).forward(req, resp);
		} catch (Exception e) {
			e.printStackTrace();
			if(EmptyUtils.isNotEmpty(result)){
				if (result instanceof String) {
					String viewName = "500.jsp";
					req.getRequestDispatcher(viewName).forward(req, resp);
				}
				else {
					ReturnResult returnResult = new ReturnResult();
					returnResult.returnFail("系统错误");
					PrintUtil.write(returnResult, resp);
				}
			}
			else {
				String viewName = "500.jsp";
				req.getRequestDispatcher(viewName).forward(req, resp);
			}
		}
	}
	
	protected void toView(HttpServletRequest req, HttpServletResponse resp, Object result) throws ServletException, IOException {
		if (EmptyUtils.isNotEmpty(result)){
			//结果页面地址应该为String类型
			if (result instanceof String) {
				String viewName = result.toString() + ".jsp";
				//跳转
				req.getRequestDispatcher(viewName).forward(req, resp);
			} else {
				PrintUtil.write(result, resp);
			}
		}
		
	}

	//如果操作不明确，跳转主页
	public Object Execute(HttpServletRequest req, HttpServletResponse resp) {
		return "pre/index";
	}
}
