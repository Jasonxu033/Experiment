package com.easybuy.web.pre;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easybuy.biz.NewsBiz;
import com.easybuy.biz.ProductBiz;
import com.easybuy.biz.ProductCategoryBiz;
import com.easybuy.biz.impl.NewsBizImpl;
import com.easybuy.biz.impl.ProductBizImpl;
import com.easybuy.biz.impl.ProductCategoryBizImpl;
import com.easybuy.entity.News;
import com.easybuy.entity.Product;
import com.easybuy.param.NewsParams;
import com.easybuy.util.Pager;
import com.easybuy.util.ProductCategoryVo;
import com.easybuy.web.AbstractServlet;

/**
 * 首页的数据控制
 * @author admin
 *
 */
@WebServlet(urlPatterns={"/Home"},name="Home")
public class HomeServlet extends AbstractServlet {

	
	private ProductBiz productBiz;///商品操作
	private NewsBiz newsBiz;//新闻层
	private ProductCategoryBiz productCategoryBiz;
	
	@Override
	public void init() throws ServletException {
		productBiz=new ProductBizImpl();
		newsBiz=new NewsBizImpl();
		productCategoryBiz=new ProductCategoryBizImpl();
	}
	
	@Override
	public Class getServletClass() {
		return HomeServlet.class;
	}
	
	public String index(HttpServletRequest request,HttpServletResponse response) throws Exception{
		//查询商品分类
		List<ProductCategoryVo> productCategoryVoList=productCategoryBiz.queryAllProductCategoryList();		
		Pager pager=new Pager(5, 5, 1);
		NewsParams param=new NewsParams();
		param.openPage((pager.getCurrentPage()-1)*pager.getRowPerPage(), pager.getRowPerPage());
		List<News> news = newsBiz.queryNewsList(param);
		//查询商品分类
		for (ProductCategoryVo vo : productCategoryVoList) {
			List<Product> productList = productBiz.getProductionList(1, 8, null, vo.getProductCategory().getId(),1);
			vo.setProductList(productList);
		}
		request.setAttribute("productCategoryVoList", productCategoryVoList);
		request.setAttribute("news", news);
		
		return "/pre/index";
		
	}

}
