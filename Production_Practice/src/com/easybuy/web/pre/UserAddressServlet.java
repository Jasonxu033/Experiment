package com.easybuy.web.pre;

import javax.servlet.ServletException;

import com.easybuy.biz.UserAddressBiz;
import com.easybuy.biz.impl.UserAddressBizImpl;
import com.easybuy.web.AbstractServlet;

public class UserAddressServlet extends AbstractServlet {

	private UserAddressBiz userAddressBiz;
	@Override
	public Class getServletClass() {
		return UserAddressServlet.class;
	}
	
	@Override
	public void init() throws ServletException {
		userAddressBiz=new UserAddressBizImpl();
	}

}
