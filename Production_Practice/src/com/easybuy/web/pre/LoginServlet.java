package com.easybuy.web.pre;

import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.tool.EclipseBatchRequestor;

import com.easybuy.biz.UserBiz;
import com.easybuy.biz.impl.UserBizImpl;
import com.easybuy.entity.User;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.ReturnResult;
import com.easybuy.web.AbstractServlet;

/**
 * 登录页面的数据处理
 * @author admin
 *
 */
// login.jsp
@WebServlet(urlPatterns={"/Login"}, name="Login")
public class LoginServlet extends AbstractServlet {

	//注入业务层处理对象
	private UserBiz userBiz;
	
	/**
	 * 初始化biz对象
	 */
	@Override
	public void init() throws ServletException {
		userBiz = new UserBizImpl();
	}
	
	/**
	 * 获取当前操作类
	 */
	@Override
	public Class getServletClass() {
		return LoginServlet.class;
	}
	
	/**
	 * 跳转到登录页面
	 */
	public String toLogin(HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		return "/pre/login";
	}
	
	/**
	 * 登录方法
	 */
	public ReturnResult login(HttpServletRequest request, 
			HttpServletResponse response) throws Exception{
		ReturnResult result = new ReturnResult();
		//获取页面的账号和密码
		String loginName = request.getParameter("loginName");
		String password = request.getParameter("password");
		//验证账号密码
		User user = userBiz.getUser(null, loginName);
		if (EmptyUtils.isEmpty(user)) {
			result.returnFail("用户不存在");
		}else{
			if (user.getPassword().equals(password)){
				request.getSession().setAttribute("loginUser", user);
				result.returnSuccess("登陆成功");
			}
			else {
				result.returnFail("密码错误");
			}
		}
		return result;
	}
	/**
	 * 退出登录
	 */
	public String loginOut(HttpServletRequest request, 
			HttpServletResponse response)throws Exception{
		ReturnResult result = new ReturnResult();
		
		try {
			User user = (User) request.getSession().getAttribute("loginUser");
			//清除登录账户的购物车信息
			request.getSession().removeAttribute("cart");
			request.getSession().removeAttribute("cart2");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		result.returnSuccess("注销成功");
		return "/pre/login";
	}
}
