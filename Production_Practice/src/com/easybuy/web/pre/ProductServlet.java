package com.easybuy.web.pre;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.easybuy.biz.ProductBiz;
import com.easybuy.biz.ProductCategoryBiz;
import com.easybuy.biz.impl.ProductBizImpl;
import com.easybuy.biz.impl.ProductCategoryBizImpl;
import com.easybuy.entity.Product;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.Pager;
import com.easybuy.util.ProductCategoryVo;
import com.easybuy.web.AbstractServlet;
/**
 * 商品业务数据操作
 * @author Sheng
 *
 */
@WebServlet(urlPatterns={"/Product"},name="Product")
public class ProductServlet extends AbstractServlet{

	private ProductBiz productBiz;//商品业务操作
	private ProductCategoryBiz productCategoryBiz;//商品分类业务操作
	
	
	@Override
	public void init() throws ServletException {
		productBiz=new ProductBizImpl();
		productCategoryBiz=new ProductCategoryBizImpl();
		
	}
	
	@Override
	public Class getServletClass() {
		
		return ProductServlet.class;
	}

	/**
	 * 添加最近浏览的商品
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private void addRecentProduct(HttpServletRequest request,Product product)throws Exception{
		//创建会话记录对象
		HttpSession session=request.getSession();
		//从请求中获取商品集合
		List<Product> recentProducts=queryRecentProducts(request);
		//判断 是否装满浏览记录
		if (recentProducts.size()>0 && recentProducts.size()==10) {
			recentProducts.remove(0);
		}
		recentProducts.add(recentProducts.size(),product);
		session.setAttribute("recentProducts", recentProducts);
	}

	private List<Product> queryRecentProducts(HttpServletRequest request) {
		HttpSession session=request.getSession();
		List<Product> recentProducts=(List<Product>) session.getAttribute("recentProducts");
		//如果页面没有传过来浏览的商品记录，则创建一个空的集合，里面没有信息
		if (EmptyUtils.isEmpty(recentProducts)) {
			recentProducts=new ArrayList<Product>();
		}
		
		return recentProducts;
	}
	
	/**
	 * 查询商品列表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public String queryProductList(HttpServletRequest request,HttpServletResponse response)throws Exception{
		//获取商品列表信息
		String category=request.getParameter("category");
		String levelStr=request.getParameter("level");
		String currentPageStr=request.getParameter("currentPage");
		String  keyWord=request.getParameter("keyWord");
		String pageSizeStr=request.getParameter("pagesize");
		
		int rowPerPage=EmptyUtils.isEmpty(pageSizeStr)? 
				20:Integer.parseInt(pageSizeStr);
		
		int currentPage=EmptyUtils.isEmpty(currentPageStr)? 
				1:Integer.parseInt(currentPageStr);
		
		int level=EmptyUtils.isEmpty(levelStr)? 
				0:Integer.parseInt(levelStr);
		
		Integer categoryId=EmptyUtils.isEmpty(category)? 
				null:Integer.valueOf(category);
		//根据页面信息查询总条数
		int total=productBiz.queryProductCount(keyWord, categoryId, level);
		Pager pager=new Pager(total,rowPerPage,currentPage);
		//添加分页对象中的地址
		pager.setUrl("/product?action=queryProductList&level="+level
				+"&category="+(EmptyUtils.isEmpty(category)?"":category));
		List<ProductCategoryVo> productCategoryVoList= productCategoryBiz.queryAllProductCategoryList();
		List<Product> productList=productBiz.getProductionList(currentPage, rowPerPage, keyWord, categoryId, level);
		request.setAttribute("productList",productList );
		request.setAttribute("pager", pager);
		request.setAttribute("total", total);
		request.setAttribute("keyWord",keyWord );
		request.setAttribute("productCategoryVoList",productCategoryVoList );
		
		return "/pre/product/queryProductList";
		
	}
	/**
	 * 浏览商品
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	public String queryProductDeatil(HttpServletRequest request,HttpServletResponse response)throws Exception{
		String id=request.getParameter("id");
		Product product=productBiz.getProductById(Integer.valueOf(id));
		List<ProductCategoryVo> productCategoryVoList= productCategoryBiz.queryAllProductCategoryList();
		request.setAttribute("product", product);
		request.setAttribute("productCategoryVoList", productCategoryVoList);
		addRecentProduct(request, product);
		return "/pre/product/productDeatil";
		
	}
	
}
