package com.easybuy.web.pre;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easybuy.biz.NewsBiz;
import com.easybuy.biz.impl.NewsBizImpl;
import com.easybuy.web.AbstractServlet;
/**
 * 新闻的查看
 * @author GX
 *
 */
@WebServlet(urlPatterns={"/News"},name="News")
public class NewsServlet extends AbstractServlet {

	private NewsBiz newsBiz;//新闻数据的业务处理
	
	@Override
	public void init() throws ServletException {
		newsBiz=new NewsBizImpl();
	
	}
	
	@Override
	public Class getServletClass() {
		
		return NewsServlet.class;
	}
	
	public String index(HttpServletRequest request,HttpServletResponse response){
		return "/pre/newsList";
	}

}
