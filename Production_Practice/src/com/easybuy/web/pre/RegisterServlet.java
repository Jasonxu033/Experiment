package com.easybuy.web.pre;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easybuy.biz.UserBiz;
import com.easybuy.biz.impl.UserBizImpl;
import com.easybuy.entity.User;
import com.easybuy.util.Constants;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.ReturnResult;
import com.easybuy.web.AbstractServlet;

/**
 * 注册
 * @author admin
 *
 */
@WebServlet(urlPatterns={"/Register"},name="Register")
public class RegisterServlet extends AbstractServlet {
	
	private UserBiz userBiz;//用户数据业务操作

	@Override
	public void init() throws ServletException {
		userBiz=new UserBizImpl();
	}
	
	
	@Override
	public Class getServletClass() {
		return RegisterServlet.class;
	}

	/**
	 * 跳转到注册
	 * @return
	 * @throws Exception
	 */
	public String toRegister(HttpServletRequest request,HttpServletResponse responce) throws Exception{
		return "/pre/register";
	}
	
	public ReturnResult saveUserToDatabase(HttpServletRequest request,HttpServletResponse responce) throws Exception{
		ReturnResult result=new ReturnResult();
		
		try {
			User user=new User();
			//页面输入的名字存在不
			String loginName=request.getParameter("loginName");
			User oldUser = userBiz.getUser(null, loginName);
			if(EmptyUtils.isNotEmpty(oldUser)){
				result.returnFail("用户已存在");
				return result;
			}
			user.setLoginName(loginName);
			user.setUserName(request.getParameter("userName"));
			user.setSex(EmptyUtils.isEmpty(request.getParameter("sex")) ? 1:0);
			user.setPassword(request.getParameter("password"));
			user.setIdentityCode(request.getParameter("identityCode"));
			user.setEmail(request.getParameter("email"));
			user.setMobile(request.getParameter("mobile"));
			user.setType(Constants.UserType.PRE);
			//检查用户信息
			result=checkUser(user);
			if(result.getStatus()==Constants.ReturnResult.SUCCESS){
				if(!userBiz.add(user)){
					return result.returnFail("注册失败");
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.returnSuccess("注册成功");
		return result;
	}


	private ReturnResult checkUser(User user) {
		//验证数据是否合理
		ReturnResult result=new ReturnResult();
		
		return result.returnSuccess();
	}
	
	
}
