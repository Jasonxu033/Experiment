package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.User;

/**
 * 用户业务层功能
 * @author admin
 *
 */
public interface UserBiz {
	//新增用户
	boolean add(User user);
	//更新用户
	boolean update(User user);
	//删除用户
	boolean deleteUserById(Integer id);
	//查询用户的条数
	Integer count();
	//分页查询所有的用户
	//currentPageNumber:当前页码，pageSize:每一页条数
	List<User> getUserList(Integer currentPageNumber, Integer pageSize);
	//查询单个用户
	User getUser(Integer id, String loginName);
}
