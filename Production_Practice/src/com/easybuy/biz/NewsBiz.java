package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.News;
import com.easybuy.param.NewsParams;

public interface NewsBiz {
	/**
	 * 更新新闻
	 * @param news
	 * @throws Exception
	 */
	void updateNews(News news);
	
	/**
	 * 添加新闻
	 * @param news
	 * @throws Exception
	 */
	void addNews(News news);
	
	/**
	 * 删除新闻
	 * @param id
	 * @throws Exception
	 */
	void deleteNews(String parameter);
	
	News getNewsById(String parameter);
	
	/**
	 * 查询新闻列表
	 * NewsParams:新闻扩展类
	 * @return
	 */
	List<News> queryNewsList(NewsParams params);
	
	/**
	 *  查询新闻数量
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Integer queryNewsCount(NewsParams params);
}
