package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.Product;

public interface ProductBiz {
	boolean add(Product product);
	//修改商品
	boolean update(Product product);
	//修改商品库存数量
	boolean updateStock(Integer productId, Integer stock);
	
	boolean deleteProductById(Integer productId);
	
	Product getProductById(Integer productId);
	//根据商品名称，分类，等级查询商品数量
	Integer queryProductCount(String proName, Integer categoryId, Integer level);
	/**
	 * 根据条件查询所有商品
	 * @param currentPage 页码
	 * @param pageSize 每一页的数量
	 * @param proName 商品名称
	 * @param categoryId 商品的分类 
	 * @param level 商品的等级
	 * @return
	 * @throws Exception
	 */
	List<Product> getProductionList(Integer currentPage, Integer pageSize, String proName,
			Integer categoryId, Integer level);
}
