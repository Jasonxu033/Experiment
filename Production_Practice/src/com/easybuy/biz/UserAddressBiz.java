package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.UserAddress;
import com.easybuy.param.UserAddressParam;

/**
 * 用户地址业务层
 * @author admin
 *
 */
public interface UserAddressBiz {
	//查询用户所有的地址
	List<UserAddress> queryUserAddressList(Integer id);
	//添加
	Integer addUserAddress(Integer id, String address, String remark);
	//通过id查询地址
	UserAddress getUserAddressById(Integer addressId);	
}
