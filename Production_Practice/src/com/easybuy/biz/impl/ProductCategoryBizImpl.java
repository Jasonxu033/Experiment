package com.easybuy.biz.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.easybuy.biz.ProductCategoryBiz;
import com.easybuy.dao.ProductCategoryDao;
import com.easybuy.dao.impl.ProductCategoryDaoImpl;
import com.easybuy.entity.ProductCategory;
import com.easybuy.param.ProductCategoryParam;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.ProductCategoryVo;
import com.sun.java.swing.plaf.windows.WindowsBorders.DashedBorder;

public class ProductCategoryBizImpl implements ProductCategoryBiz {

	@Override
	public void addProductCategory(ProductCategory pc) {
		Connection connection=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			dao.add(pc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
	}

	@Override
	public void updateProductCategory(ProductCategory pc) {
		Connection connection=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			dao.update(pc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
	}

	@Override
	public void deleteById(Integer id) {
		Connection connection=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			dao.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
	}

	@Override
	public Integer queryProductCategoryCount(ProductCategoryParam param) {
		Connection connection=null;
		Integer count=0;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			count =dao.queryProductCategoryCount(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count;
	}

	@Override
	public ProductCategory queryProductCategoryById(Integer id) {
		Connection connection=null;
		ProductCategory productCategory=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			productCategory =dao.queryProductCategoryById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		return productCategory;
	}

	@Override
	public List<ProductCategory> queryProductCategoryList(ProductCategoryParam param) {
		Connection connection=null;
		List<ProductCategory> productCategoryList=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao =new ProductCategoryDaoImpl(connection);
			productCategoryList =dao.queryProductCategoryList(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		return productCategoryList;
	}

	@Override
	public List<ProductCategoryVo> queryAllProductCategoryList() {
		//创建1级分类的列表
		List<ProductCategoryVo> productCategoryVo1List=new ArrayList<ProductCategoryVo>();
		
		//创建1级分类
		List<ProductCategory> productCategory1List
					=getProductCategories(null); 
		//根据1级循环查2级
		for (ProductCategory product1Category : productCategory1List) {
			//创建1级分类封装对象
			ProductCategoryVo productCategoryVo=new ProductCategoryVo();
			productCategoryVo.setProductCategory(product1Category);
			//创建2级分类列表
			List<ProductCategoryVo> productCategoryVo1ChildList
				=new ArrayList<ProductCategoryVo>();
			
			//创建2级分类
			List<ProductCategory> productCategory2List
			=getProductCategories(null);
			//2级循环查3级
			
			for (ProductCategory product2Category : productCategory2List) {
				ProductCategoryVo productCategoryVo2=new ProductCategoryVo();
				//
				productCategoryVo1ChildList.add(productCategoryVo2);
				productCategoryVo2.setProductCategory(product2Category);
				
				List<ProductCategoryVo> productCategoryVo2ChildList
				=new ArrayList<ProductCategoryVo>();
			
				productCategoryVo2.setProductCategoryVoList(productCategoryVo2ChildList);
				
				List<ProductCategory> productCategory3List
				=getProductCategories(product2Category.getId());
				
				
				//循环查3级
				for (ProductCategory product3Category : productCategory3List) {
					//
					ProductCategoryVo productCategoryVo3=new ProductCategoryVo();
					productCategoryVo3.setProductCategory(product3Category);
					productCategoryVo2ChildList.add(productCategoryVo3);
				}
			}
			productCategoryVo.setProductCategoryVoList(productCategoryVo1ChildList);
			productCategoryVo1List.add(productCategoryVo);
			
			
		}
		return productCategoryVo1List;
	}
/**
 * 根据父级Id查询子分类
 * @param parentId
 * @return
 */
	private List<ProductCategory> getProductCategories(Integer parentId) {
		Connection connection=null;
		List<ProductCategory> productCategoryList=null;
		try {
			connection =DataSourceUtil.openConnection();
			ProductCategoryDao dao=new ProductCategoryDaoImpl(connection);
			ProductCategoryParam param=new ProductCategoryParam();
			if(EmptyUtils.isNotEmpty(parentId)){
				param.setParentId(parentId);
			}else{
				param.setParentId(0);
			}
			productCategoryList=dao.queryProductCategoryList(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return productCategoryList;
	}

}
