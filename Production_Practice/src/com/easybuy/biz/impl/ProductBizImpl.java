package com.easybuy.biz.impl;

import java.sql.Connection;
import java.util.List;

import com.easybuy.biz.ProductBiz;
import com.easybuy.dao.NewsDao;
import com.easybuy.dao.ProductDao;
import com.easybuy.dao.impl.NewsDaoImpl;
import com.easybuy.dao.impl.ProductDaoImpl;
import com.easybuy.entity.Product;
import com.easybuy.util.DataSourceUtil;

public class ProductBizImpl implements ProductBiz {

	@Override
	public boolean add(Product product) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			count = dao.add(product);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public boolean update(Product product) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			count = dao.update(product);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public boolean updateStock(Integer productId, Integer stock) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			count = dao.updateStock(productId, stock);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public boolean deleteProductById(Integer productId) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			count = dao.deleteProductById(productId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public Product getProductById(Integer productId) {
		Connection connection = null;
		Product product = null;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			product = dao.getProductById(productId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return product;
	}

	@Override
	public Integer queryProductCount(String proName, Integer categoryId, Integer level) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			count = dao.queryProductCount(proName, categoryId, level);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count;
	}

	@Override
	public List<Product> getProductionList(Integer currentPage, Integer pageSize, String proName, Integer categoryId,
			Integer level) {
		Connection connection = null;
		List<Product> productList=null;
		
		try {
			connection = DataSourceUtil.openConnection();
			ProductDao dao = new ProductDaoImpl(connection);
			productList = dao.getProductionList(currentPage, pageSize, proName, categoryId, level);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return productList;
	}

}
