package com.easybuy.biz.impl;

import java.sql.Connection;
import java.util.List;

import com.easybuy.biz.UserAddressBiz;
import com.easybuy.dao.UserAddressDao;
import com.easybuy.dao.impl.UserAddressDaoImpl;
import com.easybuy.entity.UserAddress;
import com.easybuy.param.UserAddressParam;
import com.easybuy.util.DataSourceUtil;

public class UserAddressBizImpl implements UserAddressBiz {

	@Override
	public List<UserAddress> queryUserAddressList(Integer id) {
		Connection connection = null;
		List<UserAddress> userAddressList = null;
		
		try {
			connection = DataSourceUtil.openConnection();
			//声明接口，构建对象
			UserAddressDao dao=new UserAddressDaoImpl(connection);
			UserAddressParam param = new UserAddressParam();
			param.setUserId(id);
			//调用dao操作
			userAddressList=dao.queryUserAddressList(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return userAddressList;
	}

	@Override
	public Integer addUserAddress(Integer id, String address, String remark) {
		Connection connection = null;
		Integer count=null;
		
		try {
			connection = DataSourceUtil.openConnection();
			//声明接口，构建对象
			UserAddressDao dao=new UserAddressDaoImpl(connection);
			UserAddress userAddress = new UserAddress();
			userAddress.setUserId(id);
			userAddress.setAddress(address);
			userAddress.setRemark(remark);
			//调用dao操作
			count=dao.add(userAddress);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		
		return count;
	}

	@Override
	public UserAddress getUserAddressById(Integer addressId) {
		Connection connection = null;
		UserAddress userAddress=null;
		
		try {
			connection = DataSourceUtil.openConnection();
			//声明接口，构建对象
			UserAddressDao dao=new UserAddressDaoImpl(connection);
			//调用dao操作
			userAddress=dao.getUserAddressById(addressId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return userAddress;
	}

}
