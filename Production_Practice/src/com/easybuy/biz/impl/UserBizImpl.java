package com.easybuy.biz.impl;

import java.sql.Connection;
import java.util.List;

import com.easybuy.biz.UserBiz;
import com.easybuy.dao.UserDao;
import com.easybuy.dao.impl.UserDaoImpl;
import com.easybuy.entity.User;
import com.easybuy.util.DataSourceUtil;

/**
 * 用户业务层,对页面的所有操作都在biz里面
 * @author admin
 *
 */
public class UserBizImpl implements UserBiz {
	
	private UserDao dao=null;
	
	/**
	 * 添加用户
	 */
	@Override
	public boolean add(User user) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			count = dao.add(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}
	
	/**
	 * 修改用户信息
	 */
	@Override
	public boolean update(User user) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			count = dao.update(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public boolean deleteUserById(Integer id) {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			count = dao.deleteUserById(id+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count>0;
	}

	@Override
	public Integer count() {
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			count = dao.count();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return count;
	}

	@Override
	public List<User> getUserList(Integer currentPageNumber, Integer pageSize) {
		Connection connection = null;
		List<User> userList = null;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			userList = dao.getUserList(currentPageNumber, pageSize);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return userList;
	}

	@Override
	public User getUser(Integer id, String loginName) {
		Connection connection = null;
		User user = null;
		
		try {
			connection = DataSourceUtil.openConnection();
			// 接口dao的妙用
			UserDao dao = new UserDaoImpl(connection);
			user = dao.getUser(id, loginName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			DataSourceUtil.closeConnection(connection);
		}
		return user;
	}

}
