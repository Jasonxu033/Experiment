package com.easybuy.biz.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.easybuy.biz.OrderBiz;
import com.easybuy.dao.OrderDao;
import com.easybuy.dao.OrderDetailDao;
import com.easybuy.dao.ProductDao;
import com.easybuy.dao.impl.OrderDaoImpl;
import com.easybuy.dao.impl.OrderDetailDaoImpl;
import com.easybuy.dao.impl.ProductDaoImpl;
import com.easybuy.entity.Order;
import com.easybuy.entity.OrderDetail;
import com.easybuy.entity.User;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.ShoppingCartItem;
import com.easybuy.util.StringUtils;
import com.easybuy.util.ShoppingCart;

public class OrderBizImpl implements OrderBiz {

	@Override
	public Order payShoppingCart(ShoppingCart cart, User user, String address) {
		Connection connection =null;
		Order order=new Order();
		
		try {
			connection =DataSourceUtil.openConnection();
			//设置连接对象的操作模式
			connection.setAutoCommit(false);
			ProductDao productDao=new ProductDaoImpl(connection);
			//订单
			OrderDao orderDao=new OrderDaoImpl(connection);
			//订单详情
			OrderDetailDao orderDetailDao =new OrderDetailDaoImpl(connection);
			//增加订单
			order.setUserId(user.getId());
			order.setLoginName(user.getLoginName());
			order.setUserAddress(address);
			order.setCreateTime(new Date());
			order.setCost(cart.getTotalCost());
			order.setSerialNumber(StringUtils.randomUUID());
			orderDao.add(order);
			for (ShoppingCartItem item : cart.getItems()) {
				OrderDetail orderDetail=new OrderDetail();
				orderDetail.setOrderId(order.getId());
				orderDetail.setCost(item.getCost());
				orderDetail.setProduct(item.getProduct());
				orderDetail.setQuantity(item.getQuantity());
				orderDetailDao.add(orderDetail);
				productDao.updateStock(item.getProduct().getId(), item.getQuantity());
				connection.commit();
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}finally{
			try {
				//还原
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DataSourceUtil.closeConnection(connection);
		}
		
		return null;
	}

	@Override
	public List<Order> getOrderList(Integer userId, Integer currentPageNo, Integer pageSize) {
		Connection connection=null;
		List<Order> orderList=new ArrayList<Order>();
		
		try {
			connection=DataSourceUtil.openConnection();
			OrderDao dao=new OrderDaoImpl(connection);
			OrderDetailDao detailDao=new OrderDetailDaoImpl(connection);
			orderList = dao.getOrderList(userId, currentPageNo, pageSize);
			//循环将订单中的详情添加到订单对象
			for (Order order : orderList) {
				
				//根据订单编号查出订单的详情列表，加到订单对象中
				order.setOrderDetails(detailDao.getOrderDetailList(order.getId()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return orderList;
	}

	@Override
	public Integer count(Integer userId) {
		Connection connection=null;
		Integer count=0;
		try {
			connection=DataSourceUtil.openConnection();
			OrderDao dao=new OrderDaoImpl(connection);
			count=dao.count(userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return count;
	}

	@Override
	public List<OrderDetail> getOrderDetailList(Integer orderId) {
		Connection connection=null;
		List<OrderDetail> orderDetailList=new ArrayList<OrderDetail>();
		
		try {
			connection=DataSourceUtil.openConnection();
			OrderDetailDao detailDao=new OrderDetailDaoImpl(connection);
			orderDetailList=detailDao.getOrderDetailList(orderId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DataSourceUtil.closeConnection(connection);
		}
		
		return orderDetailList;
	}

}
