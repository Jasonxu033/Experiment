package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.ProductCategory;
import com.easybuy.param.ProductCategoryParam;
import com.easybuy.util.ProductCategoryVo;

/**
 * 商品分类业务功能
 * @author admin
 *
 */
public interface ProductCategoryBiz {
	void addProductCategory(ProductCategory productCategory);
	
	void updateProductCategory(ProductCategory productCategory);
	
	void deleteById(Integer id);
	
	Integer queryProductCategoryCount(ProductCategoryParam param);
	
	ProductCategory queryProductCategoryById(Integer id);
	//查询商品的分类列表
	List<ProductCategory> queryProductCategoryList(ProductCategoryParam param);
	/**
	 * 查询全部商品分类信息
	 * @param param
	 * @return
	 */
	List<ProductCategoryVo> queryAllProductCategoryList();
}
