package com.easybuy.biz;

import java.util.List;

import com.easybuy.entity.Order;
import com.easybuy.entity.OrderDetail;
import com.easybuy.entity.User;
import com.easybuy.util.ShoppingCart;

/**
 * 确定订单功能的业务层接口
 * @author admin
 *
 */
public interface OrderBiz {
	/**
	 * 结算订单
	 */
	Order payShoppingCart(ShoppingCart cart, User user, String address);
	
	/**
	 * 分页显示订单的列表
	 */
	List<Order> getOrderList(Integer UserId, Integer currentPageNo, Integer pageSize);
	
	/**
	 * 查询条数
	 */
	Integer count(Integer userId);
	
	/**
	 * 根据订单id，查询订单详情列表
	 */
	List<OrderDetail> getOrderDetailList(Integer orderId);
	
}
