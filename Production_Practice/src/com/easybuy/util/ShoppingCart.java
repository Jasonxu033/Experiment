package com.easybuy.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.easybuy.entity.Product;

/**
 * 用于购物车整理的实体工具类
 * @author admin
 *
 */
public class ShoppingCart implements Serializable {
	//创建一个购物车对象集合
	List<ShoppingCartItem> items = new ArrayList<ShoppingCartItem>();
	//存放购物车总价格
	private Double sum;
	
	//添加
	public ReturnResult addItem(Product product, Integer quantity){
		ReturnResult result=new ReturnResult();
		int flag=0;
		for (int i = 0; i < items.size(); i++) {
			if((items.get(i).getProduct().getId()).equals(product.getId())){
				if(items.get(i).getQuantity()+quantity>product.getStock()){
					return result.returnFail("商品数量不足");
				}
				else{
					items.get(i).setQuantity(items.get(i).getQuantity()+quantity);
					flag=1;
				}
			}
		}
		//没有此商品时
		if(quantity>product.getStock()){
			return result.returnFail("商品数量不足");
		}
		//购物车中没有此商品，则添加该商品进购物车
		if(flag==0){
			items.add(new ShoppingCartItem(product, quantity));
		}
		return result.returnSuccess();
	}
	//移除
	public void removeItem(int index){
		items.remove(index);
	}
	//修改数量
	public void modifyQuantity(int index, Integer quantity){
		items.get(index).setQuantity(quantity);
	}
	//计算总价格
	public float getTotalCost(){
		float sum = 0;
		for (ShoppingCartItem shoppingCarItem : items) {
			sum = sum + shoppingCarItem.getCost();
		}
		return sum;
	}
	
	public List<ShoppingCartItem> getItems() {
		return items;
	}
	public void setItems(List<ShoppingCartItem> items) {
		this.items = items;
	}
	public Double getSum() {
		return sum;
	}
	public void setSum(Double sum) {
		this.sum = sum;
	}
	
}
