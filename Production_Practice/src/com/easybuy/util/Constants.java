package com.easybuy.util;

/**
 * 记录状态信息的静态值的工具类
 * @author admin
 *
 */
public class Constants {
	/**
	 * 购物车返回数据的status表示说明
	 */
	public static interface ReturnResult{
		public static int SUCCESS=1;
		public static int FAIL = -1;
	}
	
	/**
	 * 前后台用户登录的身份
	 */
	public static interface UserType{
		public static int PRE=1;
		public static int BACKEND=-1;
	}
}
