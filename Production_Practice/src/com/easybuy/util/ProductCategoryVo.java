package com.easybuy.util;

import java.io.Serializable;
import java.util.List;

import com.easybuy.entity.Product;
import com.easybuy.entity.ProductCategory;

/**
 * 用于查询所有的商品分类
 * 实体类模型工具
 * @author admin
 *
 */
public class ProductCategoryVo implements Serializable {
	//商品分类对象
	private ProductCategory productCategory;
	//所有的商品分类对象集合
	private List<ProductCategoryVo> productCategoryVoList;
	//所有的商品集合
	private List<Product> productList;
	
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	public List<ProductCategoryVo> getProductCategoryVoList() {
		return productCategoryVoList;
	}
	public void setProductCategoryVoList(List<ProductCategoryVo> productCategoryVoList) {
		this.productCategoryVoList = productCategoryVoList;
	}
	public List<Product> getProductList() {
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
}
