package com.easybuy.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @author admin
 *
 */
public class PrintUtil {

	public static void write(Object result, HttpServletResponse resp) {
		resp.setContentType("text/html;charset=utf-8");
		//将result结果转为json数据
		String json = JSONObject.toJSONString(result);
		print(json, resp);
	}

	private static void print(String json, HttpServletResponse resp) {
		PrintWriter writer = null;
		
		try {
			if (resp!=null) {
				writer = resp.getWriter();
				//写出结果信息
				writer.print(json);
				//保存
				writer.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			writer.close();
		}
	}

}
