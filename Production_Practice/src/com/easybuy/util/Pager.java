package com.easybuy.util;

import java.io.Serializable;

/**
 * 分页数据处理
 * @author admin
 *
 */
public class Pager implements Serializable {
	private int rowCount;//总条数
	private int rowPerPage;//每一页的条数
	private int pageCount;//总页数
	private int currentPage;//当前页
	
	private String url;

	//构造函数
	public Pager(int rowCount, int rowPerPage, int currentPage) {
		super();
		this.rowCount = rowCount;
		this.rowPerPage = rowPerPage;
		this.currentPage = currentPage;
		
		//根据总条数和每一页的条数，算出总页数
		if(this.rowCount%this.rowPerPage==0){
			this.pageCount=this.rowCount/this.rowPerPage;
		}
		else if (this.rowCount%this.rowPerPage>0){
			this.pageCount=this.rowCount/this.rowPerPage+1;
		}
		else{//异常数据
			this.pageCount=0;
		}
		
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getRowPerPage() {
		return rowPerPage;
	}

	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	
}
