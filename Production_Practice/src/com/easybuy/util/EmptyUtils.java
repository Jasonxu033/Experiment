package com.easybuy.util;

import java.util.Collection;
import java.util.Map;

/**
 * 验证内容是否为空的工具类
 * @author PinjieXu
 *
 */
public class EmptyUtils {
	/**
	 * 判断是否为空
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj){
		if(obj==null){
			return true;
		}
		//obj是否等于单个字符类型
		if(obj instanceof CharSequence){
			return ((CharSequence)obj).length()==0;
		}
		//obj是否为空Collection
		if(obj instanceof Collection){
			return ((Collection)obj).isEmpty();
		}
		//obj是否为空Map
		if(obj instanceof Map){
			return ((Map)obj).isEmpty();
		}
		//obj是否为空数组
		if(obj instanceof Object[]){
			Object[] objects = (Object[])obj;
			if(objects.length==0){
				return true;
			}
			
			//如果该长度不为空，但里面的元素是空的话，还是认为该数组为空
			boolean empty=true;
			for (int i = 0; i < objects.length; i++) {
				if(!isEmpty(objects[i])){
					empty=false;
					break;
				}
			}
			return empty;	
		}
		//以上都不满足，就是非空
		return false;
	}
	
	/**
	 * 判断是否不为空
	 * @param obj
	 * @return
	 */
	public static boolean isNotEmpty(Object obj){
		return !isEmpty(obj);
	}
	
	/**
	 * 不知道是什么类型的判断
	 * @param args
	 * @return
	 */
	private boolean validPropertyEmpty(Object...args){
		///使用...可以传随便数量的参数，最后把这些参数变成args数组
		for (int i = 0; i < args.length; i++) {
			if(EmptyUtils.isEmpty(args[i])){
				return true;
			}
		}
		return false;
	}
}
