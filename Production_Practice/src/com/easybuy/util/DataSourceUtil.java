package com.easybuy.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 用于连接数据库的类
 * @author Pinjie Xu
 *
 */
public class DataSourceUtil {
	private static String url;//数据库链接
	private static String driver;//驱动
	private static String username;//账号
	private static String password;//密码
	
	//代码块
	static{
		init();
	}

	private static void init() {
		//创建Properties文件对象params
		Properties params = new Properties();
		String configFile = "database.properties";
		//获取configfile里的文件资源
		InputStream is = DataSourceUtil.class.getClassLoader().getResourceAsStream(configFile);
		//用params加载is资源
		try {
			params.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//获取database.properties文件里的资源
		url = params.getProperty("url");
		driver = params.getProperty("driver");
		username = params.getProperty("username");
		password = params.getProperty("password");
	}
	
	/**
	 * 和数据库建立连接
	 * @return 连接的对象
	 */
	public static Connection openConnection(){
		try {
			//加载驱动（以反射方式）
			Class.forName(driver);
			//获取连接
			return DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 关闭连接
	 * @param connection
	 */
	public static void closeConnection(Connection connection){
		//判断传递进来的connection参数是否为空
		if(connection!=null){
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	public static void main(String[] args) {
		if (openConnection()!=null) {
			System.out.println("连接成功!");
		} else {
			System.out.println("连接失败！");
		}
	}
	
}
