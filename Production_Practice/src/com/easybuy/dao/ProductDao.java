package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.Product;

/**
 * 商品的数据库操作
 * @author admin
 *
 */
public interface ProductDao {
	Integer add(Product product) throws Exception;
	//修改商品
	Integer update(Product product) throws Exception;
	//修改商品库存数量
	Integer updateStock(Integer id, Integer quantity) throws Exception;
	
	Integer deleteProductById(Integer id) throws Exception;
	
	Product getProductById(Integer id) throws Exception;
	//根据商品名称，分类，等级查询商品数量
	Integer queryProductCount(String proName, Integer categoryId, Integer level) throws Exception;
	/**
	 * 根据条件查询所有商品
	 * @param currentPage 页码
	 * @param pageSize 每一页的数量
	 * @param proName 商品名称
	 * @param categoryId 商品的分类 
	 * @param level 商品的等级
	 * @return
	 * @throws Exception
	 */
	List<Product> getProductionList(Integer currentPage, Integer pageSize, String proName,
			Integer categoryId, Integer level) throws Exception;
}
