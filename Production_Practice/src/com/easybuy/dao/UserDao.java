package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.User;

/**
 * 确定用户数据库操作的功能接口
 * @author Pinjie Xu
 *
 */
public interface UserDao {
	//新增用户
	int add(User user)throws Exception;
	//更新用户
	int update(User user)throws Exception;
	//删除用户
	int deleteUserById(String id)throws Exception;
	//查询用户的条数
	Integer count()throws Exception;
	//分页查询所有的用户
	//currentPageNumber:当前页码，pageSize:每一页条数
	List<User> getUserList(Integer currentPageNumber, Integer pageSize)throws Exception;
	//查询单个用户
	User getUser(Integer id, String loginName)throws Exception;
}
