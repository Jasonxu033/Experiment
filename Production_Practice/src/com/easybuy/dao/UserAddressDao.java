package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.UserAddress;
import com.easybuy.param.UserAddressParam;

/**
 * 用户地址操作的功能
 * @author Pinjie Xu
 *
 */
public interface UserAddressDao {
	//查询用户所有的地址
	List<UserAddress> queryUserAddressList(UserAddressParam param);
	//添加用户
	Integer add(UserAddress userAddress);
	//通过id查询地址
	UserAddress getUserAddressById(Integer addressId);
}
