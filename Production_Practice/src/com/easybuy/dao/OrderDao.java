package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.Order;

/**
 * 确定订单实体类的操作
 * @author admin
 *
 */
public interface OrderDao {
	//添加订单信息
	void add(Order order);
	//根据id删除订单
	void deleteById(Integer id);
	//根据id查询订单信息
	Order getOrderById(Integer id);
	//根据用户id查询订条数量
	Integer count(Integer UserId);
	/**
	 * 根据用户查订单列表
	 * @param UserId 用户号
	 * @param currentPageNo 查询页码
	 * @param pageSize 每一页的订单数
	 * @return
	 */
	List<Order> getOrderList(Integer UserId, Integer currentPageNo, Integer pageSize);
}
