package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.OrderDetail;
import com.easybuy.param.OrderDetailParam;

/**
 * 确定订单详情的操作功能
 * @author admin
 *
 */
public interface OrderDetailDao {
	//添加订单
	void add(OrderDetail de);
	//删除订单
	void deleteById(OrderDetail de);
	//获得数量
	Integer queryOrderDetailCount();
	//根据Id获得单个订单详情
	OrderDetail getOrderDetailById(Integer id);
	//根据订单id查询订单下所有的详情列表
	List<OrderDetail> getOrderDetailList(Integer orderId);
	
}
