package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.ProductCategory;
import com.easybuy.param.ProductCategoryParam;

/**
 * 确定商品分类操作功能
 * @author admin
 *
 */
public interface ProductCategoryDao {
	Integer add(ProductCategory productCategory);
	void update(ProductCategory productCategory);
	void deleteById(Integer id);
	Integer queryProductCategoryCount(ProductCategoryParam param);
	ProductCategory queryProductCategoryById(Integer id);
	List<ProductCategory> queryProductCategoryList(ProductCategoryParam param);
	

}
