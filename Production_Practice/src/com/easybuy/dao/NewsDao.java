package com.easybuy.dao;

import java.util.List;

import com.easybuy.entity.News;
import com.easybuy.param.NewsParams;

public interface NewsDao {
	
	/**
	 * 更新新闻
	 * @param news
	 * @throws Exception
	 */
	void update(News news) throws Exception;
	
	/**
	 * 添加新闻
	 * @param news
	 * @throws Exception
	 */
	void add(News news) throws Exception;
	
	/**
	 * 通过id删除新闻
	 * @param id
	 * @throws Exception
	 */
	
	void deleteById(Integer id) throws Exception;
	
	News getNewsById(Integer id) throws Exception;
	
	/**
	 * 查询新闻列表
	 * NewsParams:新闻扩展类
	 * @return
	 */
	List<News> queryNewsList(NewsParams params) throws Exception;
	
	/**
	 *  查询新闻数量
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Integer queryNewsCount(NewsParams params) throws Exception;
}
