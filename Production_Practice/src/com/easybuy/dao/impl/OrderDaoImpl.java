package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.easybuy.dao.OrderDao;
import com.easybuy.entity.Order;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.Pager;

import sun.invoke.empty.Empty;
import sun.print.PageableDoc;

public class OrderDaoImpl extends BaseDaoImpl implements OrderDao {
	
	public static void main(String[] args) {
		OrderDaoImpl impl = new OrderDaoImpl(DataSourceUtil.openConnection());
		Order order = new Order();
		List<Order> orderList = impl.getOrderList(null, 1, 1);
		for (Order order2 : orderList) {
			System.out.println(order2.getLoginName());
		}
		
	}
	
	public OrderDaoImpl(Connection connection) {
		super(connection);
	}

	@Override
	public void add(Order order) {
		String sql=" "
				+ " insert into easybuy_order(userId, loginName, userAddress, "
				+ " createTime,  cost, serialNumber) "
				+ " values(?, ?, ?, ?, ?, ?) ";
		Object[] params = {order.getUserId(), order.getLoginName(), 
				order.getUserAddress(), new Date(), 
				order.getCost(), order.getSerialNumber()};
		//执行添加
		this.executeInsert(sql, params);
	}

	@Override
	public void deleteById(Integer id) {
		String sql=" delete from easybuy_order where id=? ";
		Object[] params = {id};
		//执行添加
		this.executeUpdate(sql, params);

	}
	
	/**
	 * 根据id查询订单
	 * @param id
	 * @return
	 */
	@Override
	public Order getOrderById(Integer id) {
		Order order=new Order();
		String sql=" select * from easybuy_order where id=? ";
		Object[] params = {id};
		ResultSet rs=null;
		
		try {
			rs = this.executeQuery(sql, params);
			while(rs.next()){
				order = this.tableToClass(rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		
		return order;
	}

	@Override
	public Integer count(Integer UserId) {
		// 返回count
		Integer count=0;
		StringBuffer sql=new StringBuffer(""
				+ " select count(*) count from easybuy_order "
				+ " where 1=1 ");
		// 创建集合，动态填充?
		List<Object> paramList=new ArrayList<Object>();
		//动态填充
		if(EmptyUtils.isNotEmpty(UserId)){
			sql.append(" and userId=? ");
			paramList.add(UserId);
		}
		ResultSet rs=null;
		
		try {
			rs=this.executeQuery(sql.toString(), paramList.toArray());
			while(rs.next()){
				count=rs.getInt("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return count;
	}

	@Override
	public List<Order> getOrderList(Integer userId, Integer currentPageNo, Integer pageSize) {
		List<Order> orderList = new ArrayList<Order>();
		StringBuffer sql = new StringBuffer(""
				+ " select * "
				+ " from easybuy_order "
				+ " where 1=1 ");
		// 动态拼接的集合
		List<Object> paramsList = new ArrayList<Object>();
		if(EmptyUtils.isNotEmpty(userId)){
			sql.append(" and userId=? ");
			paramsList.add(userId);
		}
		int total=this.count(userId);
		//分页数据对象
		Pager pager=new Pager(total, pageSize, currentPageNo);
		sql.append(" limit "
				+(pager.getCurrentPage()-1)*pager.getRowPerPage()
				+", "
				+(pager.getRowPerPage()));
		ResultSet rs=null;
		
		try {
			rs=this.executeQuery(sql.toString(), paramsList.toArray());
			while(rs.next()){
				Order order=this.tableToClass(rs);
				//将单个订单对象添加到订单列表中
				orderList.add(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return orderList;
	}

	/**
	 * 将结果转化为对象
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	@Override
	public Order tableToClass(ResultSet rs) throws Exception {
		Order order=new Order();
		order.setId(rs.getInt("userId"));
		order.setLoginName(rs.getString("LoginName"));
		order.setUserAddress(rs.getString("userAddress"));
		order.setCreateTime(rs.getDate("createTime"));
		order.setCost(rs.getFloat("cost"));
		order.setSerialNumber(rs.getString("serialNumber"));
		return order;
	}

}
