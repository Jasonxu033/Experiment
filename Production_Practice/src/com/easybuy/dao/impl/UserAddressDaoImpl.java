package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.easybuy.dao.UserAddressDao;
import com.easybuy.entity.UserAddress;
import com.easybuy.param.UserAddressParam;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.ParameterList;

public class UserAddressDaoImpl extends BaseDaoImpl implements UserAddressDao {

	public UserAddressDaoImpl(Connection openConnection) {
		super(openConnection);
	}

	@Override
	public UserAddress tableToClass(ResultSet rs) throws Exception {
		UserAddress userAddress = new UserAddress();
		userAddress.setId(rs.getInt("id"));
		userAddress.setUserId(rs.getInt("userId"));
		userAddress.setAddress(rs.getString("address"));
		userAddress.setCreateTime(rs.getDate("createTime"));
		userAddress.setRemark(rs.getString("remark"));
		
		return userAddress;
	}
	
	@Override
	public Integer add(UserAddress userAddress){
		Integer count=0;
		try {
			String sql=" insert into easybuy_address_user "
					+ "(userId, address, createTime, isDefault, remark) "
					+ "values(?,?,?,?,?)";
			Object[] params={userAddress.getUserId(), userAddress.getAddress(), 
					new Date(), 0, userAddress.getRemark()};
			
			count = this.executeInsert(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return count;
	}
	
	public static void main(String[] args){
		UserAddressDaoImpl daoImpl = new UserAddressDaoImpl(DataSourceUtil.openConnection());
		
		//UserAddress userAddressById = daoImpl.getUserAddressById(1);
		//System.out.println(userAddressById.getAddress());
		
		UserAddressParam ddd = new UserAddressParam();
		List<UserAddress> list = daoImpl.queryUserAddressList(ddd);
		for (UserAddress userAddress : list){
			System.out.println(userAddress.getAddress());
		}
	}

	/**
	 * 根据动态条件过滤查询数据库中的地址
	 */
	@Override
	public List<UserAddress> queryUserAddressList(UserAddressParam param) {
		List<UserAddress> userAddressList = new ArrayList<UserAddress>();
		StringBuffer sql=new StringBuffer(""
				+ " select id, userId, address, createTime, isDefault, remark "
				+ " from easybuy_address_user "
				+ " where 1=1 ");
		//用于动态填充占位符'?'
		List<Object> paramsList =  new ArrayList<Object>();
		//是否需要动态拼接sql语句
		if (EmptyUtils.isNotEmpty(param.getUserId())){
			sql.append(" and userId=? ");
			paramsList.add(param.getUserId());
		}
		if (EmptyUtils.isNotEmpty(param.getAddress())){
			sql.append(" and address like ? ");
			paramsList.add("%"+param.getAddress()+"%");
		}
		
		//过滤排序效果
		if (EmptyUtils.isNotEmpty(param.getSort())){
			sql.append(" order by "+param.getSort()+" ");
		}
		//分页效果
		if(param.isPage()){
			sql.append(" limit  " + param.getStartIndex() + ", " + param.getPageSize());
		}
		ResultSet rs = null;
		try {
			rs = this.executeQuery(sql.toString(), paramsList.toArray());
			while(rs.next()){
				UserAddress userAddress = this.tableToClass(rs);
				userAddressList.add(userAddress);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return userAddressList;
	}

	/**
	 * 根据id查询用户地址
	 */
	@Override
	public UserAddress getUserAddressById(Integer addressId) {
		UserAddress userAddress = null;
		
		String sql = ""
				+ " select id, userId, address, createTime, isDefault, remark "
				+ " from easybuy_address_user "
				+ " where id=? ";
		Object[] params = {addressId};
		ResultSet rs = null;
		
		try {
			rs = this.executeQuery(sql, params);
			while(rs.next()){
				userAddress=this.tableToClass(rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		
		return userAddress;
	}

}
