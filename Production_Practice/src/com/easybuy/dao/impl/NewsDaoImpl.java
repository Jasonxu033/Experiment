package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.easybuy.dao.NewsDao;
import com.easybuy.entity.News;
import com.easybuy.param.NewsParams;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;

public class NewsDaoImpl extends BaseDaoImpl implements NewsDao {

	public NewsDaoImpl(Connection connection) {
		super(connection);
	}

	
	public static void main(String[] args) {
		NewsDaoImpl daoImpl=new NewsDaoImpl(DataSourceUtil.openConnection());
	
		try {
		    NewsParams newsParams=new NewsParams();
		    //调用查询的方法，将结果放到list集合中
			List<News> list = daoImpl.queryNewsList(newsParams);
			//循环遍历list集合
			for (News news : list) {
				System.out.println(news.getTitle()+" "+news.getContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void add(News news) throws Exception {
		
		String sql="insert into easybuy_news(title,content,createTime) values(?,?,?) ";
		//创建填充问号的数组            new Date():调用系统当前时间
		Object[] params={news.getTitle(),news.getContent(),new Date()};
		this.executeUpdate(sql, params);
		
	}

	@Override
	public void update(News news) throws Exception {
		
		String sql="update easybuy_news set title=?,content=? where id=?  ";
		Object[] params={news.getTitle(),news.getContent(),news.getId()};
		this.executeUpdate(sql, params);

	}

	@Override
	public void deleteById(Integer id) throws Exception {
		
		String sql=" delete from easybuy where id=? ";
		Object[] params={id};
		this.executeUpdate(sql, params);
	}
	
	
	/**
	 * 根据id查询得到单个新闻
	 */
	@Override
	public News getNewsById(Integer id) throws Exception {
		//创建用于返回的对象
		News news=null;
		String sql=" select * from easybuy_news where id=? ";
		Object[] params={id};
		ResultSet rs=null;
		
		try {
			rs=this.executeQuery(sql, params);
			while(rs.next()){
				news=this.tableToClass(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return news;
	}
	
	
	@Override
	public List<News> queryNewsList(NewsParams params) throws Exception {
		//创建用于返回的集合
		List<News> newsList=new ArrayList<News>();
		//创建sql语句
		StringBuffer sql=new StringBuffer(" select * from easybuy_news where 1=1 ");
		//创建用于动态填充？的集合
		List<Object> paramList=new ArrayList<Object>();
		
		//动态拼接是否根据新问题查询
		if (EmptyUtils.isNotEmpty(params.getTitle())) {
			sql.append(" and title like ? ");
			//填充？
			paramList.add(params.getTitle());
		}
		
		//动态拼接是否排序
		if (EmptyUtils.isNotEmpty(params.getSort())) {
			sql.append(" order by "+params.getSort()+" ");
		}
		
		//动态拼接是否分页
		if (params.isPage()) {
			sql.append(" limit "+params.getStartIndex()+","+params.getPageSize());
		}
		
		ResultSet rs=null;
		
		try {
			//执行查询，得到结果集
			rs=this.executeQuery(sql.toString(), paramList.toArray());
			//可能有多个值，循环取出每一个值，并放到用于返回的集合中
			while(rs.next()){
				//取出每个值
				News news = this.tableToClass(rs);
				//放到用于返回的集合中
				newsList.add(news);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return newsList;
	}

	
	
	
	@Override
	public Integer queryNewsCount(NewsParams params) throws Exception {
		
		
		Integer count=0;
		StringBuffer sql=new StringBuffer(" select count(*) count from easybuy_news where 1=1 ");
		//创建用于拼接填充数据的集合
		List<Object> paramsList=new ArrayList<Object>();
		if (EmptyUtils.isNotEmpty(params.getTitle())) {
			sql.append(" and title like ? ");
			paramsList.add(params.getTitle());
				
		}
		ResultSet rs=null;
		try {
			rs=this.executeQuery(sql.toString(), paramsList.toArray());
			while(rs.next()){
				count=rs.getInt("count");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return count;
	}
	
	
	/**
	 * 将查询结果转化为对象
	 */
	@Override
	public News tableToClass(ResultSet rs) throws Exception {
		News news=new News();
		news.setId(rs.getInt("id"));
		news.setTitle(rs.getString("title"));
		news.setContent(rs.getString("content"));
		news.setCreateTime(rs.getDate("createTime"));
		
		return news;
	}

}
