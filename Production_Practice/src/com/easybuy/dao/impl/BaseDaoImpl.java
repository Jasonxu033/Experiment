package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

/**
 * 基本的Dao层实现，用于提取基本的增删改查
 * @author Pinjie Xu
 *
 */
public abstract class BaseDaoImpl {
	//连接对象
	protected Connection connection;
	//编译对象(SQL语句的预编译)
	protected PreparedStatement pstm;
	
	//确保使用BaseDaoImpl的时候，必须传递连接信息
	public BaseDaoImpl(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * 查询
	 * @param sql 数据库查询指令
	 * @param params 查询指令中需要填充的参数
	 * @return 查询的结果集
	 */
	public ResultSet executeQuery(String sql, Object[] params){
		ResultSet rs=null;
		
		try {
			//获取编译对象
			pstm=connection.prepareStatement(sql);
			if(params!=null){
				//循环填充sql中的参数信息
				for (int i = 0; i < params.length; i++) {
					//一次填充
					pstm.setObject(i+1, params[i]);
				}
			}
			//执行查询
			rs=pstm.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
	
	/**
	 * 更新
	 * @param sql
	 * @param params
	 * @return
	 */
	public int executeUpdate(String sql, Object[] params){
		int updateRows = 0;
		
		try {
			pstm = connection.prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				pstm.setObject(i+1, params[i]);
			}
			updateRows=pstm.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updateRows;
	}
	
	/**
	 * 插入
	 * @param sql
	 * @param params
	 * @return
	 */
	public int executeInsert(String sql, Object[] params){
		Long id = 0L;
		
		try {
			//RETURN_GENERATED_KEYS添加以后返回的关键值
			pstm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			for (int i = 0; i < params.length; i++) {
				pstm.setObject(i+1, params[i]);
			}
			//执行添加语句
			pstm.executeUpdate();
			ResultSet rs=pstm.getGeneratedKeys();
			
			if(rs.next()){
				id=rs.getLong(1);
				System.out.println("数据的主键："+id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id.intValue();
	}
	
	/**
	 * 释放资源
	 * @return
	 */
	public boolean closeResource(){
		if(pstm!=null){
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 释放结果集
	 * @return
	 */
	public boolean closeResource(ResultSet rs){
		if(rs!=null){
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 将表转化成类的抽象方法
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public abstract Object tableToClass(ResultSet rs) throws Exception;
}
