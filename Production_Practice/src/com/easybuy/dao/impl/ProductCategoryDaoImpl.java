package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.easybuy.dao.ProductCategoryDao;
import com.easybuy.entity.ProductCategory;
import com.easybuy.param.ProductCategoryParam;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;

public class ProductCategoryDaoImpl extends BaseDaoImpl implements ProductCategoryDao {

	public ProductCategoryDaoImpl(Connection connection) {
		super(connection);
		
	}
public static void main(String[] args) {
	  ProductCategoryDaoImpl daoImpl=new ProductCategoryDaoImpl(DataSourceUtil.openConnection());

	  ProductCategoryParam param=new ProductCategoryParam();

	  List<ProductCategory>list=daoImpl.queryProductCategoryList(param);
	  for (ProductCategory category : list) {
		System.out.println(category.getName()+" "+category.getType());
	}
}
	@Override
	public Integer add(ProductCategory productCategory) {
		Integer id=0;
		String sql="insert into easybuy_product_category(name,parentId,type,IconClass) values(?,?,?,?)";
		Object[]params={productCategory.getName(),productCategory.getParentId(),productCategory.getType(),productCategory.getIconClass()};
		id=this.executeInsert(sql, params);
		
		
		return id;
	}

	@Override
	public void update(ProductCategory productCategory) {
		
		String sql="update easybuy_product_category set name=?,parentId=?,type=?,iconClass=? where id=? ";
		Object[]params={productCategory.getName(),productCategory.getParentId(),productCategory.getType(),productCategory.getIconClass(),productCategory.getId()};
		
		this.executeUpdate(sql, params);
		

	}

	@Override
	public void deleteById(Integer id) {
		String sql="delete from easybuy_product_category where id=? ";
		Object[]params={id};
		
		this.executeUpdate(sql, params);
		


	}

	@Override
	public Integer queryProductCategoryCount(ProductCategoryParam param) {
		Integer count=0;
		StringBuffer sql=new StringBuffer("select count(*) count from easybuy_product_category where 1=1 ");
		//创建用于填充？
		List<Object> paramList=new ArrayList<Object>();
		if (EmptyUtils.isNotEmpty(param.getName())) {
			//追加名字模糊查询
			sql.append("and name like ?");
			//添加集合
			paramList.add("%"+param.getName()+"%");
				
		}
		if (EmptyUtils.isNotEmpty(param.getParentId())) {
		sql.append(" and parentId=? ");
		paramList.add(param.getParentId());
		}
		ResultSet rs=null;
		 try {
			rs=this.executeQuery(sql.toString(), paramList.toArray());
			while(rs.next()){
				count=rs.getInt("count");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			
		
		finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return count;
}

	@Override
	public ProductCategory queryProductCategoryById(Integer id) {
		ProductCategory category=new ProductCategory();
		String sql="select * from easybuy_product_category  where id=? ";
		Object[]params={id};
		ResultSet rs=null;
		try {
			rs=this.executeQuery(sql, params);
			while(rs.next()){
				category=this.tableToClass(rs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return category;
	}

	@Override
	public List<ProductCategory> queryProductCategoryList(ProductCategoryParam param) {
		List<ProductCategory> productCategorieList= new ArrayList<ProductCategory>();
		StringBuffer sql=new StringBuffer("select epc1.*,epc2.name as parentName from easybuy_product_category epc1 left join easybuy_product_category epc2 on epc1.parentId=epc2.id where 1=1 ");
		List<Object>paramList=new ArrayList<Object>();
		if(EmptyUtils.isNotEmpty(param.getName())){
			sql.append("and epc1.name like ?");
			paramList.add("%"+param.getName()+"%");
			
			
		}
		if (EmptyUtils.isNotEmpty(param.getParentId())) {
			sql.append("and epc1.parentId = ?");
			paramList.add(param.getParentId());
			
		}if(EmptyUtils.isNotEmpty(param.getType())){
		sql.append("and epc1.type = ?");
		paramList.add(param.getType());
		}
		if (param.isPage()) {
			sql.append("limit"+param.getStartIndex()+","+param.getPageSize());
			
		}
	ResultSet rs=null;
	try {
		rs=this.executeQuery(sql.toString(), paramList.toArray());
		while(rs.next()){
			ProductCategory category=this.tableToClass(rs);
			category.setParentName(rs.getString("parentName"));
			productCategorieList.add(category);
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		this.closeResource();
		this.closeResource(rs);
	}
		return productCategorieList;
	}

	@Override
	public ProductCategory tableToClass(ResultSet rs) throws Exception {
		ProductCategory category=new ProductCategory();
		category.setId(rs.getInt("id"));
		category.setName(rs.getString("name"));
		category.setParentId(rs.getInt("parentId"));
		category.setType(rs.getInt("type"));
		category.setIconClass(rs.getString("iconClass"));
		return category;
	}

}
