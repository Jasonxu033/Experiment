package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.easybuy.dao.OrderDetailDao;
import com.easybuy.entity.News;
import com.easybuy.entity.OrderDetail;
import com.easybuy.entity.Product;
import com.easybuy.param.OrderDetailParam;
import com.easybuy.util.DataSourceUtil;
//import com.easybuy.util.EmptyUtil;
import com.easybuy.util.EmptyUtils;

public class OrderDetailDaoImpl extends BaseDaoImpl implements OrderDetailDao {

	public OrderDetailDaoImpl(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		OrderDetailDaoImpl od=new OrderDetailDaoImpl(DataSourceUtil.openConnection());
		OrderDetailParam detail=new OrderDetailParam();
		List<OrderDetail> ol = od.getOrderDetailList(null);
		for (OrderDetail orderDetail : ol) {
			System.out.println(orderDetail.getId()+"  "+orderDetail.getQuantity());			
		}
	}
	/**
	 * 订单详情
	 */
	@Override
	public void add(OrderDetail de) {
		String sql=" insert into easybuy_order_detail(orderId,productId,quantity,cost) values(?,?,?,?) ";
		//填充？
		Object[] params={de.getOrderId(),de.getProduct().getId(),de.getQuantity(),de.getCost()};
		this.executeInsert(sql, params);		
	}

	@Override
	public void deleteById(OrderDetail de) {
		String sql=" delete from easybuy_order_detail where id=?  ";
		//填充？
		Object[] params={de.getId()};
		this.executeUpdate(sql, params);		
	
	}

	@Override
	public Integer queryOrderDetailCount() {
		Integer count=0;
		String sql=new String(" select count(*) count from easybuy_order_detail where 1=1 ");
		//创建集合，动态填充
		ResultSet rs=null;
		try {
			rs=this.executeQuery(sql, null);
			while(rs.next()){
				count=rs.getInt("count");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return count;
	}

	@Override
	public OrderDetail getOrderDetailById(Integer id) {
		OrderDetail detail=new OrderDetail();
		String sql=" select * from easybuy_order_detail where id=? ";
		Object[] param={id};
		ResultSet rs=null;
		try {
			rs=this.executeQuery(sql, param);
			while(rs.next()){
				detail=this.tableToClass(rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return detail;
	}

	@Override
	public List<OrderDetail> getOrderDetailList(Integer orderId) {
		List<OrderDetail> odl=new ArrayList<OrderDetail>();
		StringBuffer sql =new StringBuffer(" select *from easybuy_order_detail where 1=1 ");
		//?
		List<Object> paramsList=new ArrayList<Object>();
		if(EmptyUtils.isNotEmpty(orderId)){
			sql.append(" and orderId=? ");
			paramsList.add(orderId);
		}
		ResultSet rs=null;
		try {
			rs=this.executeQuery(sql.toString(), paramsList.toArray());
			while(rs.next()){
				OrderDetail detail=this.tableToClass(rs);
				//detail放到odl集合
				odl.add(detail);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return odl;
	}

	@Override
	public OrderDetail tableToClass(ResultSet rs) throws Exception {
		// TODO Auto-generated method stub
		OrderDetail de=new OrderDetail();
		de.setId(rs.getInt("id"));
		de.setOrderId(rs.getInt("orderId"));
		de.setProductId(rs.getInt("ProductId"));
		de.setQuantity(rs.getInt("quantity"));
		de.setCost(rs.getFloat("cost"));
		return de;
	}
	
}
