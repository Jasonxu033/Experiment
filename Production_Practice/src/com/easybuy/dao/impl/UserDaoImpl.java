package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.easybuy.dao.UserDao;
import com.easybuy.entity.User;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.Pager;

/**
 * 实现用户表数据库操作的内容
 * @author admin
 *
 */
public class UserDaoImpl extends BaseDaoImpl implements UserDao {

	public static void main(String[] args) {
        UserDaoImpl daoImpl = new UserDaoImpl(DataSourceUtil.openConnection());
        User user = daoImpl.getUser(1, null);

        System.out.println(user.getLoginName()+"  "+user.getIdentityCode());
    }
	
	public UserDaoImpl(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 保存用户（这是文档注释）
	 * @param user 要添加的用户对象//浏览器上的注册
	 */
	@Override
	public int add(User user){
		//创建用户返回的变量信息id
		Integer id=0;
		//执行添加的SQL语句
		try {
			String sql=" insert into "
					+ "easybuy_user(loginName, userName, password, sex, identityCode, email, mobile) "
					+ "values(?,?,?,?,?,?,?) ";
			//创建填充？占位符的数组
			Object[] params={user.getLoginName(), user.getUserName(),
					user.getPassword(), user.getSex(), user.getIdentityCode(),
					user.getEmail(), user.getMobile()};
			id = this.executeInsert(sql, params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return id;
	}
	
	/**
	 * 更新用户信息
	 */
	@Override
	public int update(User user){
		// TODO Auto-generated method stub
		Integer count=0;
		
		try {
			String sql = " update easybuy_user "
					+ "set loginName=?, userName=?, sex=?, identityCode=?, email=?, mobile=?  "
					+ "where id=?";
			Object[] params={user.getLoginName(), user.getUserName(), user.getSex(), 
					user.getIdentityCode(), user.getEmail(), user.getMobile(), user.getId()};
			count=this.executeUpdate(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		
		return count;
	}

	@Override
	public int deleteUserById(String id){
		//删除
		Integer count=0;
		//执行添加的SQL语句
		try {
			String sql="delete from easybuy_user where id=? ";
			//创建填充？占位符的数组
			Object[] params={id};
			count = this.executeUpdate(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return count;
	}
	
	/**
	 * 查询所有用户数量
	 */
	@Override
	public Integer count(){
		Integer num=0;
		String sql = "select count(*) count from easybuy_user ";
		ResultSet rs=null;
		try {
			//rs存查询结果
			rs = this.executeQuery(sql, null);
			while (rs.next()) {
				num=rs.getInt("count");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return num;
	}

	
	/**
	 * 查询第currentPageNumber页，每页pageSize条信息
	 */
	@Override
	public List<User> getUserList(Integer currentPageNumber, Integer pageSize) {
		//创建用于返回的集合
		List<User> userList=new ArrayList<User>();
		
		//limit分页, 第x条数据查y个
		String sql="select id, loginName, password, userName, "
				+ "sex, identityCode, email, mobile, type "
				+ "from easybuy_user "
				+ "limit ?,?";
		ResultSet rs=null;
		
		try {
			//总用户数
			int total=count();
			//分页对象信息
			Pager pager = new Pager(total, pageSize, currentPageNumber);
			//填充？的数组
			Object[] params = {(pager.getCurrentPage()-1)*pager.getRowPerPage(), pager.getRowPerPage()};
			//执行得到结果集
			rs=this.executeQuery(sql, params);
			//循环获取rs的数据
			while(rs.next()){
				User user=this.tableToClass(rs);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		
		return userList;
	}

	/**
	 * 根据id或者登陆名查询单个用户对象
	 */
	@Override
	public User getUser(Integer id, String loginName) {
		//创建用于返回的对象
		User user=new User();
		//创建sql语句
		StringBuffer sql=new StringBuffer("  select id,loginName,password,userName,sex,"
				+ "identityCode,email,mobile,type from easybuy_user where 1=1 ");
		//动态填充？的集合（需要转化为数组）
		List<Object> paramsList=new ArrayList<Object>();
		/**
		 * where 1=1是为了与后面的条件进行拼接使用
		 * 后面条件：1.and id=?
		 * 			2.and loginName=?
		 */
		//如果id不为空，则在sql语句中拼接上id的条件
		if (EmptyUtils.isNotEmpty(id)) {
			sql.append(" and id=? ");	
			paramsList.add(id);
		}
		if (EmptyUtils.isNotEmpty(loginName)) {
			sql.append(" and loginName=? ");
			paramsList.add(loginName);
		}
		ResultSet rs=null;
		try {
			//执行查询
			rs=this.executeQuery(sql.toString(), paramsList.toArray());
			while (rs.next()) {
				user=this.tableToClass(rs);		
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			this.closeResource();
			this.closeResource(rs);
		}

		return user;
	}

	
	/**
	 * 将表中的信息转化为对象中的属性
	 * @param rs 数据库查询结果集
	 * @return Object 转化的用户对象
	 */
	@Override
	public User tableToClass(ResultSet rs) throws Exception {
		//创建用户对象，用户函数返回
		User user = new User();
		//从rs中取出字段信息，放到用户对象中
		user.setLoginName(rs.getString("loginName"));
		user.setUserName(rs.getString("userName"));
		user.setPassword(rs.getString("password"));
		user.setSex(rs.getInt("Sex"));
		user.setIdentityCode(rs.getString("identityCode"));
		user.setEmail(rs.getString("email"));
		user.setMobile(rs.getString("mobile"));
		user.setType(rs.getInt("type"));
		user.setId(rs.getInt("id"));
		
		//将转化后的用户对象返回
		return user;
	}

}
