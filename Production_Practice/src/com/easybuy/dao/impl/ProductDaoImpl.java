package com.easybuy.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.ls.LSInput;

import com.easybuy.dao.ProductDao;
import com.easybuy.entity.Product;
import com.easybuy.util.DataSourceUtil;
import com.easybuy.util.EmptyUtils;
import com.easybuy.util.Pager;

import sun.invoke.empty.Empty;

/**
 * 商品数据库操作
 * @author admin
 *
 */
public class ProductDaoImpl extends BaseDaoImpl implements ProductDao {
	public static void main(String[] args) {
		ProductDaoImpl daoImpl = new ProductDaoImpl(DataSourceUtil.openConnection());
		Product p = daoImpl.getProductById(1);
		
		List<Product> pl = daoImpl.getProductionList(1, 10, null, null, null);
		for (Product product : pl) {
			System.out.println(product.getDescription());
		}
		
	}
	public ProductDaoImpl(Connection connection) {
		super(connection);
	}

	@Override
	public Integer add(Product product) {
		Integer id=0;
		String sql=" insert into easybuy_product(name, description, price, "
				+ "stock, categoryLevel1, categoryLevel2, categoryLevel3, "
				+ "fileName, isDelete) "
				+ " values(?,?,?,?,?,?,?,?,?) ";
		Object[] params = {product.getName(), product.getDescription(), product.getPrice(), 
				product.getStock(), product.getCategoryLevel1(), product.getCategoryLevel2(),
				product.getCategoryLevel3(), product.getFileName(), 0};
		try {
			id = this.executeInsert(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeResource();
		}
		return id;
	}

	@Override
	public Integer update(Product product){
		Integer id = 0;
		String sql = " "
				+ " update easybuy_product set "
				+ " name = ?, fileName = ?, categoryLevel1 = ?, categoryLevel2 = ?,"
				+ " categoryLevel3 = ? "
				+ " where id=? ";
		Object[] params = {product.getName(), product.getFileName(), product.getCategoryLevel1(), 
				product.getCategoryLevel2(), product.getCategoryLevel3(), product.getId()};
		try {
			id = this.executeUpdate(sql, params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return id;
	}

	@Override
	public Integer updateStock(Integer id, Integer quantity) {
		Integer count = 0;
		String sql = " update easybuy_product set stock=? "
				+ "    where id = ? ";
		Object[] params = {quantity, id};
		try {
			count = this.executeUpdate(sql, params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return count;
	}

	@Override
	public Integer deleteProductById(Integer id){
		Integer count = 0;
		String sql = " delete from easybuy_product where id=? ";
		Object[] params = {id};
		try {
			count = this.executeUpdate(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			this.closeResource();
		}
		return count;
	}

	@Override
	public Product getProductById(Integer id) {
		Product product = new Product();
		String sql = " select * from easybuy_product where id=? ";
		Object[] params = {id};
		ResultSet rs = null;
		
		try {
			rs=this.executeQuery(sql, params);
			while(rs.next()){
				product=this.tableToClass(rs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			this.closeResource();
			this.closeResource(rs);
		}
		return product;
	}

	@Override
	public Integer queryProductCount(String proName, Integer categoryId, Integer level) {
		Integer count=0;
		StringBuffer sql = new StringBuffer(""
				+ " select count(*) count "
				+ " from easybuy_product "
				+ " where 1=1 ");
		List<Object> paramsList = new ArrayList<Object>();
		
		if(EmptyUtils.isNotEmpty(proName)) {
			//名字模糊查询
			sql.append(" and name like ? ");
			paramsList.add("%" + proName + "%");
		}
		if(EmptyUtils.isNotEmpty(categoryId)){
			sql.append(" and (categoryLevel1=? or categoryLevel2 = ? or categoryLevel3=?) ");
			paramsList.add(categoryId);
			paramsList.add(categoryId);
			paramsList.add(categoryId);
		}
		ResultSet rs = null;
		
		try {
			rs=this.executeQuery(sql.toString(), paramsList.toArray());
			while(rs.next()){
				count=rs.getInt("count");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			this.closeResource();
			this.closeResource(rs);
		}
		return count;
	}

	@Override
	public List<Product> getProductionList(Integer currentPage, Integer pageSize, String proName, Integer categoryId,
			Integer level) {
		List<Product> productList = new ArrayList<Product>();
		StringBuffer sql = new StringBuffer(" select * from easybuy_product where 1=1 ");
		
		List<Object> paramList = new ArrayList<Object>();
		if(EmptyUtils.isNotEmpty(proName)){
			sql.append(" and name like ? ");
			paramList.add("%" + proName + "%");
		}
		if(EmptyUtils.isNotEmpty(categoryId)){
			sql.append(" and (categoryLevel1=? or categoryLevel2 = ? or categoryLevel3=?) ");
			paramList.add(categoryId);
			paramList.add(categoryId);
			paramList.add(categoryId);
		}
		//
		int total = this.queryProductCount(proName, categoryId, level);
		
		Pager pager = new Pager(total, pageSize, currentPage);
		sql.append(" limit " + 
		           (pager.getCurrentPage()-1)*pager.getRowPerPage() + 
		            pager.getRowPerPage());
		ResultSet rs = null;
		
		try {
			rs=this.executeQuery(sql.toString(), paramList.toArray());
			while(rs.next()){
				Product product = this.tableToClass(rs);
				productList.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			this.closeResource();
			this.closeResource(rs);
		}
		return productList;
	}

	@Override
	public Product tableToClass(ResultSet rs) throws Exception {
		Product product=new Product();
		product.setId(rs.getInt("id"));
		product.setName(rs.getString("name"));
		product.setDescription(rs.getString("description"));
		product.setPrice(rs.getFloat("price"));
		product.setStock(rs.getInt("stock"));
		product.setCategoryLevel1(rs.getInt("categoryLevel1"));
		product.setCategoryLevel2(rs.getInt("categoryLevel2"));
		product.setCategoryLevel3(rs.getInt("categoryLevel3"));
		product.setFileName(rs.getString("fileName"));
		return product;
	}

}
