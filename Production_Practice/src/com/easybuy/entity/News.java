package com.easybuy.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 新闻类
 * @author Pinjie Xu
 *
 */
public class News implements Serializable{
	//属性信息
	private Integer id;
	private String title;
	private String Content;
	private Date createTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
