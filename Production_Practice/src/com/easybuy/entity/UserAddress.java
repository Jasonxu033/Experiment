package com.easybuy.entity;

import java.util.Date;

/**
 * 用户地址
 * @author Pinjie Xu
 *
 */
public class UserAddress {
	private Integer id;//地址编号
	private Integer userId;//改地址所属用户
	private String address;
	private Date createTime;
	private String remark;//备注
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
