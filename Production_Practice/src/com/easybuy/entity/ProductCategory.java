package com.easybuy.entity;

import java.io.Serializable;

/**
 * 商品分类
 * @author Pinjie Xu
 *
 */
public class ProductCategory implements Serializable{
	private Integer id;
	private String name;
	private Integer parentId;//父标签
	private Integer type;//分类
	private String iconClass;//图标
	
	private String parentName;//父级名称

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

}
