package com.easybuy.entity;

import java.io.Serializable;

/**
 * 订单详情实体类
 * @author Pinjie Xu
 *
 */
public class OrderDetail implements Serializable {
	private Integer id;
	private Integer orderId; // 订单号
	private Integer productId; //商品编号
	private Integer quantity;
	private Float cost;
	
	private Product product; //商品编号对应商品

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getCost() {
		return cost;
	}

	public void setCost(Float cost) {
		this.cost = cost;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
