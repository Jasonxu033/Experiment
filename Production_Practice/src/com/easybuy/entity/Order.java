package com.easybuy.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 实体类：对应订单表
 * @author Pinjie Xu
 *
 */
public class Order implements Serializable {
	//属性
	private Integer id;
	private Integer userId;
	private String loginName;
	private String userAddress;
	private Date createTime;
	private Float cost;
	private String serialNumber;//订单号
	
	private List<OrderDetail> orderDetails;//订单详情列表，一个订单对应一个订单详情

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Float getCost() {
		return cost;
	}

	public void setCost(Float cost) {
		this.cost = cost;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
}
