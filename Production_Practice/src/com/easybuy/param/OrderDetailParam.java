package com.easybuy.param;

import com.easybuy.entity.OrderDetail;
/**
 * 订单详情的拓展类
 * @author GX
 *
 */
public class OrderDetailParam extends OrderDetail {
	private Integer startIndex;//起始参
	private Integer pageSize;//页参
	private String sort;//排序参
	private boolean isPage = false;//分页参
	
	
	/**
	 * 用于提供分页
	 * @param startIndex
	 * @param pageSize
	 */
	public void openPage(Integer startIndex,Integer pageSize){
		
		this.isPage=true;
		this.pageSize=pageSize;
		this.startIndex=startIndex;
	}


	public Integer getStartIndex() {
		return startIndex;
	}


	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}


	public Integer getPageSize() {
		return pageSize;
	}


	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	public String getSort() {
		return sort;
	}


	public void setSort(String sort) {
		this.sort = sort;
	}


	public boolean isPage() {
		return isPage;
	}


	public void setPage(boolean isPage) {
		this.isPage = isPage;
	}

	
}
