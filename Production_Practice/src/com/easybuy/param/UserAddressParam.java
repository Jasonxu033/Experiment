package com.easybuy.param;

import com.easybuy.entity.UserAddress;

/**
 * 用户地址实体类的拓展类
 * @author admin
 *
 */
public class UserAddressParam extends UserAddress {
	//初始数据
	private Integer startIndex;
	//每一页的参数
	private Integer pageSize;
	//排序参数
	private String sort;
	//是否是一个页面,是否有分页信息
	private boolean isPage=false;
	
	public void openPage(Integer startIndex, Integer pageSize){
		this.isPage=true;
		this.startIndex=startIndex;
		this.pageSize=pageSize;
		
	}
	
	public Integer getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public boolean isPage() {
		return isPage;
	}
	public void setPage(boolean isPage) {
		this.isPage = isPage;
	}
	
	
}
